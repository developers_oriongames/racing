﻿using UnityEngine;
using UnityEditor;
using GameServices;

public class GameEditor : EditorWindow {

    [MenuItem("Game/GameEditor")]
    public static void ShowMissionControl(){
        GetWindow(typeof(GameEditor));
    }

    void OnGUI() {
        ShowPlayStopButton();
        ShowResetDataButton();
        if (EditorApplication.isPlaying) {
            ShowFreeCurrencyButton("Cash");
            ShowGameDataLabel();
            ShowGameTestLabel();
        }
    }

    void ShowGameDataLabel() {
        GUILayout.Label("Game Data", EditorStyles.boldLabel);
        string totalCash = "Total Cash: " + CurrencyManager.TotalCash.ToString();
        string totalStar = "Total Star: " + LevelManager.GetTotalStar().ToString();
        GUILayout.Label(totalCash);
        GUILayout.Label(totalStar);
    }

    void ShowResetDataButton(){
        if (GUILayout.Button("Reset Data")) {
            PlayerPrefs.DeleteAll();
            //CarManager.ResetCars();
            if (EditorApplication.isPlaying) {
                EditorApplication.ExitPlaymode();
            }
        }
    }

    void ShowFreeCurrencyButton(string name){
        int amount = 10000;
        if (GUILayout.Button(amount + " Free " + name)) {
            if (name == "Cash") {
                CurrencyManager.GiveMoney(amount);
            }
        }
    }
    void ShowGameTestLabel() {
        GUILayout.Label("Racing Game TEST Variables", EditorStyles.boldLabel);
        string skipTutorial = " SKIP Tutorial";
        string unlockAllCars = " UNLOCK All Cars";
        string unlockAllLevels = " UNLOCK All Levels";
        string gameStartAnimation = " SKIP Game Start Animation";
        string loadGameQuickly = " Load Game QUICKLY";
        string allowWorngDirection = " Allow WORNG DIRECTION driving";

        GameManager.isToSkipTutorial
            = GUILayout.Toggle(GameManager.isToSkipTutorial
                                = GameManager.isToSkipTutorial ? false : true, skipTutorial, GUILayout.Height(20));
        GameManager.isToUnlockAllCars
            = GUILayout.Toggle(GameManager.isToUnlockAllCars
                                = GameManager.isToUnlockAllCars ? false : true, unlockAllCars, GUILayout.Height(20));
        GameManager.isToUnlockAllLevels
            = GUILayout.Toggle(GameManager.isToUnlockAllLevels
                                = GameManager.isToUnlockAllLevels ? false : true, unlockAllLevels, GUILayout.Height(20));
        GameManager.isToHideGameStartAnimation
            = GUILayout.Toggle(GameManager.isToHideGameStartAnimation
                                = GameManager.isToHideGameStartAnimation ? false : true, gameStartAnimation, GUILayout.Height(20));
        GameManager.isToLoadQuickly
            = GUILayout.Toggle(GameManager.isToLoadQuickly
                                = GameManager.isToLoadQuickly ? false : true, loadGameQuickly, GUILayout.Height(20));
        GameManager.isToAllowWrongDirection
          = GUILayout.Toggle(GameManager.isToAllowWrongDirection
                              = GameManager.isToAllowWrongDirection ? false : true, allowWorngDirection, GUILayout.Height(20));
    }

    void ShowPlayStopButton(){
        string buttonName;
        if(EditorApplication.isPlaying){
            buttonName = "Stop";
        }else{
            buttonName = "Play";
        }
        if (GUILayout.Button(buttonName)) {
            if(EditorApplication.isPlaying){
                EditorApplication.isPlaying = false;
            }else{
                EditorApplication.isPlaying = true;
            }
        }
    }
}
