﻿using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class StartUp
{

#if UNITY_EDITOR
    static StartUp()
    {
        PlayerSettings.keystorePass = "racing";
        PlayerSettings.keyaliasPass = "racing";
    }
#endif

}