﻿using UnityEngine;
using System;
using UnityEditor;
using UnityEditor.Callbacks;

public class BuildPostprocessor
{
    [PostProcessBuild(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        PlayerSettings.Android.bundleVersionCode += 1;

        PlayerSettings.bundleVersion = "0." + PlayerSettings.Android.bundleVersionCode;

        AssetDatabase.SaveAssets();

        //Debug.Log(pathToBuiltProject);
    }
}
