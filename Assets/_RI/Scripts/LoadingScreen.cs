﻿using UnityEngine.SceneManagement;
using UnityEngine;

namespace RI_Extension 
{
    public class LoadingScreen : MonoBehaviour
    {
        private static LoadingScreen instance;

        public static LoadingScreen GetInstance()
        {
            return instance;
        }

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            if (Utility.IsFireTV())
            {
                if (Game.Instance.loadingScreenTV != null)
                {
                    Game.Instance.loadingScreen = Game.Instance.loadingScreenTV;
                    if (Game.Instance.loadingScreenTouch != null)
                    {
                        Game.Instance.loadingScreenTouch.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                if (Game.Instance.loadingScreenTouch != null)
                {
                    Game.Instance.loadingScreen = Game.Instance.loadingScreenTouch;
                    if (Game.Instance.loadingScreenTV != null)
                    {
                        Game.Instance.loadingScreenTV.gameObject.SetActive(false);
                    }
                }
            }
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            HideLoadingScreen();
        }

        public void ShowLoadingScreen()
        {
            if (Game.Instance != null && Game.Instance.loadingScreen != null && Game.Instance.loadingText != null)
            {
                Game.Instance.loadingScreen.gameObject.SetActive(true);
                Game.Instance.loadingText.gameObject.SetActive(true);
            }
        }

        private void HideLoadingScreen()
        {
            if (SceneManager.GetActiveScene().name == "Start")
            {
                if (Game.Instance != null && Game.Instance.loadingScreen != null && Game.Instance.loadingText != null)
                {
                    Game.Instance.loadingScreen.gameObject.SetActive(false);
                    Game.Instance.loadingText.gameObject.SetActive(false);
                }
            }
            else
            {
                if (Game.Instance != null && Game.Instance.loadingScreen != null && Game.Instance.loadingText != null)
                {
                    Game.Instance.loadingScreen.gameObject.SetActive(false);
                    Game.Instance.loadingText.gameObject.SetActive(false);
                }
            }
        }
    }

}
