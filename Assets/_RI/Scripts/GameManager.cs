﻿using UnityEngine.SceneManagement;
using UnityEngine;

namespace RI_Extension 
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance = null;
        [HideInInspector]
        public bool isRestarted = false;

        public bool isDebug = false;
        public bool debugTV = false;
        public bool showFPS = false;

        public int refResolutionX;
        public int refResolutionY;
        public bool isTestPromo;

        public static GameManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<GameManager>();
                }
                return _instance;
            }
        }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

            if (Utility.IsFireTV())
            {
                Game.Instance.isTV = true;
            }
            else
            {
                Game.Instance.isTV = false;
            }

            if (debugTV)
            {
                Game.Instance.isTV = true;
            }

            RestoreData();

            RI_Plugin.Instance.SetIsTv(Game.Instance.isTV + "");
        }

        void Start()
        {
            Application.targetFrameRate = 60;
            RI_Plugin.Instance.InitializePromo(refResolutionX, refResolutionY, isTestPromo);

        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Storage.SaveDataInt("LastSceneLoaded", scene.buildIndex);
        }

        public void RestoreData()
        {
            if (Storage.ReadDataInt(Constants.LaunchCount_Key, -1) == -1)
            {
                StoreData();
            }

            Game.Instance.launchCount = Storage.ReadDataInt(Constants.LaunchCount_Key, 0);
            Game.Instance.launchCount++;
            IAP iap = GetComponent<IAP>();
            if (iap != null)
            {
                iap.SetPurchased();
            }
            Game.Instance.fullGamePurchased = Storage.ReadDataString(Constants.FullGamePurchased, "false").ToLower().Equals("true");
            Game.Instance.removeAdsPurchased = Storage.ReadDataString(Constants.RemoveAdsPurchased, "false").ToLower().Equals("true");
        }

        public void StoreData()
        {
            Storage.SaveDataInt(Constants.LaunchCount_Key, Game.Instance.launchCount);
            Storage.SaveDataString(Constants.FullGamePurchased, Game.Instance.fullGamePurchased + "");
            Storage.SaveDataString(Constants.RemoveAdsPurchased, Game.Instance.removeAdsPurchased + "");
        }

        public void SetFullGameLevel(int fullGameLevel)
        {
            Game.Instance.levelLockAt = fullGameLevel;
        }

        public bool IsTV()
        {
            if (Game.Instance != null)
            {
                return Game.Instance.isTV;
            }
            return false;
        }

        public void Quit()
        {
            Application.Quit();
        }
    }

}