﻿using UnityEngine;

namespace RI_Extension {
    public class IAP : MonoBehaviour {
        public static IAP Instance;

        public IAP_Product[] iap_products = new IAP_Product[]
        {
        new IAP_Product(Constants.UNLOCK_ALL,"$20.99"),
        new IAP_Product(Constants.UNLOCK_CARS,"$9.99"),
        new IAP_Product(Constants.UNLOCK_LEVELS,"$15.99"),
        new IAP_Product(Constants.REMOVE_ADS,"$3.99"),

        new IAP_Product(Constants.BUY_M_KOOPER,"$0.99"),
        new IAP_Product(Constants.BUY_P_FIREBIRD,"$0.99"),
        new IAP_Product(Constants.BUY_H_RS,"$0.99"),
        new IAP_Product(Constants.BUY_F_FIESTA,"$0.99"),
        new IAP_Product(Constants.BUY_B_MW,"$0.99"),
        new IAP_Product(Constants.BUY_C_IMPALA,"$0.99"),
        new IAP_Product(Constants.BUY_M_BENZ_512,"$0.99"),
        new IAP_Product(Constants.BUY_F_FOUS_RS,"$0.99"),
        new IAP_Product(Constants.BUY_D_CHALLENGER,"$0.99"),
        new IAP_Product(Constants.BUY_F_CALIFORNIA,"$0.99"),
        new IAP_Product(Constants.BUY_A_R8,"$0.99"),
        new IAP_Product(Constants.BUY_B_CONTENTAL,"$0.99"),
        new IAP_Product(Constants.BUY_L_LP_670,"$0.99"),
        new IAP_Product(Constants.BUY_M_MP3_12C,"$0.99"),
        };

        [HideInInspector]
        public string[] productIDs;

        void Awake() {
            if (Instance == null) {
                Instance = this;
            }
            if (iap_products != null) {
                productIDs = new string[iap_products.Length];
                for (int i = 0; i < iap_products.Length; i++) {
                    productIDs[i] = iap_products[i].ID;
                }
            }
        }

        public void PurchaseComplete(string product) {
            //purchase was successful
            Debug.Log("<<UnityIAP>>\tPurchase Complete : " + product);
            Storage.SaveDataString(product, "true");
            if (iap_products != null) {
                foreach (IAP_Product prod in iap_products) {
                    if (prod != null && prod.ID.Equals(prod)) {
                        prod.isPurchased = true;
                    }
                }
            }
            GameEvents.OnPurchaseComplete(product);
        }

        public void PurchaseFailed(string product, string failureReason) {
            //purchase was unsuccessful
            Debug.Log("<<UnityIAP>>\tPurchase Failed : " + product + " Because Of " + failureReason);
            GameEvents.OnPurchaseFailed(product, failureReason);
        }

        public void Onclicked() {
            Debug.Log("clicked");
        }

        public void SetPurchased() {
            if (iap_products != null) {
                foreach (IAP_Product product in iap_products) {
                    if (product != null) {
                        product.isPurchased = Storage.ReadDataString(product.ID, "false").ToLower().Equals("true");
                    }
                }
            }
        }

        public bool IsPurchased(string productID) {
            if (iap_products != null) {
                foreach (IAP_Product product in iap_products) {
                    if (product != null && product.ID.Equals(productID)) {
                        return product.isPurchased;
                    }
                }
            }
            return false;
        }
    }

    public class IAP_Product {
        public string ID;
        public bool isPurchased = false;
        public string price;

        public IAP_Product(string ID, string defaultPrice) {
            this.ID = ID;
            price = defaultPrice;
        }
    }
}