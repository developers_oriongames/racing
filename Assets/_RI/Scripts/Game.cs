﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RI_Extension 
{
    public class Game : MonoBehaviour
    {
        private static Game _instance = null;

        public bool isTV = false;
        public int adsInterval;
        public bool isRemoveAdsVarient = false;
        public int levelLockAt = 6;

        public int launchCount = -1;

        public bool isPremium = false;
        public bool removeAdsPurchased = false;
        public bool fullGamePurchased = false;

        public Image loadingScreenTV = null;
        public Image loadingScreenTouch = null;
        [HideInInspector]
        public Image loadingScreen = null;
        public Text loadingText = null;

        public Dictionary<int, string> productIndexMapping, productDefaultPrice;

        #region Properties
        public static Game Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<Game>();
                }
                return _instance;
            }
        }
        #endregion

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
            }
        }
    }
}
