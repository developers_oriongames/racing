﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RI_Extension {
    public class PromoClient : MonoBehaviour {

        public const int SPOT_MENU = 0;
        public const int SPOT_LEVEL_SELECT = 1;
        public const int SPOT_CHARACTER_SELECT = 2;
        public const int SPOT_LEVEL_LOADING = 3;
        public const int SPOT_LEVEL_PAUSE = 4;
        public const int SPOT_LEVEL_COMPLETE = 5;
        public const int SPOT_LEVEL_OVER = 6;
        public const int SPOT_LEVEL_HELP = 7;
        public const int SPOT_LEVEL_CREDITS = 8;

        private static Dictionary<int, Vector4> spotAttributes = null;
        private static int currentSpot = -1, newSpot = -1;

        public delegate void onPromoAdEvent();
        public static event onPromoAdEvent OnPromoAdShown;

        private void Awake() {
            currentSpot = -1;
            newSpot = -1;
            Init();
        }

        private void Update() {
            if (currentSpot != newSpot) {
                currentSpot = newSpot;
                RI_Plugin.Instance.ShowPromoAd(currentSpot, false);
            }
        }

        public static void Init() {
            spotAttributes = null;
            spotAttributes = new Dictionary<int, Vector4>();
        }

        public static void SetSpotAttributes(int spot, Vector4 values) {
            if (spotAttributes != null && spot != -1) {
                spotAttributes.Add(spot, values);
                OnPromoAdShown?.Invoke();
            }
        }

        public static Vector4 GetSpotAttributes(int spot) {
            if (spotAttributes != null && spot != -1 && spotAttributes.ContainsKey(spot)) {
                return spotAttributes[spot];
            }
            return Vector4.negativeInfinity;
        }

        public static void SetPromoSpot(int spot) {
            newSpot = spot;
        }

        public static int GetCurrentPromoSpot() {
            return currentSpot;
        }

        public static bool IsSpotInitialized() {
            return spotAttributes != null && currentSpot != -1 && spotAttributes.ContainsKey(GetCurrentPromoSpot());
        }
    }
}