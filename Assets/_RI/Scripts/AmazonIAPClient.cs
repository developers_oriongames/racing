﻿using com.amazon.device.iap.cpt;
using System.Collections.Generic;
using UnityEngine;

namespace RI_Extension 
{
    public class AmazonIAPClient : MonoBehaviour
    {
        // Obtain object used to interact with plugin
        private IAmazonIapV2 iapService = AmazonIapV2Impl.Instance;
        private string requestUserIdString, requestProductIdString, PurchaseString, requestPurchaseIdString;
        public Dictionary<string, string> productRequestMap = new Dictionary<string, string>();
        public static Dictionary<string, Dictionary<string, string>> productInfo;
        private IAP iap;

        private void Start()
        {
            productInfo = new Dictionary<string, Dictionary<string, string>>();
            iap = GetComponent<IAP>();
            GetUserData();
            GetProductData();
            GetPurchaseUpdates();
        }

        public void Purchase(string product)
        {
            // Construct object passed to operation as input
            SkuInput request = new SkuInput();

            // Set input value
            request.Sku = product;

            // Call synchronous operation with input object
            RequestOutput response = iapService.Purchase(request);

            // Get return value
            PurchaseString = response.RequestId;
            productRequestMap.Add(response.RequestId, product);
            //NotifyFulfillment();
            // Debug.Log("<<IAP Response>>\t"+response.ToString()+"");
        }

        public void GetUserData()
        {
            // Call synchronous operation with no input
            RequestOutput response = iapService.GetUserData();

            // Get return value
            requestUserIdString = response.RequestId;
            // Debug.Log("IAP Strings\t" + requestUserIdString + "\t\t" + requestProductIdString + "\t\t" + PurchaseString + "\t\t" + requestPurchaseIdString);
        }

        public void GetProductData()
        {
            // Construct object passed to operation as input
            SkusInput request = new SkusInput();

            // Create list of SKU strings
            List<string> list = new List<string>();
            foreach (string id in iap.productIDs)
            {
                list.Add(id);
            }
            // Set input value
            request.Skus = list;

            // Call synchronous operation with input object
            RequestOutput response = iapService.GetProductData(request);

            // Get return value
            requestProductIdString = response.RequestId;
            // Debug.Log("IAP Strings\t" + requestUserIdString + "\t\t" + requestProductIdString + "\t\t" + PurchaseString + "\t\t" + requestPurchaseIdString);
        }

        public void GetPurchaseUpdates()
        {
            // Construct object passed to operation as input
            ResetInput request = new ResetInput();

            // Set input value
            request.Reset = true;

            // Call synchronous operation with input object
            RequestOutput response = iapService.GetPurchaseUpdates(request);

            // Get return value
            requestPurchaseIdString = response.RequestId;
            // Debug.Log("IAP Strings\t" + requestUserIdString + "\t\t" + requestProductIdString + "\t\t" + PurchaseString + "\t\t" + requestPurchaseIdString);
        }

        public void NotifyFulfillment(string receiptID, string FulfillmentResult)
        {
            // Construct object passed to operation as input
            NotifyFulfillmentInput request = new NotifyFulfillmentInput();

            // Set input values
            request.ReceiptId = receiptID;
            request.FulfillmentResult = FulfillmentResult;

            // Call synchronous operation with input object
            // RequestOutput response = 
            iapService.NotifyFulfillment(request);
        }
    }



}
