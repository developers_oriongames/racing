﻿using System.Collections.Generic;
using UnityEngine;

namespace RI_Extension 
{
    public class AnalyticsClient
    {

        public const string FULL_GAME_LEVEL = "fullGameLevel";
        public const string REMOVE_ADS_VARIANT = "removeAdsVariant";
        public const string FULL_GAME_IAP_KEY = "iapKey";
        public const string ADS_START_TIMER = "ads_start_timer";
        public const string TRY_NOW = "tryNow";
        public const string BOOSTER = "booster";

        public const string CAR_00_SPITFIRE = "spitfire";
        public const string CAR_01_RAPTOR = "raptor";
        public const string CAR_02_MARINTO = "marinto";
        public const string CAR_03_BULBOUS = "bulbous";
        public const string CAR_04_POWER_HAWK = "power_hawk";
        public const string CAR_05_SKY_FAM = "sky_fam";
        public const string CAR_06_BENDGEAR = "bendgar";
        public const string CAR_07_CAPILO = "capilo";
        public const string CAR_08_GERMLIN = "germlin";
        public const string CAR_09_FANCY_BIRD = "fancy_bird";
        public const string CAR_10_EAGLE_HEAD = "eagle_head";
        public const string CAR_11_RAFFAELE = "raffaele";
        public const string CAR_12_COLADA = "colada";
        public const string CAR_13_MIGHTY_THUNDER = "mighty_thunder";
        public const string CAR_14_RING_BASHER = "ring_basher";

        public static int ads_start_timer = 0;

        public static bool tryNow = false;
        public static bool booster = false;

        private Dictionary<string, string> remoteConfig;

        public Dictionary<string, string> GetRemoteCongParams()
        {
            if (remoteConfig == null)
                remoteConfig = new Dictionary<string, string>();

            return remoteConfig;
        }

        public static void Init()
        {
            ads_start_timer = 0;
            //RI_Plugin.Instance.fetchNewConfig();
        }

        public static void SetValues(string key, string value)
        {
            switch (key)
            {
                case FULL_GAME_LEVEL:
                    if (value != null && !value.Equals("null") && !value.Equals(""))
                    {
                        int val = int.Parse(value);
                        GameManager.Instance.SetFullGameLevel(val);
                    }
                    break;

                case ADS_START_TIMER:
                    if (value != null && !value.Equals("null") && !value.Equals(""))
                    {
                        ads_start_timer = int.Parse(value);
                    }
                    break;

                case TRY_NOW:
                    if (value != null && !value.Equals("null") && !value.Equals(""))
                    {
                        if(value.Contains("true"))
                        {
                            tryNow = true;
                        }
                        else
                        {
                            tryNow = false;
                        }
                    }
                    break;

                case BOOSTER:
                    if (value != null && !value.Equals("null") && !value.Equals(""))
                    {
                        if (value.Contains("true"))
                        {
                            booster = true;
                        }
                        else
                        {
                            booster = false;
                        }
                    }
                    break;

                //case CAR_00_SPITFIRE:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_00_SPITFIRE);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_00_SPITFIRE, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_01_RAPTOR:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_01_RAPTOR);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_01_RAPTOR, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_02_MARINTO:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_02_MARINTO);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_02_MARINTO, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_03_BULBOUS:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_03_BULBOUS);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_03_BULBOUS, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_04_POWER_HAWK:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_04_POWER_HAWK);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_04_POWER_HAWK, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_05_SKY_FAM:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_05_SKY_FAM);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_05_SKY_FAM, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_06_BENDGEAR:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_06_BENDGEAR);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_06_BENDGEAR, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_07_CAPILO:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_07_CAPILO);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_07_CAPILO, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_08_GERMLIN:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_08_GERMLIN);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_08_GERMLIN, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_09_FANCY_BIRD:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_09_FANCY_BIRD);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_09_FANCY_BIRD, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_10_EAGLE_HEAD:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_10_EAGLE_HEAD);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_10_EAGLE_HEAD, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_11_RAFFAELE:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_11_RAFFAELE);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_11_RAFFAELE, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_12_COLADA:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_12_COLADA);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_12_COLADA, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_13_MIGHTY_THUNDER:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_13_MIGHTY_THUNDER);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_13_MIGHTY_THUNDER, int.Parse(value) == 1);
                //    }
                //    break;

                //case CAR_14_RING_BASHER:
                //    if (value != null && !value.Equals("null") && !value.Equals(""))
                //    {
                //        Debug.Log("UNITY_TRIAL_CAR: set value for " + CAR_14_RING_BASHER);
                //        CarManager.trialCars.SetValueFromRemoteConfig(CAR_14_RING_BASHER, int.Parse(value) == 1);
                //    }
                //    break;
            }
        }


        public static void LogEvent(string eventName, Dictionary<string, string> parameters)
        {

            if (parameters != null)
            {
                d("AnalyticsClient Log Event " + eventName);
                RI_Plugin.Instance.LogEvent(eventName, parameters);
            }
        }

        private static void d(string msg)
        {
            Debug.Log("<<ANALYTICS_CLIENT>> " + msg);
        }
    }

}
