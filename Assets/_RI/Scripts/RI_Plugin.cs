﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RI_Extension
{
    public class RI_Plugin : MonoBehaviour
    {
        private static AndroidJavaClass ajc;
        private AndroidJavaObject currentActivity;
        private AndroidJavaObject vibrator;
        private static RI_Plugin _instance;
        private float deltaTime = 0;
        private float adTimer = 90;

        public delegate void onRewardUserAdEvent();
        public static event onRewardUserAdEvent OnRewardUserEvent;

        public delegate void onReturnAdEvent();
        public static event onReturnAdEvent OnReturnFromAd;

        #region Properties
        public static RI_Plugin Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<RI_Plugin>();
                }
                return _instance;
            }
        }
        #endregion


        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else if (_instance != this)
            {
                Destroy(gameObject);
                return;
            }


            ajc = new AndroidJavaClass("com.renderedideas.riextensions.UnityWrapper");

            string s = "buildType" + "&&" + "amazon" + "&&" +
                    "flurry_key" + "&&" + "QS58KMTDJSN9XT6QBGTT" + "&&" +
                    "chartboost_app_id" + "&&" + "5ec7a18717a57109cbf9ba5c" + "&&" +
                    "chartboost_signature" + "&&" + "e25755e5d8f40298a544961aafaeac576bcaa69b" + "&&" +
                    "chartboost_start" + "&&" + "Default" + "&&" +
                    "chartboost_middle" + "&&" + "Default" + "&&" +
                    //"chartboost_end" + "&&" + "Default" + "&&" ++
                    "chartboostVideo" + "&&" + "Default" + "&&" +
                    "vungle_app_id" + "&&" + "5ec7a257719b0300011b88d8" + "&&" +
                    "vungle_start" + "&&" + "START-5669416" + "&&" +
                    "vungle_middle" + "&&" + "MIDDLE-3898655" + "&&" +
                    //"vungle_end" + "&&" + "START-5669416" + "&&" +   //no end is given
                    "vungle_video" + "&&" + "REWARDED-7536677" + "&&" +
                    "adColony_app_id" + "&&" + "appb81d0299a12e46eeb1" + "&&" +
                    "adColony_start" + "&&" + "vz37c9e24e501b406899" + "&&" +
                    "adColony_middle" + "&&" + "vz6eaf50ae60b049ed92" + "&&" +
                    "adColony_video" + "&&" + "vzbc319a2136b34275a8";


            AndroidJavaClass javaUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            currentActivity = javaUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            if (currentActivity != null)
                vibrator = currentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");

            ajc.CallStatic("initUnity", new object[] { currentActivity, s });

            //if (PlayerPrefs.GetInt("Remote_Init") != 1)
            //{
            //    Debug.Log("Car manager config");
            //    FindObjectOfType<CarManager>().AddTrialCarsToRemoteConfig();
            //    PlayerPrefs.SetInt("Remote_Init", 1);
            //}

            Debug.Log("Car manager config");
            FindObjectOfType<CarManager>().AddTrialCarsToRemoteConfig();

            AnalyticsClient.Init();
        }




        public void Update()
        {
            if (Game.Instance.launchCount > 1)
            {
                adTimer = AnalyticsClient.ads_start_timer + 100;
            }
            else
            {
                adTimer += Time.deltaTime;
            }
        }


        public void SetIsTv(string tv)
        {
            if (ajc != null)
                ajc.CallStatic("setisTv", new object[] { tv });
        }

        public void SetAdsInterval()
        {
            if (ajc != null)
                ajc.CallStatic("setAdsInterval", new object[] { });
        }

        public void SetRemoveAdsVariant()
        {
            if (ajc != null)
                ajc.CallStatic("setRemoveAdsVariant", new object[] { });
        }

        public void Log(string a)
        {
            Debug.Log("<< RI - Plugin >>\t" + a);
        }


        //++++++++++++++++++++++++++++++++++++++               AdManagerEvents                  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public void cacheAD(string spot, bool showAd)
        {
            if (!Game.Instance.isTV)
            {
                if (spot.Contains("video"))
                {
                    if (IsDownloaded("video_1"))
                    {
                        spot = "video_2";
                    }
                    else if (IsDownloaded("video_2"))
                    {
                        spot = "video_1";
                    }
                    else
                    {
                        spot = "video_1";
                    }
                    StartCoroutine(cacheADCoroutine(spot, showAd));
                    return;
                }

                Debug.Log("Game.Instance.removeAdsPurchased:  " + Game.Instance.removeAdsPurchased);
                Debug.Log("Game.Instance.isRemoveAdsVarient:  " + Game.Instance.isRemoveAdsVarient);
                if (!Game.Instance.removeAdsPurchased /*&& Game.Instance.isRemoveAdsVarient*/)
                {
                    Debug.Log("---OG---Caching Ad Unity");
                    StartCoroutine(cacheADCoroutine(spot, showAd));
                }
            }
        }

        public void showAd(string spot)
        {
            if (!Game.Instance.isTV)
            {
                if (spot.Contains("video"))
                {
                    if (IsDownloaded("video_1"))
                    {
                        spot = "video_1";
                    }
                    else if (IsDownloaded("video_2"))
                    {
                        spot = "video_2";
                    }
                    else
                    {
                        spot = "video_1";
                    }


                    if (IsDownloaded(spot))
                    {
                        StartCoroutine(showAdCoroutine(spot));
                    }
                    else cacheAD(spot, false);
                    return;
                }
                Debug.Log("Show ad: Game.Instance.removeAdsPurchased:  " + Game.Instance.removeAdsPurchased);
                Debug.Log("Show ad: Game.Instance.isRemoveAdsVarient:  " + Game.Instance.isRemoveAdsVarient);
                if (!Game.Instance.removeAdsPurchased /*&& Game.Instance.isRemoveAdsVarient*/)
                {
                    Debug.LogError("---OG---Trying to show ad!");
                    if (IsDownloaded(spot) && adTimer > AnalyticsClient.ads_start_timer)
                    {
                        Debug.LogError("---OG---Showing ad!");
                        StartCoroutine(showAdCoroutine(spot));
                    }
                    else cacheAD(spot, false);
                }
            }
        }

        private IEnumerator cacheADCoroutine(string spot, bool showAd)
        {
            if (ajc != null)
                ajc.CallStatic("cacheAd", new object[] { spot, showAd });
            yield return new WaitForEndOfFrame();
        }

        private IEnumerator showAdCoroutine(string spot)
        {
            if (ajc != null)
            {
                Debug.Log("<<RI-Plugin>>\tShowing Ad calling static \t" + spot);
                ajc.CallStatic("showAd", new object[] { spot });
            }
            else
            {
                Debug.Log("<<RI-Plugin>>\tShowing Ad calling static is null \t" + spot);
            }
            cacheAD(spot, false);
            yield return new WaitForEndOfFrame();
        }

        public bool IsDownloaded(string spot)
        {
            if (ajc != null)
            {
                return ajc.CallStatic<bool>("isDownloaded", new object[] { spot });
            }
            return false;
        }

        public void onAdShownEvent(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonAdShownEvent \t" + value);
        }

        public void onReturnFromAdEvent(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonReturnFromAdEvent \t" + value);
            OnReturnFromAd?.Invoke();
        }

        public void onReturnFromAdTimeout(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonReturnFromAdTimeout \t" + value);
            OnReturnFromAd?.Invoke();
        }

        public void onRewardUserEvent(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonRewardUserEvent \t" + value);
            OnRewardUserEvent?.Invoke();
        }

        public void onSkipUserEvent(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonSkipUserEvent \t" + value);
        }

        public void onAdDownloadEvent(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonAdDownloadEvent \t" + value);
        }

        public void onAdDownloadFailed(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonAdDownloadEvent \t" + value);
        }

        public void onNativeAdDownloadEvent(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonNativeAdDownloadEvent \t" + value);
        }

        public void onReturnFromNativeAdEvent(string value)
        {
            Debug.Log("<<RI-Plugin>>\tonReturnFromNativeAdEvent \t" + value);
        }

        //++++++++++++++++++++++++++++++++++++++               RemoteConfig                  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        public void RemoteKeys(string value)
        {
            Debug.LogError("UNITY_TRIAL_CAR: RemoteKeys " + value);
            string[] values = value.Split('$');
            try
            {
                Debug.Log("<<RI-Plugin>> \t RemoteKeys \t" + values[0] + "\t" + values[1]);
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
            AnalyticsClient.SetValues(values[0], values[1]);
        }

        public void OnRemoteConfigInitialized(string value)
        {
            Debug.LogError("UNITY_TRIAL_CAR: OnRemoteConfigInitialized " + value);
            fetchNewConfig();
        }


        public void fetchNewConfig()
        {
            StartCoroutine(fetchConfigCO());
        }

        public void setConfigVariant()
        {
            StartCoroutine(ConfigVariantCO());
        }

        public void logHomePressed(string view, string screen)
        {
            StartCoroutine(HomePressedCO(view, screen));
        }

        public void LogEvent(string eventName, Dictionary<string, string> s)
        {
            Debug.Log("<< RI - Plugin >> Logging Event " + eventName);
            StartCoroutine(logCO(eventName, s));
        }

        public void InitializeRemoteConfig(Dictionary<string, string> parameters)
        {
            StartCoroutine(InitializeRemoteCo(parameters));
        }

        public void Vibrate(long milliSeconds)
        {
            if (IsAndroid())
                vibrator.Call("vibrate", milliSeconds);
            else
                Handheld.Vibrate();
        }

        #region PromoAd

        public void InitializePromo(int gameWidth, int gameHeight, bool isTest)
        {
            Log("Initializing Promo Unity");
            StartCoroutine(PromoCo(gameWidth, gameHeight, isTest));
        }

        public void ShowPromoAd(int spot, bool force)
        {
            Log("SHOWING Promo Unity");
            StartCoroutine(PromoCo(spot, force));
        }

        public void UnhidePromoAd()
        {
            Log("Unhide Promo Unity");
            StartCoroutine(PromoCo(0));
        }

        public void HidePromoAd()
        {
            Log("Hide Promo Unity");
            StartCoroutine(PromoCo(1));
        }

        public void ClosePromoAd()
        {
            Log("Close Promo Unity");
            StartCoroutine(PromoCo(2));
        }

        public void ClickPromo()
        {
            Log("Click Promo Unity");
            StartCoroutine(PromoCo(3));
        }

        public void OnPromoAdShown(string value)
        {
            try
            {
                string[] values = value.Split(',');
                Vector4 attributes = new Vector4(float.Parse(values[1]), float.Parse(values[2]), float.Parse(values[3]), float.Parse(values[4]));
                PromoClient.SetSpotAttributes(int.Parse(values[0]), attributes);
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }

        }


        #endregion

        private bool IsAndroid()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
	        return true;
#else
            return false;
#endif
        }


        public static IEnumerator fetchConfigCO()
        {
            Debug.Log("----fetchNewConfig----");
            if (ajc != null)
                ajc.CallStatic("fetchNewConfig", new object[] { });
            yield return new WaitForEndOfFrame();
        }

        public static IEnumerator ConfigVariantCO()
        {
            yield return new WaitForEndOfFrame();
        }


        public static IEnumerator HomePressedCO(string view, string screen)
        {
            if (ajc != null)
                ajc.CallStatic("logHomePressed", new object[] { view, screen });
            yield return new WaitForEndOfFrame();
        }

        public static IEnumerator logCO(string eventName, Dictionary<string, string> s)
        {
            string ss = "";
            foreach (KeyValuePair<string, string> keyValuePair in s)
            {
                ss += keyValuePair.Key + "&&";
                ss += keyValuePair.Value + "&&";
            }
            if (ajc != null)
                ajc.CallStatic("logEvent", new object[] { eventName, ss });
            yield return new WaitForEndOfFrame();
        }

        public static IEnumerator InitializeRemoteCo(Dictionary<string, string> s)
        {
            string ss = "";
            foreach (KeyValuePair<string, string> keyValuePair in s)
            {
                ss += keyValuePair.Key + "&&";
                ss += keyValuePair.Value + "&&";
            }

            Debug.Log("UNITY_TRIAL_CAR: Remote config string: " + ss);

            if (ajc != null)
                ajc.CallStatic("initRemoteConfigParams", new object[] { ss });
            yield return new WaitForEndOfFrame();
        }

        private static IEnumerator PromoCo(int action)
        {
            if (ajc != null)
            {
                switch (action)
                {
                    case 0:
                        ajc.CallStatic("unhidePromoAd", new object[] { });
                        break;

                    case 1:
                        ajc.CallStatic("hidePromoAd", new object[] { });
                        break;

                    case 2:
                        ajc.CallStatic("closePromoAd", new object[] { });
                        break;

                    case 3:
                        ajc.CallStatic("clickPromo", new object[] { });
                        break;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        private static IEnumerator PromoCo(int spot, bool force)
        {
            if (ajc != null)
            {
                ajc.CallStatic("showPromoAd", new object[] { spot, force });
            }

            yield return new WaitForEndOfFrame();
        }

        private static IEnumerator PromoCo(int gameWidth, int gameHeight, bool isTest)
        {
            if (ajc != null)
                ajc.CallStatic("initializePromo", new object[] { gameWidth, gameHeight, isTest });

            yield return new WaitForEndOfFrame();
        }

    }

}
