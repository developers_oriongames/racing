﻿using UnityEngine;

namespace RI_Extension {
    public class Constants : MonoBehaviour {

        public const string isTV_Key = "isTV";
        public const string LaunchCount_Key = "LaunchCount";
        public const string ADSInterval_Key = "ADSInterval";
        public const string FullGamePurchased = "FullGamePurchased";
        public const string RemoveAdsPurchased = "RemoveAdsPurchased";

        #region Amazon SKUs
        public const string FULL_GAME_PURCHASED = "__test_sku_id__";

        public const string UNLOCK_ALL = "one_time_offer_orion";
        public const string UNLOCK_CARS = "unlock_cars_cr";
        public const string UNLOCK_LEVELS = "unlock_levels_cr";
        public const string REMOVE_ADS = "remove_ads_cr";

        public const string BUY_M_KOOPER = "buy_minikopper_cr";//2
        public const string BUY_P_FIREBIRD = "P_Firebird";//3
        public const string BUY_H_RS = "buy_rs_car_cr"; //4
        public const string BUY_F_FIESTA = "buy_fiesta_cr";//5
        public const string BUY_B_MW = "buy_bmw_cr";//6
        public const string BUY_C_IMPALA = "buy_imapala_cr";//7
        public const string BUY_M_BENZ_512 = "buy_benj_cr";//8
        public const string BUY_F_FOUS_RS = "buy_fous_cr";//9
        public const string BUY_D_CHALLENGER = "buy_challenger_cr";//10
        public const string BUY_F_CALIFORNIA = "buy_california_cr";//11
        public const string BUY_A_R8 = "buy_r8_cr";//12
        public const string BUY_B_CONTENTAL = "buy_contental_cr";//13
        public const string BUY_L_LP_670 = "buy_670_cr";//14
        public const string BUY_M_MP3_12C = "M_MP3_12C";//15
        #endregion

        #region Amazon Log_Event_Names
        public const string LEVEL_START_EVENT = "Level_Start";
        public const string LEVEL_FAIL_EVENT = "Level_Fail";
        public const string RESTART_LEVEL_EVENT = "Level_Restart";
        public const string LEVEL_COMPLETE_EVENT = "Level_Complete";
        #endregion
    }
}