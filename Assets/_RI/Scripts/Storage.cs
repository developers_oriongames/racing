﻿ 
using System.Collections.Generic;
using UnityEngine;

namespace RI_Extension
{
    public class Storage
    {
        public static int ReadDataInt(string key, int defaultValue)
        {
            if (PlayerPrefs.HasKey(key))
            {
                return PlayerPrefs.GetInt(key);
            }
            return defaultValue;
        }

        public static string ReadDataString(string key, string defaultValue)
        {
            if (PlayerPrefs.HasKey(key))
            {
                return PlayerPrefs.GetString(key);
            }
            return defaultValue;
        }

        public static float ReadDataFloat(string key, float defaultValue)
        {
            if (PlayerPrefs.HasKey(key))
            {
                return PlayerPrefs.GetFloat(key);
            }
            return defaultValue;
        }


        public static void SaveDataInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        public static void SaveDataString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        public static void SaveDataFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }
    }

}
