﻿ 
using System.Collections.Generic;
using UnityEngine;

namespace RI_Extension
{
    public class Utility
    {
        public static bool IsFireTV()
        {
            if (GameManager.Instance.debugTV)
            {
                return true;
            }

            if (SystemInfo.deviceModel.ToLower().Contains("aft"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}
