﻿using com.amazon.device.iap.cpt;
using System.Collections.Generic;
using UnityEngine;

namespace RI_Extension {
    public class EventHandlerClass : MonoBehaviour {

        // Obtain object used to interact with plugin
        private IAmazonIapV2 iapService = AmazonIapV2Impl.Instance;
        private EventHandlerClass EventHandler;
        private IAP iap;
        private AmazonIAPClient amazonIAP;

        private void Start() {
            EventHandler = this;
            iap = gameObject.GetComponent<IAP>();
            amazonIAP = gameObject.GetComponent<AmazonIAPClient>();
            // Register for an event
            iapService.AddGetPurchaseUpdatesResponseListener(GetPurchaseUpdatesResponseHandler);
            // Register for an event
            iapService.AddGetProductDataResponseListener(GetProductDataResponseHandler);
            // Register for an event
            iapService.AddPurchaseResponseListener(PurchaseResponseHandler);
            // Register for an event
            iapService.AddGetUserDataResponseListener(GetUserDataResponseHandler);
        }

        // Define event handler
        private void GetUserDataResponseHandler(GetUserDataResponse args) {
            string requestId = args.RequestId;
            string userId = args.AmazonUserData.UserId;
            string marketplace = args.AmazonUserData.Marketplace;
            string status = args.Status;
        }

        // Define event handler
        private void PurchaseResponseHandler(PurchaseResponse args) {
            if (args != null) {
                string requestId = args.RequestId;
                string userId = args.AmazonUserData.UserId;
                string marketplace = args.AmazonUserData.Marketplace;
                string status = args.Status;
                if (status.Equals("ALREADY_PURCHASED")) {
                    if (amazonIAP.productRequestMap != null && amazonIAP.productRequestMap.TryGetValue(requestId, out string val)) {
                        iap.PurchaseComplete(val);
                    }
                } else if (!status.Equals("SUCCESSFUL")) {
                    //IAP PURCHASE FAILED
                    iap.PurchaseFailed("", "");
                }
                string receiptId = args.PurchaseReceipt.ReceiptId;
                long cancelDate = args.PurchaseReceipt.CancelDate;
                long purchaseDate = args.PurchaseReceipt.PurchaseDate;
                string sku = args.PurchaseReceipt.Sku;
                string productType = args.PurchaseReceipt.ProductType;
                if (status.Equals("SUCCESSFUL")) {
                    Debug.Log("<<AMAZON_IAP_RI>> Purchase Response Successfull");
                    amazonIAP.NotifyFulfillment(receiptId, "FULFILLED");
                    iap.PurchaseComplete(sku);
                }

                Debug.Log("<<AMAZON_IAP_RI>> Purchase Response " + requestId + "\t" + userId + "\t" + marketplace + "\t" + receiptId + "\t" + cancelDate + "\t" + purchaseDate + "\t" + sku + "\t" + productType + "\t" + status);
                amazonIAP.GetPurchaseUpdates();
            } else {
                //IAP PURCHASE FAILED
                Debug.Log("<<AMAZON_IAP_RI>> Args null Purchase Response Failed");
                iap.PurchaseFailed("", "");
            }
        }

        // Define event handler
        private void GetProductDataResponseHandler(GetProductDataResponse args) {
            Debug.Log("<<AMAZON_IAP_RI>> Product Data Response Handler Called");
            string requestId = args.RequestId;
            Dictionary<string, ProductData> productDataMap = args.ProductDataMap;
            List<string> unavailableSkus = args.UnavailableSkus;
            string status = args.Status;
            if (iap != null) {
                Dictionary<string, string> productDict;
                foreach (string productID in iap.productIDs) {
                    if (productDataMap.ContainsKey(productID)) {
                        Debug.Log("<<AMAZON_IAP_RI>> ProductID found");
                        string sku = productDataMap[productID].Sku;
                        string productType = productDataMap[productID].ProductType;
                        string price = productDataMap[productID].Price;
                        string title = productDataMap[productID].Title;
                        string description = productDataMap[productID].Description;
                        string smallIconUrl = productDataMap[productID].SmallIconUrl;
                        productDict = new Dictionary<string, string>();
                        productDict.Add("name", title.Contains("(") ? title.Substring(title.IndexOf("(")) : title);
                        productDict.Add("price", price);
                        if (AmazonIAPClient.productInfo == null) {
                            AmazonIAPClient.productInfo = new Dictionary<string, Dictionary<string, string>>();
                        }
                        AmazonIAPClient.productInfo.Add(productID, productDict);
                        if (iap != null) {
                            foreach (IAP_Product product in iap.iap_products) {
                                if (product != null && product.ID.Equals(productID))
                                    product.price = price;
                            }
                        }
                    } else {
                        Debug.Log("<<AMAZON_IAP_RI>> ProductID not found " + productID);
                    }
                }
            }
        }
        // Define event handler
        private void GetPurchaseUpdatesResponseHandler(GetPurchaseUpdatesResponse args) {
            string requestId = args.RequestId;
            string userId = args.AmazonUserData.UserId;
            string marketplace = args.AmazonUserData.Marketplace;
            List<PurchaseReceipt> receipts = args.Receipts;
            string status = args.Status;
            bool hasMore = args.HasMore;
            // for each purchase receipt you can get the following values
            string receiptId = receipts[0].ReceiptId;
            long cancelDate = receipts[0].CancelDate;
            long purchaseDate = receipts[0].PurchaseDate;
            string sku = receipts[0].Sku;
            string productType = receipts[0].ProductType;
            GameEvents.OnRestorePurchase(receipts);
        }
    }
}
