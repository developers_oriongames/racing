﻿#define ACTIVE_RI_SDK

using GameServices.UserData;
using UnityEngine;

namespace GameServices {
	public class GameManager : MonoBehaviour {

		private static GameManager instance;

#region Global Data
		public static bool isInGamePlay = false;
		public static bool isInGameOver = false;
		public static bool isInTutorial = false;
		public static bool isInPause = false;
		public static bool isInRevival = false;
#endregion

#region Testing
#if UNITY_EDITOR
		public static bool isToSkipTutorial = true;
		public static bool isToUnlockAllCars = false;
		public static bool isToUnlockAllLevels = false;
		public static bool isToHideGameStartAnimation = false;
		public static bool isToLoadQuickly = false;
		public static bool isToAllowWrongDirection = true;
#else
		public static readonly bool isToSkipTutorial = false;
		public static readonly bool isToUnlockAllCars = false;
		public static readonly bool isToUnlockAllLevels = false;
		public static readonly bool isToHideGameStartAnimation = false;
		public static readonly bool isToLoadQuickly = false;
		public static readonly bool isToAllowWrongDirection = false;
#endif
#endregion

		/// <summary>
		/// Returns if the in-game-currency is active or otherwise.
		/// </summary>
		public static bool IsInGameCurrencyActive => false;
		public static readonly bool isToUseIAPoverAds = true;

		public static int gamePlayCount = 0;

		void Awake() {
			if (instance == null) {
				instance = this;
			}
		}

		void Start() {
			if (!UserData.UserDataManager.GetTutorialShownStatus() && !isToSkipTutorial) {
				ShowTutorial();
				GameEvents.OnPlayTutorial(Constants.DEFAULT);
			} else {
				ShowMenu();
			}
		}

		public static void AgainPlayTutorial() {
			//AudioManager.StopMusic();
			instance.ShowTutorial();
			GameEvents.OnPlayTutorial(Constants.FROM_SETTINGS);
		}

		void ShowTutorial() {
			Debug.Log("Tutorial Load");
			TrackManager.SetCurrentTrack(Tracks.DreamCity);
			GameModeManager.SetCurrentGameMode(GameModes.Classic);
			//Debug.Log("GameModeManager.GetCurrentGameMode:  " + GameModeManager.GetCurrentGameMode);
			//Debug.Log("TrackManager.CurrentTrack:  " + TrackManager.CurrentTrack);
			CarManager.SetSelectedCar(CarManager.GetTotalCarsCount() - 5); //14;
			Navigation.LoadScene(Scenes.Tutorial, GameScreen.TutorialScreen);
			isInTutorial = true;
		}

		void ShowMenu() {
			//Debug.Log("Game Load");
			Navigation.LoadScene(Scenes.Menu, GameScreen.CarSelectScreen);
			isInGamePlay = false;
		}

		public void CloseTutorial() {
			UserData.UserDataManager.SetTutorialShownStatus(true);
		}

		public static void LoadGamePlay() {
			//Debug.Log("<<<<<<<<<<<<< LoadGamePlay >>>>>>>>>>>>>>>");
			AudioManager.StopMusic();
			AudioManager.PlaySound(AudioManager.Instance.clip_play);
			UIManager.Instance.uIMenu.CloseGameMode();
			switch (TrackManager.CurrentTrack) {
				case Tracks.Desert:
					Navigation.LoadScene(Scenes.Track0, GameScreen.GameStartScreen);
					break;
				case Tracks.DesertRev:
					Navigation.LoadScene(Scenes.Track1, GameScreen.GameStartScreen);
					break;
				case Tracks.IceLand:
					Navigation.LoadScene(Scenes.Track2, GameScreen.GameStartScreen);
					break;
				case Tracks.IceLandRev:
					Navigation.LoadScene(Scenes.Track3, GameScreen.GameStartScreen);
					break;
				case Tracks.HighWay:
					Navigation.LoadScene(Scenes.Track4, GameScreen.GameStartScreen);
					break;
				case Tracks.HighWayRev:
					Navigation.LoadScene(Scenes.Track5, GameScreen.GameStartScreen);
					break;
				case Tracks.ExpressHighWay:
					Navigation.LoadScene(Scenes.Track6, GameScreen.GameStartScreen);
					break;
				case Tracks.ExpressHighWayRev:
					Navigation.LoadScene(Scenes.Track7, GameScreen.GameStartScreen);
					break;
				case Tracks.DreamCity:
					Navigation.LoadScene(Scenes.Track8, GameScreen.GameStartScreen);
					break;
			}
			isInGamePlay = true;
			LevelManager.selectedLevel = LevelManager.currentSelectedLevelNo;
			GameEvents.OnGameLevelLoad(LevelManager.selectedLevel + 1);
			gamePlayCount++;
		}

		//static int requiedStars;
		//static int requiedCarNo;
        public static void SelectLevel(int levelNo)
        {
            Debug.Log("On Click Level Button: " + levelNo);
            LevelManager.currentSelectedLevelNo = levelNo;
            switch (InputManager.GetCurrentPlatform())
            {
                case BuildPlatforms.AmazonTab:
                case BuildPlatforms.Android:
                case BuildPlatforms.IOS:
                    //requiedStars = LevelManager.LevelUnlockConditionMobile[LevelManager.currentSelectedLevelNo, 0];
                    //requiedCarNo = LevelManager.LevelUnlockConditionMobile[LevelManager.currentSelectedLevelNo, 1];
                    break;
                case BuildPlatforms.AmazonTv:
                    //requiedStars = LevelManager.LevelUnlockConditionTV[LevelManager.currentSelectedLevelNo, 0];
                    //requiedCarNo = LevelManager.LevelUnlockConditionTV[LevelManager.currentSelectedLevelNo, 1];
                    break;
            }

            //DebugMsg(requiedCarNo, requiedStars);
            if (IsToUnlockLevel())
            {
                switch (LevelManager.currentSelectedLevelNo)
                {
                    case 0:
                        TrackManager.SetCurrentTrack(Tracks.IceLand);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 1:
                        TrackManager.SetCurrentTrack(Tracks.Desert);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 2:
                        TrackManager.SetCurrentTrack(Tracks.IceLand);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 3:
                        TrackManager.SetCurrentTrack(Tracks.DesertRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 4:
                        TrackManager.SetCurrentTrack(Tracks.HighWay);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 5:
                        TrackManager.SetCurrentTrack(Tracks.HighWayRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 6:
                        TrackManager.SetCurrentTrack(Tracks.Desert);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 7:
                        TrackManager.SetCurrentTrack(Tracks.IceLandRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 8:
                        TrackManager.SetCurrentTrack(Tracks.ExpressHighWayRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 9:
                        TrackManager.SetCurrentTrack(Tracks.Desert);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 10:
                        TrackManager.SetCurrentTrack(Tracks.DreamCity);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 11:
                        TrackManager.SetCurrentTrack(Tracks.ExpressHighWay);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 12:
                        TrackManager.SetCurrentTrack(Tracks.HighWay);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 13:
                        TrackManager.SetCurrentTrack(Tracks.IceLandRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 14:
                        TrackManager.SetCurrentTrack(Tracks.HighWayRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 15:
                        TrackManager.SetCurrentTrack(Tracks.ExpressHighWay);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 16:
                        TrackManager.SetCurrentTrack(Tracks.DesertRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 17:
                        TrackManager.SetCurrentTrack(Tracks.IceLandRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 18:
                        TrackManager.SetCurrentTrack(Tracks.ExpressHighWayRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 19:
                        TrackManager.SetCurrentTrack(Tracks.DesertRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                    case 20:
                        TrackManager.SetCurrentTrack(Tracks.HighWay);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 21:
                        TrackManager.SetCurrentTrack(Tracks.IceLand);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 22:
                        TrackManager.SetCurrentTrack(Tracks.ExpressHighWay);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 23:
                        TrackManager.SetCurrentTrack(Tracks.HighWayRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 24:
                        TrackManager.SetCurrentTrack(Tracks.DreamCity);
                        GameModeManager.SetCurrentGameMode(GameModes.Versus);
                        break;
                    case 25:
                        TrackManager.SetCurrentTrack(Tracks.ExpressHighWayRev);
                        GameModeManager.SetCurrentGameMode(GameModes.Elimination);
                        break;
                    case 26:
                        TrackManager.SetCurrentTrack(Tracks.DreamCity);
                        GameModeManager.SetCurrentGameMode(GameModes.Classic);
                        break;
                }

                TrackManager.SetCurrentMapNumber();
                UIManager.Instance.uIMenu.OpenGameMode();
            }
            else if (!UserDataManager.GetUnlockAllLevelsPurchaseStatus())
            {
                Debug.Log("Buy Level");
//#if ACTIVE_RI_SDK
                UIManager.Instance.uIMenu.OpenStore(Constants.LEVEL_IS_LOCKED);
//#endif
            }
        }

        private static bool IsToUnlockLevel()
        {
            if (isToUnlockAllLevels
                || UserDataManager.GetUnlockFullGamePurchaseStatus() 
                || UserDataManager.GetUnlockAllLevelsPurchaseStatus())
            {
                return true;
            } 

            if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv
                && LevelManager.currentSelectedLevelNo < 3
                && LevelManager.IsLevelUnlocked(LevelManager.currentSelectedLevelNo))
            {
                return true;
            }
            if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTab 
                     && LevelManager.IsLevelUnlocked(LevelManager.currentSelectedLevelNo))
            {
                return true;
            }
            return false;
        }

        public static void LevelComplete() {
			isInGamePlay = false;
			if (!isInTutorial) {
                UIManager.Instance.uIGame.GameOverWork();
				Debug.Log("LevelManager.selectedLevel: " + LevelManager.selectedLevel);
			}
			if (InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv) {
				Debug.Log(">>>>>> ITS AD TIME! Showing Ads! <<<<<");
				AdManager.ShowInterstitialAutoSelect();
			}
		}

		public static void Pause() {
			if (isInTutorial) {
				return;
			}
			isInPause = true;
			AudioManager.StopCarSound();
			AudioManager.StopMusic();
			UIManager.CurrentScreen = GameScreen.PauseScreen;
			UIManager.Instance.uIGame.PauseAnimation();
			GameEvents.OnPauseScreen();
			Time.timeScale = 0;
		}

		public static void Resume() {
			UIManager.Instance.uIGame.Resume();
			Time.timeScale = 1;
			isInPause = false;
		}

		public static void Restart() {
			AudioManager.StopMusic();
			UIManager.Instance.uIGame.Restart();
			Time.timeScale = 1;
			isInPause = false;
			gamePlayCount++;
			isInGamePlay = true;
		}

        static void DebugMsg(int requiedCarNo, int requiedStars)
        {
            Debug.Log("UserDataManager.isToUnlockAllLevels: "
                      + isToUnlockAllLevels);
            Debug.Log("(GameShop.GetTotalStar() >= requiedStars: "
                      + (LevelManager.GetTotalStar() >= requiedStars));
            Debug.Log("GameShop.GetTotalStar() "
                      + LevelManager.GetTotalStar());
            Debug.Log("GameShop.UnlockCondition[GameShop.mLevNo, 0] "
                      + requiedStars);
            Debug.Log("GameShop.mCarPrize[CarNo] == 0: "
                      + (CarManager.Instance.cars[requiedCarNo].GetInGameCarPrice() == 0));
            Debug.Log("GameShop.mCarPrize[CarNo]: "
                      + CarManager.Instance.cars[requiedCarNo].GetInGameCarPrice());
            Debug.Log("UserData.UserDataManager.GetUnlockAllLevelsPurchaseStatus(): "
                      + UserData.UserDataManager.GetUnlockAllLevelsPurchaseStatus());
        }
    }
}