﻿#define ACTIVE_RI_SDK

using UnityEngine;
using System;
#if ACTIVE_RI_SDK
using RI_Extension;
using com.amazon.device.iap.cpt;
#endif
using GameServices.UserData;
using System.Collections.Generic;

public enum InAppPurchaseType {
    None = 0,
    Consumable = 1,
    NonConsumable = 2,
}

public enum ConsumableIAPItems {
    NONE,
    FREE,
    CASH_BUNDLE_1,
    CASH_BUNDLE_2,
    CASH_BUNDLE_3,
}

public enum NonConsumableIAPItems {
    NONE,
    UNLOCK_ALL,
    UNLOCK_CARS,
    UNLOCK_LEVELS,
    REMOVE_ADS,

    BUY_M_KOOPER,
    BUY_P_FIREBIRD,
    BUY_H_RS,
    BUY_F_FIESTA,
    BUY_B_MW,
    BUY_C_IMPALA,
    BUY_M_BENZ_512,
    BUY_F_FOUS_RS,
    BUY_D_CHALLENGER,
    BUY_F_CALIFORNIA,
    BUY_A_R8,
    BUY_B_CONTENTAL,
    BUY_L_LP_670,
    BUY_M_MP3_12C,
    FREE,
}

namespace GameServices {
    public class PurchaseManager : MonoBehaviour {

        static PurchaseManager instance;

        void Awake() {
            if (instance == null) {
                instance = this;
            }
#if ACTIVE_RI_SDK
            GameEvents.OnRestorePurchase += OnRestorePurchase;
            GameEvents.OnPurchaseComplete += OnPurchaseComplete;
            GameEvents.OnPurchaseFailed += OnPurchaseFailed;
#endif
        }

        void OnDestroy() {
#if ACTIVE_RI_SDK
            GameEvents.OnRestorePurchase -= OnRestorePurchase;
            GameEvents.OnPurchaseComplete -= OnPurchaseComplete;
            GameEvents.OnPurchaseFailed -= OnPurchaseFailed;
#endif
        }

        #region In Game
        /// <summary>
        /// Purchase car with in game currency
        /// </summary>
        public static void CarPurchase() {
            if (GameManager.IsInGameCurrencyActive) {
                CurrencyManager.TakeMoney(CarManager.GetCurrentSelectedCar().GetInGameCarPrice());
            }
            AudioManager.PlayBuySound();
            CarManager.GetCurrentSelectedCar().SetPurchaseStatus(true);
            CarManager.SelectCar();
            CarManager.SaveCarData();
        }
        #endregion

        #region In App Purchase
        /// <summary>
        /// Buys a non-consumable item from store 
        /// </summary>
        /// <param name="item"></param>
        public static void BuyNonConsumableItem(NonConsumableIAPItems item) {
            GameEvents.OnPurchaseStarted(GetSKUIDs(item));
#if UNITY_EDITOR
            OnPurchaseComplete(GetSKUIDs(item));
#else
            PurchaseWithAmazonStore(GetSKUIDs(item));
#endif
        }

        /// <summary>
        /// In order to purchase a particular iap product, you need to call the ​Purchase()​ method of  AmazonIAPClient​ object.
        /// </summary>
        /// <param name="productSKU"></param>
        static void PurchaseWithAmazonStore(string productSKU) {
#if ACTIVE_RI_SDK
            AmazonIAPClient amazonIAP = FindObjectOfType<AmazonIAPClient>();
            amazonIAP.Purchase(productSKU);
#endif
        }

        /// <summary>
        /// Returns if the non-consumable product is purchased
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool IsPurchased(NonConsumableIAPItems item) {
#if ACTIVE_RI_SDK
            return IAP.Instance.IsPurchased(GetSKUIDs(item));
#else
            return true;
#endif
        }

        #region Event Listeners
        static void OnPurchaseComplete(string skuId) {
            NonConsumableIAPItems item = GetNonConsumableIAPItems(skuId);
            switch (item) {
                case NonConsumableIAPItems.UNLOCK_ALL:
                    GiveUnlockFullGame();
                    break;
                case NonConsumableIAPItems.UNLOCK_CARS:
                    GiveUnlockAllCars();
                    break;
                case NonConsumableIAPItems.UNLOCK_LEVELS:
                    GiveUnlockAllLevels();
                    break;
                case NonConsumableIAPItems.REMOVE_ADS:
                    GiveRemoveAds();
                    break;
                case NonConsumableIAPItems.BUY_M_KOOPER:
                case NonConsumableIAPItems.BUY_P_FIREBIRD:
                case NonConsumableIAPItems.BUY_H_RS:
                case NonConsumableIAPItems.BUY_F_FIESTA:
                case NonConsumableIAPItems.BUY_B_MW:
                case NonConsumableIAPItems.BUY_C_IMPALA:
                case NonConsumableIAPItems.BUY_M_BENZ_512:
                case NonConsumableIAPItems.BUY_F_FOUS_RS:
                case NonConsumableIAPItems.BUY_D_CHALLENGER:
                case NonConsumableIAPItems.BUY_F_CALIFORNIA:
                case NonConsumableIAPItems.BUY_A_R8:
                case NonConsumableIAPItems.BUY_B_CONTENTAL:
                case NonConsumableIAPItems.BUY_L_LP_670:
                case NonConsumableIAPItems.BUY_M_MP3_12C:
                    GiveCar(item);
                    break;
            }
        }
        static void OnPurchaseFailed(string arg1, string arg2) {

        }
#if ACTIVE_RI_SDK
        static void OnRestorePurchase(List<PurchaseReceipt> receipts) {
            foreach (PurchaseReceipt receipt in receipts) {
                //Debug.LogError("__AMAZON__GetPurchaseUpdatesResponseHandler receipt[0] sku" + receipt.Sku);
                //Debug.LogError("__AMAZON__GetPurchaseUpdatesResponseHandler receipt[0] status" + receipt.ProductType);
                
                //******************* Amazon TV ******************///
                //if (string.Equals(receipt.Sku, RI_Extension.Constants.FULL_GAME_PURCHASED, StringComparison.Ordinal)) {
                //    Game.Instance.fullGamePurchased = true;//RI
                //    Storage.SaveDataString(RI_Extension.Constants.FullGamePurchased, Game.Instance.fullGamePurchased + "");//RI
                //}

                //******************* Amazon Tab ******************///
                //******************* Store Non-consumables ******************///
                if (string.Equals(receipt.Sku, RI_Extension.Constants.REMOVE_ADS, StringComparison.Ordinal)) {
                    GiveRemoveAds();
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.UNLOCK_ALL, StringComparison.Ordinal)) {
                    GiveUnlockFullGame();
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.UNLOCK_CARS, StringComparison.Ordinal)) {
                    GiveUnlockAllCars();
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.UNLOCK_LEVELS, StringComparison.Ordinal)) {
                    GiveUnlockAllLevels();
                }
                //******************* Cars Non-consumables ******************///
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_M_KOOPER, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_M_KOOPER);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_P_FIREBIRD, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_P_FIREBIRD);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_H_RS, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_H_RS);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_F_FIESTA, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_F_FIESTA);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_B_MW, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_B_MW);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_C_IMPALA, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_C_IMPALA);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_M_BENZ_512, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_M_BENZ_512);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_F_FOUS_RS, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_F_FOUS_RS);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_D_CHALLENGER, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_D_CHALLENGER);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_F_CALIFORNIA, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_F_CALIFORNIA);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_A_R8, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_A_R8);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_B_CONTENTAL, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_B_CONTENTAL);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_L_LP_670, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_L_LP_670);
                }
                if (string.Equals(receipt.Sku, RI_Extension.Constants.BUY_M_MP3_12C, StringComparison.Ordinal)) {
                    GiveCar(NonConsumableIAPItems.BUY_M_MP3_12C);
                }
            }
        }
#endif
        #endregion

        #region Convertion Methods
        /// <summary>
        /// Converts product skus to non-consumable items
        /// </summary>
        /// <param name="sku"></param>
        /// <returns>returns converted non-consumable items</returns>
        static NonConsumableIAPItems GetNonConsumableIAPItems(string sku) {
#if ACTIVE_RI_SDK
            switch (sku) {
                case RI_Extension.Constants.UNLOCK_ALL:
                    return NonConsumableIAPItems.UNLOCK_ALL;
                case RI_Extension.Constants.UNLOCK_CARS:
                    return NonConsumableIAPItems.UNLOCK_CARS;
                case RI_Extension.Constants.UNLOCK_LEVELS:
                    return NonConsumableIAPItems.UNLOCK_LEVELS;
                case RI_Extension.Constants.REMOVE_ADS:
                    return NonConsumableIAPItems.REMOVE_ADS;
                case RI_Extension.Constants.BUY_M_KOOPER:
                    return NonConsumableIAPItems.BUY_M_KOOPER;
                case RI_Extension.Constants.BUY_P_FIREBIRD:
                    return NonConsumableIAPItems.BUY_P_FIREBIRD;
                case RI_Extension.Constants.BUY_H_RS:
                    return NonConsumableIAPItems.BUY_H_RS;
                case RI_Extension.Constants.BUY_F_FIESTA:
                    return NonConsumableIAPItems.BUY_F_FIESTA;
                case RI_Extension.Constants.BUY_B_MW:
                    return NonConsumableIAPItems.BUY_B_MW;
                case RI_Extension.Constants.BUY_C_IMPALA:
                    return NonConsumableIAPItems.BUY_C_IMPALA;
                case RI_Extension.Constants.BUY_M_BENZ_512:
                    return NonConsumableIAPItems.BUY_M_BENZ_512;
                case RI_Extension.Constants.BUY_F_FOUS_RS:
                    return NonConsumableIAPItems.BUY_F_FOUS_RS;
                case RI_Extension.Constants.BUY_D_CHALLENGER:
                    return NonConsumableIAPItems.BUY_D_CHALLENGER;
                case RI_Extension.Constants.BUY_F_CALIFORNIA:
                    return NonConsumableIAPItems.BUY_F_CALIFORNIA;
                case RI_Extension.Constants.BUY_A_R8:
                    return NonConsumableIAPItems.BUY_A_R8;
                case RI_Extension.Constants.BUY_B_CONTENTAL:
                    return NonConsumableIAPItems.BUY_B_CONTENTAL;
                case RI_Extension.Constants.BUY_L_LP_670:
                    return NonConsumableIAPItems.BUY_L_LP_670;
                case RI_Extension.Constants.BUY_M_MP3_12C:
                    return NonConsumableIAPItems.BUY_M_MP3_12C;
                default:
                    return NonConsumableIAPItems.NONE;
            }
#else 
            return NonConsumableIAPItems.NONE;
#endif
        }
        /// <summary>
        /// Converts non-consumable items to product skus
        /// </summary>
        /// <param name="item"></param>
        /// <returns>returns converted products skus</returns>
        static string GetSKUIDs(NonConsumableIAPItems item) {
#if ACTIVE_RI_SDK
            switch (item) {
                case NonConsumableIAPItems.UNLOCK_ALL:
                    return RI_Extension.Constants.UNLOCK_ALL;
                case NonConsumableIAPItems.UNLOCK_CARS:
                    return RI_Extension.Constants.UNLOCK_CARS;
                case NonConsumableIAPItems.UNLOCK_LEVELS:
                    return RI_Extension.Constants.UNLOCK_LEVELS;
                case NonConsumableIAPItems.REMOVE_ADS:
                    return RI_Extension.Constants.REMOVE_ADS;
                case NonConsumableIAPItems.BUY_M_KOOPER:
                    return RI_Extension.Constants.BUY_M_KOOPER;
                case NonConsumableIAPItems.BUY_P_FIREBIRD:
                    return RI_Extension.Constants.BUY_P_FIREBIRD;
                case NonConsumableIAPItems.BUY_H_RS:
                    return RI_Extension.Constants.BUY_H_RS;
                case NonConsumableIAPItems.BUY_F_FIESTA:
                    return RI_Extension.Constants.BUY_F_FIESTA;
                case NonConsumableIAPItems.BUY_B_MW:
                    return RI_Extension.Constants.BUY_B_MW;
                case NonConsumableIAPItems.BUY_C_IMPALA:
                    return RI_Extension.Constants.BUY_C_IMPALA;
                case NonConsumableIAPItems.BUY_M_BENZ_512:
                    return RI_Extension.Constants.BUY_M_BENZ_512;
                case NonConsumableIAPItems.BUY_F_FOUS_RS:
                    return RI_Extension.Constants.BUY_F_FOUS_RS;
                case NonConsumableIAPItems.BUY_D_CHALLENGER:
                    return RI_Extension.Constants.BUY_D_CHALLENGER;
                case NonConsumableIAPItems.BUY_F_CALIFORNIA:
                    return RI_Extension.Constants.BUY_F_CALIFORNIA;
                case NonConsumableIAPItems.BUY_A_R8:
                    return RI_Extension.Constants.BUY_A_R8;
                case NonConsumableIAPItems.BUY_B_CONTENTAL:
                    return RI_Extension.Constants.BUY_B_CONTENTAL;
                case NonConsumableIAPItems.BUY_L_LP_670:
                    return RI_Extension.Constants.BUY_L_LP_670;
                case NonConsumableIAPItems.BUY_M_MP3_12C:
                    return RI_Extension.Constants.BUY_M_MP3_12C;
                default:
                    return null;
            }
#else
            return null;
#endif
        }
#endregion

#region Purchase Rewards Items
        static void GiveCash(int amount) {
            CurrencyManager.GiveMoney(amount);
            GameEvents.OnItemPurchased(IAPButtonType.Store);
        }

        static void GiveCar(NonConsumableIAPItems item) {
            GiveRemoveAds();
            // we can give car by currently selected car
            //CarManager.GetCurrentSelectedCar().SetPurchaseStatus(true); 
            // or we can check the purchased product and unlock the corrospondent car
            for (int i = 0; i < CarManager.GetTotalCarsCount(); i++) {
                if (item == CarManager.Instance.cars[i].GetAmazonIAPItem()) {
                    CarManager.Instance.cars[i].SetPurchaseStatus(true);
                    CarManager.SetCurrentSelectedCar(i);
                    CarManager.SetSelectedCar(i);
                }
            }
            CarManager.SelectCar();
            CarManager.SaveCarData();
            GameEvents.OnItemPurchased(IAPButtonType.Others);
        }

        static void GiveRemoveAds() {
            UserDataManager.SetRemoveAdsPurchaseStatus(true);
            AdManager.UpdateRemoveAdsStatus();
#if ACTIVE_RI_SDK
            Game.Instance.fullGamePurchased = true;//RI
            Storage.SaveDataString(RI_Extension.Constants.FullGamePurchased, Game.Instance.fullGamePurchased + "");//RI
#endif
            GameEvents.OnItemPurchased(IAPButtonType.Store);
        }

        public static void GiveUnlockFullGame() {
            GiveRemoveAds();
            GiveUnlockAllCars();
            GiveUnlockAllLevels();
            UserDataManager.SetUnlockFullGamePurchaseStatus(true);
#if ACTIVE_RI_SDK
            Game.Instance.fullGamePurchased = true;//RI
            Storage.SaveDataString(RI_Extension.Constants.FullGamePurchased, Game.Instance.fullGamePurchased + "");//RI
#endif
            GameEvents.OnItemPurchased(IAPButtonType.Store);
        }

        static void GiveUnlockAllCars() {
            GiveRemoveAds();
            for (int i = 0; i < CarManager.GetTotalCarsCount(); i++) {
                CarManager.Instance.cars[i].SetPurchaseStatus(true);
            }
            CarManager.SaveCarData();
            UserDataManager.SetUnlockAllCarsPurchaseStatus(true);
            GameEvents.OnItemPurchased(IAPButtonType.Store);
        }

        static void GiveUnlockAllLevels() {
            GiveRemoveAds();
            UserDataManager.SetUnlockAllLevelsPurchaseStatus(true);
            GameEvents.OnItemPurchased(IAPButtonType.Store);
            LevelManager.UnlockAllLevels();
        }
#endregion
#endregion
    }
}