﻿using GameServices;
using GameServices.UserData;
using System.Collections.Generic;
using UnityEngine;

public class CarManager : MonoBehaviour
{
	public static CarManager Instance;
	public Car[] cars = new Car[15];

	public Car currentSelectedCar;
	public Car selectedCar;

	public static TrialCars trialCars 
	{ 
		get
        {
			if(Instance == null)
            {
				Instance = FindObjectOfType<CarManager>();
            }

			if (m_trialCars == null)
            {			
				m_trialCars = new TrialCars(Instance);
            }

			return m_trialCars;
        }
	}
	private static TrialCars m_trialCars;

	

	/*
	public static int[] mCarPrize = {
		0,
		9500,
		13500,
		26800,
		35300,
		46500,
		60000,
		65000,
		78000,
		88000,
		100000,
		112500,
		135000,
		150000,
		175000,
	};

	public static float[] mCarStorePrize = {
		0,
		0.90f,
		1.35f,
		2.68f,
		3.53f,
		4.65f,
		6.00f,
		6.50f,
		7.80f,
		8.80f,
		10.00f,
		11.25f,
		13.50f,
		15.00f,
		17.50f,
	};

	public static string[] CarName = {
		"P_207",
		"M_Kooper",
		"P_Firebird",
		"H_RS",
		"F_Fiesta",
		"B_MW",
		"C_Impala",
		"M_Benz_512",
		"F_Fous_RS",
		"D_Challenger",
		"F_California",
		"A_R8",
		"B_Contnental",
		"L_LP_670",
		"M_MP3_12C"
	};

	public static float[] Acceleration = {
		4, 
		4.5f,
		5,
		5.5f,
		6,
		6.5f,
		7,
		8,
		9,
		9.5f,
		10,
		11,
		11.5f,
		12,
		13 
	};    //{ 3, 3.5f, 4f, 4.5f, 6f, 6.5f, 7, 8, 9, 9.5f, 10, 11, 11.5f, 12, 13f };
	public static float[] TopSpeed = { 
		185, 
		195, 
		200, 
		215,
		222,
		230,
		240,
		252,
		259, 
		272,
		285, 
		298,
		310, 
		322,
		330 
	}; //{ 180, 187f, 210, 225, 235, 242, 256, 268, 279, 292, 305, 318, 330, 340, 349.5f };
	public static float[] Handling = { 
		0.4f, 
		0.43f, 
		0.48f,
		0.52f,
		0.55f, 
		0.6f,
		0.63f,
		0.68f, 
		0.71f,
		0.75f,
		0.78f,
		0.8f,
		0.84f,
		0.88f, 
		0.92f 
	}; //{ 0.4f, 0.43f, 0.48f, 0.52f, 0.55f, 0.6f, 0.63f, 0.68f, 0.71f, 0.75f, 0.78f, 0.8f, 0.84f, 0.88f, 0.92f };
	public static float[] Nitro = {
		0.013f,
		0.015f, 
		0.017f, 
		0.018f,
		0.02f, 
		0.025f, 
		0.03f,
		0.035f,
		0.04f,
		0.045f,
		0.05f, 
		0.06f,
		0.08f,
		0.09f, 
		0.1f 
	}; 
	//{ 0.01f, 0.013f, 0.017f, 0.018f, 0.02f, 0.025f, 0.03f, 0.035f, 0.04f, 0.045f, 0.05f, 0.06f, 0.08f, 0.09f, 0.1f };
	*/

	void Awake() {
		if (Instance == null) {
			Instance = this;
		}
		currentSelectedCar = cars[0];
		selectedCar = cars[0];
    }

	static void ForInstance(CarManager carManager)
    {
		if (Instance == null)
		{
			Instance = carManager;
		}
	}

	void Start() {
		SetCurrentSelectedCar(UserDataManager.GetLastSelectedCar());
		//cars[0].SetPurchaseStatus(true); //give default car
		UserDataManager.SaveCarUnlockStatusData("0", true);//give default car
		//SaveCarData();
		GetCarData();
	}
	/// <summary>
	/// Gets the total car count -> cars.Length
	/// </summary>
	/// <returns></returns>
	public static int GetTotalCarsCount() {
		return Instance.cars.Length;
	}
	/// <summary>
	/// Get the current selected car in UI
	/// </summary>
	/// <returns></returns>
	public static Car GetCurrentSelectedCar() {
		//return Instance.cars[mCurrentCarSelected];
		return Instance.currentSelectedCar;
	}

	public static void SetCurrentSelectedCar(int i) {
		//mCurrentCarSelected = i;
		Instance.currentSelectedCar = Instance.cars[i];
	} 

	/// <summary>
	/// Get the selected car (default or unlocked) which can be used in game play
	/// </summary>
	/// <returns></returns>
	public static Car GetSelectedCar() {
		//return Instance.cars[mCarSelected];
		return Instance.selectedCar;
	}

	public static void SetSelectedCar(int i) {
		//mCarSelected = i;
		Instance.selectedCar = Instance.cars[i];
	}


	/// <summary>
	/// Buy Car
	/// </summary>
	public static void BuyCar(NonConsumableIAPItems item) {
        //Car currentSelectedCar = CarManager.GetCurrentSelectedCar();
        if (GameManager.IsInGameCurrencyActive) {                                                           //Check if the in-game-currency is active
            if (!Instance.currentSelectedCar.GetPurchaseStatus()) {											//Check if the car has already bought
				if (GameManager.isToUnlockAllCars) {														//Is to unlock all cars for Testing
					AudioManager.PlayBuySound();
					Instance.currentSelectedCar.SetPurchaseStatus(true);
					SelectCar();
					SaveCarData();
				} else {																					//Normal case: For release builds
					if (Instance.currentSelectedCar.GetInGameCarPrice() < CurrencyManager.TotalCash) {      //Having enough cash for the purchase //Actual purchase with in game currency
						PurchaseManager.CarPurchase();
					} else {																				//Not having enough cash for the purchase
						if (GameManager.isToUseIAPoverAds) {                                                //Show IAP to buy cash offer
							UIManager.Instance.uIMenu.OpenStore(Constants.NOT_ENOUGH_CASH);
						} else {																			//Show rewarded Ad offer
							UIManager.Instance.uIMenu.ShowAdsPopup();
						}
					}
				}
			}
		} else {                                                                                //Check if the in-game-currency is de-active
			PurchaseManager.BuyNonConsumableItem(item);                                                           //Actual purchase with IAP
		}
	}

	public static void SelectCar() {
		SetSelectedCar(GetCurrentSelectedCar().GetCarID());
		UserDataManager.SaveLastSelectedCar(GetSelectedCar().GetCarID()); //TODO
	}

	public static void GetCarData() {
		for (int i = 0; i < GetTotalCarsCount(); i++) {
			Instance.cars[i].SetPurchaseStatus(UserDataManager.GetCarUnlockStatusData(i.ToString()));
		}
	}

	public static void SaveCarData() {
		for (int i = 0; i < GetTotalCarsCount(); i++) {
			UserDataManager.SaveCarUnlockStatusData(i.ToString(), Instance.cars[i].GetPurchaseStatus());
		}
	}

	public static void ResetCars() {
		for (int i = 0; i < Instance.cars.Length; i++) {
			if (i == 0) { continue; }
			Instance.cars[i].SetPurchaseStatus(false);
		}
	}

	public void AddTrialCarsToRemoteConfig()
    {
		Dictionary<string, string> tempDictionary = TrialCars.GetPreparedDictionary();
		RI_Extension.RI_Plugin.Instance.InitializeRemoteConfig(tempDictionary);
    }
}
