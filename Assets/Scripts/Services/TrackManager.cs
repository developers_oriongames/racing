﻿using UnityEngine;

namespace GameServices {
	public enum Tracks {
		Desert = 0,
		DesertRev = 1,
		IceLand = 2,
		IceLandRev = 3,
		HighWay = 4,
		HighWayRev = 5,
		ExpressHighWay = 6,
		ExpressHighWayRev = 7,
		DreamCity = 8,
	}

	public class TrackManager : MonoBehaviour {

		public static TrackManager Instance;

		#region Tracks
		private static Tracks currentTrack = Tracks.Desert;
		public static Tracks CurrentTrack {
			get => currentTrack;
		}
		public static void SetCurrentTrack(Tracks track) {
			currentTrack = track;
		}
		#endregion

		void Awake() {
			if (Instance == null) {
				Instance = this;
			}
		}

		#region Map
		static int currentMapNumber = 0;
		public static int GetCurrentMapNumber() {
			return currentMapNumber;
		}

		public static void SetCurrentMapNumber() {
			int mapNumber;
			switch (CurrentTrack) {
				case Tracks.Desert:
				case Tracks.DesertRev:
					mapNumber = 0;
					break;
				case Tracks.IceLand:
				case Tracks.IceLandRev:
					mapNumber = 1;
					break;
				case Tracks.HighWay:
				case Tracks.HighWayRev:
					mapNumber = 2;
					break;
				case Tracks.ExpressHighWay:
				case Tracks.ExpressHighWayRev:
					mapNumber = 3;
					break;
				case Tracks.DreamCity:
					mapNumber = 4;
					break;
				default:
					mapNumber = 0;
					break;
			}
			currentMapNumber = mapNumber;
		}
		#endregion
	}
}