﻿#define ACTIVE_RI_SDK

using UnityEngine;
using System.Collections;


#if ACTIVE_RI_SDK
using RI_Extension;
#endif
namespace GameServices {
	public enum AdSpot {
		start = 0,  //- Eg:This spot can be used to show Ad before a LEVEL STARTS.
		middle = 1, //- Eg:This spot can be used when a player dies or when a LEVEL IS COMPLETED.
		end = 2,    //- Eg:This spot can be used when the APP IS EXITED.
		video = 3,  //- Video ad for REWARDED VIDEO.
	}

    public enum PromoSpot {
        SPOT_MENU = 0,
        SPOT_LEVEL_SELECT = 1,
        SPOT_CHARACTER_SELECT = 2,
        SPOT_LEVEL_LOADING = 3,
        SPOT_LEVEL_PAUSE = 4,
        SPOT_LEVEL_COMPLETE = 5,
        SPOT_LEVEL_OVER = 6,
        SPOT_LEVEL_HELP = 7,
        SPOT_LEVEL_CREDITS = 8,
    }

    public class AdManager : MonoBehaviour {

        public static AdManager instance;
        public static bool isAdsRemoved;

        public enum RewardAdState
        {
            TRY_THIS_CAR = 0,
            IN_GAME_BOOST = 1
        }

        public RewardAdState rewardAdState;

        void Awake() {
            if (instance == null) {
                instance = this;
            }
        }

        void OnEnable() {
            //GameEvents.OnSceneLoadFinished += ShowPromoAd;
            //GameEvents.OnSceneLoadStarted += HidePromoAd;
        }

        void OnDisable() {
            //GameEvents.OnSceneLoadFinished -= ShowPromoAd;
            //GameEvents.OnSceneLoadStarted -= HidePromoAd;
        }

        void Start() {
            if (UserData.UserDataManager.GetRemoveAdsPurchaseStatus()) {
                UpdateRemoveAdsStatus();
            }
#if ACTIVE_RI_SDK
            RI_Plugin.OnReturnFromAd += OnReturnFromAd​;
            RI_Plugin.OnRewardUserEvent += OnRewardUserEvent​;

#endif
            CacheAD();
        }

        static void CacheAD() {
            DownloadAd(AdSpot.start);
            DownloadAd(AdSpot.middle);
            DownloadAd(AdSpot.end);
            DownloadAd(AdSpot.video);
        }

        /// <summary>
        /// The onReturnFromAd event is called when the ad has completed and shown successfully
        /// </summary>
        void OnReturnFromAd() {
            //Time.timeScale = 1;
            //close ui or somthing after completing the ad		//TODO
            DownloadAd(AdSpot.video);   //Cache rewarded video ad for next time use
        }

        /// <summary>
        /// you can use the method to perform tasks after showing a rewarded ad. Reward the user in this event callback.  
        /// </summary>
        void OnRewardUserEvent() 
        {
            if (rewardAdState == RewardAdState.TRY_THIS_CAR)
            {
                UIManager.Instance.uIMenu.carSelectionPanel.GiveRewardGiftCar();
            }
            else
            {
                GameController.Instance.carUserControl.boostVal = 1f;
                //Time.timeScale = 1;
            }   
            // reward player for viewing the ad			//TODO
            DownloadAd(AdSpot.video);   //Cache rewarded video ad for next time use // should we cache again ??
        }

        static void DownloadAd(AdSpot adSpot) {
            if (!isAdsRemoved) {
#if ACTIVE_RI_SDK
                RI_Plugin.Instance.cacheAD(adSpot.ToString(), false);
#endif
            }
        }
        static bool IsAdLoaded(AdSpot adSpot) {
            if (!isAdsRemoved) {
#if ACTIVE_RI_SDK
                //Debug.Log("RI_Plugin.Instance.IsDownloaded(adSpot.ToString()):  " 
                //    + RI_Plugin.Instance.IsDownloaded(adSpot.ToString()));
                if (RI_Plugin.Instance.IsDownloaded(adSpot.ToString()))
                {
                    return true;
                }
                else {
                    DownloadAd(adSpot);
                    return false;
                }
                
                //return RI_Plugin.Instance.IsDownloaded(adSpot.ToString());
#endif
            }
            return false;
        }
        static void ShowAd(AdSpot adSpot) {
            if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv)
            {
                return;
            }
            if (!isAdsRemoved && IsAdLoaded(adSpot)) {
#if ACTIVE_RI_SDK
                Debug.LogError("---OG--- Ad is loaded!");
                RI_Plugin.Instance.showAd(adSpot.ToString());
#endif
            }

            Debug.LogError("---OG---Ad is not loaded or remove ad purchased!");

            CacheAD();
            Debug.Log("//AD Start------------->>>");
            Debug.Log("isAdsRemoved:  " + isAdsRemoved);
            Debug.Log("adSpot:  " + adSpot);
            Debug.Log("IsAdLoaded(adSpot):  " + IsAdLoaded(adSpot));
            Debug.Log("//AD End------------->>>");
        }

        #region public methods
        public static bool IsRewardedVideoAdsLoaded() { return IsAdLoaded(AdSpot.video); }
        public static bool IsInstastatialAdsLoaded(AdSpot adSpot) { return IsAdLoaded(adSpot); }


        IEnumerator AdsNotAvailablePopup() {
            UIManager.Instance.uIGame.gamePlayPanel.RewardAdNotAvailableTextEnable(true);
            yield return new WaitForSeconds(2f);
            UIManager.Instance.uIGame.gamePlayPanel.RewardAdNotAvailableTextEnable(false);
        }

        IEnumerator AdsNotAvailablePopup_menu()
        {
            UIManager.Instance.uIMenu.carSelectionPanel.RewardAdNotAvailableTextEnable(true);
            yield return new WaitForSeconds(2f);
            UIManager.Instance.uIMenu.carSelectionPanel.RewardAdNotAvailableTextEnable(false);
        }

        public void ShowRewardedVideo() {
            #if UNITY_ANDROID && !UNITY_EDITOR  
                if (RI_Plugin.Instance.IsDownloaded("video_1") || RI_Plugin.Instance.IsDownloaded("video_2"))
                {
                    RI_Plugin.Instance.showAd("video");
                }
                else {
                    RI_Plugin.Instance.cacheAD("video", false);
                    if (rewardAdState == RewardAdState.TRY_THIS_CAR)
                    {
                        if (!UIManager.Instance.uIMenu.carSelectionPanel.rewardAdNotAvailableText.gameObject.activeInHierarchy)
                        {
                            StartCoroutine(AdsNotAvailablePopup_menu());
                        }
                    }
                    else
                    {
                        if (!UIManager.Instance.uIGame.gamePlayPanel.RewardAdNotAvailableTextIsActive())
                        {
                            StartCoroutine(AdsNotAvailablePopup());
                        }
                    }
                }
                
            #elif UNITY_EDITOR
                if (rewardAdState == RewardAdState.TRY_THIS_CAR) 
                {
                    UIManager.Instance.uIMenu.carSelectionPanel.GiveRewardGiftCar();
                } 
                else 
                {
                    GameController.Instance.carUserControl.boostVal = 1f;
                }          
            #endif
        }

        public static void ShowInterstitialLevelStart() { ShowAd(AdSpot.start); }
        public static void ShowInterstitialLevelMiddle() { ShowAd(AdSpot.middle); }
        public static void ShowInterstitialLevelEnd() { ShowAd(AdSpot.start); }   // previously it was AdSpot.end
        public static void ShowInterstitialAutoSelect() {
            if (IsInstastatialAdsLoaded(AdSpot.start)) {
                ShowAd(AdSpot.start);
                return;
            } 
            if (IsInstastatialAdsLoaded(AdSpot.middle)) {
                ShowAd(AdSpot.middle);
                return;
            } 
            if (IsInstastatialAdsLoaded(AdSpot.end)) {
                ShowAd(AdSpot.end);
                return;
            }
        }
        public static void UpdateRemoveAdsStatus() { isAdsRemoved = true; }
#endregion


#region Promo Ad
        /// <summary>
        /// Show Promo ad in amazon tab
        /// </summary>
        public static void ShowPromoAd(PromoSpot promoSpot) {
#if ACTIVE_RI_SDK
            if (isAdsRemoved) { return; }
            if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) { return; }
            int spotNo = 0;
            switch (promoSpot) {
                case PromoSpot.SPOT_MENU:
                    spotNo = PromoClient.SPOT_MENU;
                    break;
                case PromoSpot.SPOT_LEVEL_SELECT:
                    spotNo = PromoClient.SPOT_LEVEL_SELECT;
                    break;
                case PromoSpot.SPOT_CHARACTER_SELECT:
                    spotNo = PromoClient.SPOT_CHARACTER_SELECT;
                    break;
                case PromoSpot.SPOT_LEVEL_LOADING:
                    spotNo = PromoClient.SPOT_LEVEL_LOADING;
                    break;
                case PromoSpot.SPOT_LEVEL_PAUSE:
                    spotNo = PromoClient.SPOT_LEVEL_PAUSE;
                    break;
                case PromoSpot.SPOT_LEVEL_COMPLETE:
                    spotNo = PromoClient.SPOT_LEVEL_COMPLETE;
                    break;
                case PromoSpot.SPOT_LEVEL_OVER:
                    spotNo = PromoClient.SPOT_LEVEL_OVER;
                    break;
                case PromoSpot.SPOT_LEVEL_HELP:
                    spotNo = PromoClient.SPOT_LEVEL_HELP;
                    break;
                case PromoSpot.SPOT_LEVEL_CREDITS:
                    spotNo = PromoClient.SPOT_LEVEL_CREDITS;
                    break;
                default:
                    spotNo = PromoClient.SPOT_MENU;
                    break;
            }
            Debug.Log("Promo Ad Shown at: " + promoSpot);
            PromoClient.SetPromoSpot(spotNo);
#endif
        }

        public static void HidePromoAd() {
#if ACTIVE_RI_SDK
            //if (isAdsRemoved) { return; }
            //if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) { return; }
            //Debug.Log("Promo Ad Shown at: " + Navigation.GetCurrentScene());
            Debug.Log("Close......................... ");
            PromoClient.SetPromoSpot(-1);
            RI_Plugin.Instance.ClosePromoAd();
#endif
        }
#endregion

        public void TestAd(int i) {
            AdSpot adSpot = AdSpot.end;
            switch(i) {
                case 0:
                    adSpot = AdSpot.start;
                    break;
                case 1:
                    adSpot = AdSpot.middle;
                    break;
                case 2:
                    adSpot = AdSpot.end;
                    break;
                case 3:
                    adSpot = AdSpot.video;
                    break;
            }
            ShowAd(adSpot);
        }
    }
}