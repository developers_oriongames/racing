﻿using UnityEngine;
using DG.Tweening;
using GameServices.UserData;

namespace GameServices {
    public enum GamePlaySoundType {
        BoostCollect = 0,
        Boost = 1,
        BrakeSmall = 2,
        BrakeBig = 3,
        Start = 4,
        Eliminate = 5,
        EliminateBeep = 6,
        Finish = 7,
        SmallCrash = 8,
        BigCrash = 9,
        WrongDirection = 10,
        Jump = 11,
    }

    public class AudioManager : MonoBehaviour {

        public static AudioManager Instance;

        [Header("Flags")]
        public static bool isSoundOn;
        public static bool isMusicOn;

        [Header("Volume Level")]
        public static float soundVolume = 1;
        public static float musicVolume = 1;

        [Header("Sprites")]
        public Sprite soundOnSprite;
        public Sprite soundOffSprite;
        public Sprite musicOnSprite;
        public Sprite musicOffSprite;

        [Header("Colors")]
        public Color soundActiveColor;
        public Color soundDeactiveColor;
        public Color musicActiveColor;
        public Color musicDeactiveColor;

        [Header("Background Clips")]
        [SerializeField] private AudioClip bg_game_play;
        [SerializeField] private AudioClip bg_game_play_25;
        [SerializeField] private AudioClip bg_level_complete;
        [SerializeField] private AudioClip bg_menu;

        [Header("UI Clips")]
        [SerializeField] private AudioClip clip_buy;
        [SerializeField] private AudioClip clip_click;

        [Header("Game Clips")]
        public AudioClip[] clip_car;
        [Space]
        public AudioClip[] clip_car_boost;
        [Space]
        public AudioClip clip_boost_collect;
        public AudioClip clip_boost_shoot;
        public AudioClip clip_brake_1;
        public AudioClip clip_brake_2;
        public AudioClip clip_car_start;
        public AudioClip clip_crash_big;
        public AudioClip clip_crash_small;
        public AudioClip clip_eliminate;
        public AudioClip clip_eliminate_beep;
        public AudioClip clip_finish;
        public AudioClip clip_jump;
        public AudioClip clip_play;
        public AudioClip clip_wrong_direction;

        [Header("Audio Source")]
        [SerializeField] private AudioSource musicAudioSource;
        [SerializeField] private AudioSource soundAudioSource;
        [SerializeField] private AudioSource carAudioSource;
        [SerializeField] private AudioSource clip1AudioSource;
        [SerializeField] private AudioSource clip2AudioSource;
        [SerializeField] private AudioSource clip3AudioSource;

        // Keys
        const string KEY_SOUND = "key_sound";
        const string KEY_MUSIC = "key_music";

        const string KEY_SOUND_VOLUME = "key_sound_volume";
        const string KEY_MUSIC_VOLUME = "key_music_volume";

        const int SOUND_ON_STATE = 0;
        const int SOUND_OFF_STATE = 1;

        const int MUSIC_ON_STATE = 0;
        const int MUSIC_OFF_STATE = 1;

        void Awake() {
            if (Instance == null) {
                Instance = this;
            }
            isSoundOn = (PlayerPrefs.GetInt(KEY_SOUND) == SOUND_ON_STATE);
            isMusicOn = (PlayerPrefs.GetInt(KEY_MUSIC) == MUSIC_ON_STATE);
            //GetVolume();
            soundVolume = UserDataManager.GetSoundVolume();
            musicVolume = UserDataManager.GetMusicVolume();
        }

        public static void UpdateMusicVolume(float volume) {
          UserDataManager.SaveMusicVolume(volume);
            musicVolume = UserDataManager.GetMusicVolume();
            if (Instance.musicAudioSource != null) { Instance.musicAudioSource.volume = musicVolume; }
        }

        public static void UpdateSoundVolume(float volume) {
            UserDataManager.SaveSoundVolume(volume);
            soundVolume = UserDataManager.GetSoundVolume();
            if (Instance.soundAudioSource != null) { Instance.soundAudioSource.volume = volume; }
            if (Instance.carAudioSource != null) { Instance.carAudioSource.volume = volume; }
            if (Instance.clip1AudioSource != null) { Instance.clip1AudioSource.volume = volume; }
            if (Instance.clip2AudioSource != null) { Instance.clip2AudioSource.volume = volume; }
            if (Instance.clip3AudioSource != null) { Instance.clip3AudioSource.volume = volume; }
        }

        /// <summary>
        /// Toggle the sfx sounds
        /// </summary>
        public static void ToggleSound() {
            if (isSoundOn) {
                isSoundOn = false;
                PlayerPrefs.SetInt(KEY_SOUND, SOUND_OFF_STATE);
            } else {
                isSoundOn = true;
                PlayerPrefs.SetInt(KEY_SOUND, SOUND_ON_STATE);
            }
            GameEvents.OnFXToggled();
        }

        /// <summary>
        /// Toggle the background music
        /// </summary>
        public static void ToggleMusic() {
            if (isMusicOn) {
                isMusicOn = false;
                PlayerPrefs.SetInt(KEY_MUSIC, MUSIC_OFF_STATE);
                //CheckBackgroundMusic();
            } else {
                isMusicOn = true;
                PlayerPrefs.SetInt(KEY_MUSIC, MUSIC_ON_STATE);
                //CheckBackgroundMusic();
            }
            GameEvents.OnMusicToggled();
        }

        /// <summary>
        /// Play supplied sound clip
        /// </summary>
        /// <param name="clip"></param>
        public static void PlaySound(AudioClip clip) {
            if (isSoundOn && clip != null && Instance.soundAudioSource != null) {
                Instance.soundAudioSource.volume = soundVolume;
                Instance.soundAudioSource.PlayOneShot(clip);
            }
        }

        /// <summary>
        /// Play supplied audio source
        /// </summary>
        /// <param name="audioSource"></param>
        public static void PlaySound(AudioSource audioSource) {
            if (isSoundOn && audioSource != null) {
                audioSource.volume = soundVolume;
                audioSource.Play();
            }
        }

        public static void StopSound(AudioSource audioSource) {
            if (isSoundOn && audioSource != null) {
                audioSource.Stop();
            }
        }

        /// <summary>
        /// Play the button sound.
        /// </summary>
        public static void PlayButtonSound() {
            PlaySound(Instance.clip_click);
        }

        /// <summary>
        /// Play the buy sound.
        /// </summary>
        public static void PlayBuySound() {
            PlaySound(Instance.clip_buy);
        }

        #region Car Sound
        private static float soundDump = 3.75f; 
        /// <summary>
        /// Play Car Sound
        /// </summary>
        public static void PlayCarSound() {
            if (isSoundOn && Instance.carAudioSource != null) {
                Instance.carAudioSource.clip = CarUserControl.isBoost 
                    ? Instance.clip_car_boost[CarManager.GetSelectedCar().GetCarID()] 
                    : Instance.clip_car[CarManager.GetSelectedCar().GetCarID()];
            }
            if (!Instance.carAudioSource.isPlaying) {
                //print("Playing Car Engine Audio: CLIP :::: " + Instance.carAudioSource.clip);
                Instance.carAudioSource.Play();
                Instance.carAudioSource.loop = true;
                if (GameManager.isInTutorial) { soundDump *= 2.5f; }
                Instance.carAudioSource.volume = Mathf.Pow(soundVolume, soundDump);  //dumping the volume: increase the power (soundDump) to dicrease the soundVol //2.5f
            }
        }

        public static void StopCarSound() {
            if (Instance.carAudioSource != null) {
                Instance.carAudioSource.Pause();
                Instance.carAudioSource.loop = false;
                Instance.carAudioSource.volume = 0;
                Instance.carAudioSource.pitch = 0;
            }
        }

        public static void ChangeCarSoundPitch(float pitch) {
            if (Instance.carAudioSource != null) {
                Instance.carAudioSource.DOPitch(pitch, 1f);
            }
        }
        #endregion

        #region Game Play Sound
        public static void PlayGamePlaySound(GamePlaySoundType gamePlaySoundType) {
            if (GameManager.isInPause) { return; }
            Instance.clip1AudioSource.volume = soundVolume;
            Instance.clip2AudioSource.volume = soundVolume;
            Instance.clip3AudioSource.volume = soundVolume;
            switch (gamePlaySoundType) {
                case GamePlaySoundType.BoostCollect:
                    Instance.clip1AudioSource.PlayOneShot(Instance.clip_boost_collect);
                    break;
                case GamePlaySoundType.Boost:
                    Instance.clip1AudioSource.PlayOneShot(Instance.clip_boost_shoot);
                    break;
                case GamePlaySoundType.BrakeSmall:
                    Instance.clip2AudioSource.PlayOneShot(Instance.clip_brake_1);
                    if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
                        Instance.clip2AudioSource.volume = soundVolume / 2;
                    }
                    break;
                case GamePlaySoundType.BrakeBig:
                    if (!Instance.clip2AudioSource.isPlaying) {
                        Instance.clip2AudioSource.clip = Instance.clip_brake_2;
                        Instance.clip2AudioSource.Play();
                    }
                    break;
                case GamePlaySoundType.Start:
                    Instance.clip1AudioSource.clip = Instance.clip_car_start;
                    Instance.clip1AudioSource.PlayDelayed(6.5f); //4
                    break;
                case GamePlaySoundType.Eliminate:
                    Instance.clip3AudioSource.PlayOneShot(Instance.clip_eliminate);
                    break;
                case GamePlaySoundType.EliminateBeep:
                    if (!Instance.clip3AudioSource.isPlaying) {
                        Instance.clip3AudioSource.clip = Instance.clip_eliminate_beep;
                        Instance.clip3AudioSource.PlayDelayed(1f);
                    }
                    break;
                case GamePlaySoundType.Finish:
                    Instance.clip1AudioSource.PlayOneShot(Instance.clip_finish);
                    break;
                case GamePlaySoundType.SmallCrash:
                    Instance.clip1AudioSource.PlayOneShot(Instance.clip_crash_small);
                    break;
                case GamePlaySoundType.BigCrash:
                    Instance.clip1AudioSource.PlayOneShot(Instance.clip_crash_big);
                    break;
                case GamePlaySoundType.WrongDirection:
                    if (!Instance.clip1AudioSource.isPlaying) {
                        Instance.clip1AudioSource.PlayOneShot(Instance.clip_wrong_direction);
                    }
                    break;
                case GamePlaySoundType.Jump:
                    Instance.clip1AudioSource.PlayOneShot(Instance.clip_jump);
                    break;
            }
        }

        public static void StopGamePlaySound() {
            Instance.clip1AudioSource.Stop();
            Instance.clip2AudioSource.Stop();
            Instance.clip3AudioSource.Stop();
        }
        #endregion

        #region Music
        public static void PlayMenuMusic() {
            PlayMusic(Instance.bg_menu);
        }

        public static void PlayGamePlayMusic() {
            if (!Instance.musicAudioSource.isPlaying) {
                PlayMusic(Instance.bg_game_play);
            }
        }
        public static void PlayLevelCompleteMusic() {
            if (!Instance.musicAudioSource.isPlaying) {
                PlayMusic(Instance.bg_level_complete);
            }
        }
        static void PlayMusic(AudioClip clip) {
            StopMusic();
            if (isMusicOn && clip != null && Instance.musicAudioSource != null) {
                Instance.musicAudioSource.volume = musicVolume;
                Instance.musicAudioSource.loop = true;
                Instance.musicAudioSource.clip = clip;
                if (!Instance.musicAudioSource.isPlaying) {
                    //Debug.Log("bg music: " + clip);
                    if (UIManager.CurrentScreen == GameScreen.GameCompleteScreen) {
                        Instance.musicAudioSource.PlayDelayed(1);
                    } else {
                        Instance.musicAudioSource.Play();
                    }
                }
            }
        }

        public static void StopMusic() {
            if (Instance.musicAudioSource != null) {
                Instance.musicAudioSource.Stop();
                Instance.musicAudioSource.loop = false;
            }
        }
        #endregion
    }
}