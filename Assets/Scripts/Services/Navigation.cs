﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using System.Collections.Generic;
using GameServices;
using MEC;

public enum Scenes {
        Loading = 0,
        Menu = 1,
        Track0 = 2,
        Track1 = 3,
        Track2 = 4,
        Track3 = 5,
        Track4 = 6,
        Track5 = 7,
        Track6 = 8,
        Track7 = 9,
        Track8 = 10,
        Tutorial = 11,
    }

public class Navigation : MonoBehaviour {

    static Navigation Instance;

    static Queue<Action> fadeInOutTasks = new Queue<Action>();
    static bool IsFadeInOutBusy;

    public static bool isInLoading;
    public static bool isToShowLoadingPanel;
    const float loadTime = 0.2F;
    bool isClicked = false;

    [SerializeField] private UIGameLoading gameLoading;
    [SerializeField] private Image loadingPanel;
    [SerializeField] private GameObject gameLoadingPanel;

    void Awake() {
        if (Instance == null) {
            Instance = this;
        }
        ActiveGameLoadingPanel();
        loadingPanel.gameObject.SetActive(false);
    }

    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
        GameEvents.OnInputBack += BackButtonPressed;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        GameEvents.OnInputBack -= BackButtonPressed;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (GlobalData.isInPause) {
            Time.timeScale = 1;
           GameManager. isInPause = false;
        }
        gameLoading.Loading();
        isClicked = false;
        if (!IsInGameLoadScene()) {
            DeactiveGameLoadingPanel();
        } else {
            Debug.Log("IsInGameLoadScene() : >>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<");
            //LoadScene(Scenes.Menu, GameScreen.ShopScreen);
        }
        FadeOut();
    }

    public static void ActiveGameLoadingPanel() {
        Instance.gameLoadingPanel.SetActive(true);
    }

    static void DeactiveGameLoadingPanel() {
        Instance.gameLoadingPanel.SetActive(false);
    }

    void BackButtonPressed() {
        if (!isInLoading) {
            UIManager.BackNavigation();
        }
    }

    #region Pause
    void OnApplicationPause(bool pauseStatus) {
        //print("OnApplicationPause: " + pauseStatus);
        PauseWork(pauseStatus);
    }
    void OnApplicationFocus(bool hasFocus) {
        //print("OnApplicationFocus: " + hasFocus);
        PauseWork(!hasFocus);
    }
    void PauseWork(bool isPaused) {
        if (isPaused && UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
#if !UNITY_EDITOR
                GameManager.Pause();
#endif
        }
    }
    #endregion

    /// <summary>
    /// To get the current/active scene from SCENE enum
    /// </summary>
    /// <returns></returns>
    public static Scenes GetCurrentScene() {
        return (Scenes)SceneManager.GetActiveScene().buildIndex;
    }

    /// <summary>
    /// Returns true if current scene is game load scene
    /// </summary>
    /// <returns></returns>
    public static bool IsInGameLoadScene() {
        if (GetCurrentScene() == Scenes.Loading) {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Returns true if current scene is Menu scene
    /// </summary>
    /// <returns></returns>
    public static bool IsInMenuScene() {
        if (GetCurrentScene() == Scenes.Menu) {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Returns true if current scene is Tutorial scene
    /// </summary>
    /// <returns></returns>
    public static bool IsInTutorialScene() {
        if (GetCurrentScene() == Scenes.Tutorial) {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Returns true if current scene is Game scene
    /// </summary>
    /// <returns></returns>
    public static bool IsInGameScene() {
        if (GetCurrentScene() == Scenes.Track0
            || GetCurrentScene() == Scenes.Track1
            || GetCurrentScene() == Scenes.Track2
            || GetCurrentScene() == Scenes.Track3
            || GetCurrentScene() == Scenes.Track4
            || GetCurrentScene() == Scenes.Track5
            || GetCurrentScene() == Scenes.Track6
            || GetCurrentScene() == Scenes.Track7
            || GetCurrentScene() == Scenes.Track8) {
            return true;
        }
        return false;
    }

    void FadeIn(int sceneIndex, float delay = 0.0F) {
        GameEvents.OnSceneLoadStarted();
        isInLoading = true;
        loadingPanel.gameObject.SetActive(true);
        loadingPanel.DOFade(1, loadTime).SetEase(Ease.OutExpo).SetDelay(delay).SetUpdate(true).OnComplete(() => {
            //SceneManager.LoadScene(sceneIndex);
            //Instance.StartCoroutine(ShowLoadScreen(sceneIndex));
            Timing.RunCoroutine(_ShowLoadScreen(sceneIndex));
        });
    }

    void FadeOut() {
        //Debug.Log("fade out call");
        loadingPanel.DOFade(0, loadTime).SetEase(Ease.OutQuad).SetUpdate(true).OnComplete(() => {
            isInLoading = false;
            loadingPanel.gameObject.SetActive(false);
            //print("99999999999999999999999999999 : " + GetCurrentScene());
            ///GameEvents.OnSceneLoadFinished();
        });
    }

    /// <summary>
    /// Load scene with parameter from SCENE enum and an optional scene load delay. To get active scene call GetCurrentScene()
    /// </summary>
    /// <param name="scene"></param>
    public static void LoadScene(Scenes scene, GameScreen currentScreen, float loadDelay = 0.0f) {
        if (Instance.isClicked) return;
        Instance.isClicked = true;
        UIManager.CurrentScreen = currentScreen;
        if (currentScreen == GameScreen.GameStartScreen) { GameEvents.OnGameStartScreen(); }
        Instance.gameLoading.EnableLoading();
        ActiveGameLoadingPanel();
        Instance.FadeIn((int)scene, loadDelay);
    }

    public static void LoadScene(Scenes scene) {
        SceneManager.LoadScene((int)scene);
    }

    static IEnumerator<float> _ShowLoadScreen(int sceneIndex) {
        bool isLoad = false;
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Single);
        if (GameManager.isToLoadQuickly) {
            asyncOperation.allowSceneActivation = true;
        } else {
            asyncOperation.allowSceneActivation = false;
            yield return Timing.WaitForSeconds(1.0f);
            while (!isLoad) {
                yield return Timing.WaitForSeconds(0.1f); ;
                if (Instance.gameLoading.progress <= 80) {
                    Instance.gameLoading.progress += (int)UnityEngine.Random.Range(10f, 20f);
                } else if (Instance.gameLoading.progress > 80 && Instance.gameLoading.progress < 100) {
                    Instance.gameLoading.progress += (int)UnityEngine.Random.Range(1f, 5f);
                } else if (Instance.gameLoading.progress >= 100) {
                    asyncOperation.allowSceneActivation = true;
                    isLoad = true;
                }
            }
            GameEvents.OnSceneLoadFinished();
        }
        yield return Timing.WaitForOneFrame;
    }

    /// <summary>
    /// Fades the in out mimicing a fake scene load
    /// </summary>
    /// <param name="Callback">Callback Fucntion that will execute in between the fade in out.</param>
    //public static void FadeInOut(Action Callback = null) {
    //    if (IsFadeInOutBusy) {
    //        Action EnqueuedCallback = () => {
    //            FadeInOut(Callback);
    //        };
    //        fadeInOutTasks.Enqueue(EnqueuedCallback);
    //        return;
    //    }
    //    IsFadeInOutBusy = true;
    //    GameEvents.OnSceneLoadStarted();
    //    Instance.loadingPanel.gameObject.SetActive(true);
    //    Instance.loadingPanel.DOFade(1, loadTime).SetEase(Ease.OutExpo).SetUpdate(true).OnComplete(() => {
    //        if (Callback != null) {
    //            Callback();
    //        }
    //        Instance.loadingPanel.DOFade(0, loadTime).SetEase(Ease.OutQuad).SetUpdate(true).OnComplete(() => {
    //            Instance.loadingPanel.gameObject.SetActive(false);
    //            GameEvents.OnSceneLoadFinished();
    //            IsFadeInOutBusy = false;
    //            if (fadeInOutTasks.Count > 0) {
    //                Action DequeuedCallback = fadeInOutTasks.Dequeue();
    //                DequeuedCallback();
    //            }
    //        });
    //    });
    //}
}