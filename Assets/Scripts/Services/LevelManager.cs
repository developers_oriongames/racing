﻿using GameServices.UserData;
using System;
using GameServices;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    /*
	 //Previous Unlock Method
	//TotalStar, CarNum
	public static int[,] LevelUnlockConditionMobile = {
		{00, 00},	//1                
		{04, 00},	//2				
		{08, 00},	//3		
		{13, 00},	//4			
		{17, 00},	//5			
		{21, 00},	//6			
		{26, 00},	//7			
		{30, 00},	//8			
		{34, 00},	//car 04 //9
		{38, 00},	//10			
		{43, 00},	//car 05 //11			
		{48, 00},	//12
		{52, 00},	//13
		{56, 00},	//car 07 //14
		{61, 00},	//15
		{65, 00},	//16
		{70, 00},	//17
		{75, 00},	//car 09 //18
		{80, 00},	//19
		{85, 00},	//20
		{90, 00},	//car 10 //21
		{95, 00},	//22
		{100, 00},	//car 11 //23
		{105, 00},	//24
		{110, 00},	//car 12 //25
		{115, 00},	//26
		{120, 00},	//car 14 //27
	};
	//TotalStar, CarNum
	public static int[,] LevelUnlockConditionTV = {
		{00, 00},	//1               
		{04, 00},	//2			
		{08, 00},	//3	
		{99, 00},	//4		
		{99, 00},	//5		
		{99, 00},	//6		
		{99, 00},	//7		
		{99, 00},	//8
		{99, 00},	//9
		{99, 00},	//10			
		{99, 00},	//11
		{99, 00},	//12
		{99, 00},	//13
		{99, 00},	//14
		{99, 00},	//15
		{99, 00},	//16
		{99, 08},	//17
		{99, 00},	//18
		{99, 00},	//19
		{99, 00},	//20
		{99, 00},	//21
		{99, 00},	//22
		{99, 00},	//23
		{99, 00},	//24
		{99, 00},	//25
		{99, 00},	//26
		{99, 00},	//27
	};
	*/

    public static int[] LevStar =
    {
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
    };
	
	public static int currentSelectedLevelNo;
	public static int selectedLevel;

    private const int TotalLevels = 27;

    void OnEnable()
    {
        GameEvents.OnGameCompleteScreen += UpdateLevelUnlockInfo;
        GameEvents.OnEliminationScreen += UpdateLevelUnlockInfo;
    }
	
    void OnDisable()
    {
        GameEvents.OnGameCompleteScreen -= UpdateLevelUnlockInfo;
        GameEvents.OnEliminationScreen -= UpdateLevelUnlockInfo; 
	}

    void Start() {
		if (LevStar == null) {
			LevStar = new int[TotalLevels/*LevelUnlockConditionMobile.GetLength(0)*/];
		}
		currentSelectedLevelNo = 0;
		selectedLevel = 0;
		GetLevelData();
	}

    private void UpdateLevelUnlockInfo()
    {
        if (GameManager.isInTutorial) { return; }

        if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv && selectedLevel >= 2)
        {
            return;
        }
		
        UserDataManager.SetMaxUnlockedLevel(selectedLevel + 1);
    }

    public static void UnlockAllLevels()
    {
		UserDataManager.SetMaxUnlockedLevel(99);
    }

    public static int GetTotalStar() {
		int t_star = 0;
		try {
			for (int i = 0; i < LevStar.Length; i++) {
				t_star += LevStar[i];
			}
		} catch (NullReferenceException e) {
			Debug.LogError("Error! " + e.Message);
		}
		return t_star;
	}

    public static bool IsToShowTickMark(int levelNo)
    {
        GetLevelData();
        return LevStar[levelNo] >= 3;
    }

    public static bool IsLevelUnlocked(int levelNo)
	{
		return levelNo <= UserDataManager.GetMaxUnlockedLevel();
	}

	public static void GetLevelData() {
		for (int i = 0; i < LevStar.Length; i++) {
			LevStar[i] = UserDataManager.GetLevelStarData(i.ToString(), LevStar[i]);
		}
	}

	public static void SaveLevelData() {
		for (int i = 0; i < LevStar.Length; i++) {
			UserDataManager.SaveLevelStarData(i.ToString(), LevStar[i]);
		}
	}
}
