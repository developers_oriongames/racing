﻿using UnityEngine;

namespace GameServices {
    public enum GameModes {
        None = 0,
        Classic = 1, //0
        Elimination = 2, //2
        Versus = 3, //1
    }

    public class GameModeManager : MonoBehaviour {
        public static GameModeManager Instance;

        #region GameModes
        private static GameModes currentGameMode = GameModes.Classic;
        public static GameModes GetCurrentGameMode {
            get => currentGameMode;
        }
        public static void SetCurrentGameMode(GameModes gameMode) {
            currentGameMode = gameMode;
        }
        public static string GameModeName() {
            switch (currentGameMode) {
                case GameModes.Classic:
                    return Constants.CLASSIC;
                case GameModes.Versus:
                    return Constants.VERSUS;
                case GameModes.Elimination:
                    return Constants.ELIMINATION;
                default:
                    return Constants.NONE;
            }
        }
        #endregion

        void Awake() {
            if (Instance == null) {
                Instance = this;
            }
        }
    }
}