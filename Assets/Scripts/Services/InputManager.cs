using GameServices.UserData;
using UnityEngine;

namespace GameServices {

    public enum BuildPlatforms {
        None = 0,
        AmazonTab = 1,
        AmazonTv = 2,
        Android = 3,
        IOS = 4,
    }

    public enum ControllerTypes {
        NotSet = 0,
        Sensor = 1,
        Touch = 2,
        Remote = 3,
    }

    public class InputManager : MonoBehaviour {

        private static InputManager instance;

        //Testing
        private static bool isToTestAmazonTV = false;
        //Testing
        private static BuildPlatforms currentBuildPlatforms = BuildPlatforms.None;
        private static ControllerTypes currentControllerType = ControllerTypes.NotSet;

        void Awake() {
            if (instance == null) {
                instance = this;
            }
            SetCurrentBuildPlatform();
        }

        void Start() {
            Debug.Log("SetCurrentController ............... " + UserDataManager.GetController());
            SetCurrentController(UserDataManager.GetController());
        }

        #region Platform
        private static string deviceModel;
        public static BuildPlatforms GetCurrentPlatform() {
            return currentBuildPlatforms;
        }
        private static void SetCurrentBuildPlatform() {
            deviceModel = SystemInfo.deviceModel.ToUpper();
            //amazon tv contains 'AMAZON' and also 'AFT' or 'ATF'  
            if ((deviceModel.Contains("AFT") || deviceModel.Contains("ATF"))
                && deviceModel.Contains("AMAZON")) {
                //amazon tv
                currentBuildPlatforms = BuildPlatforms.AmazonTv;
            } else {
                //amazon fire tab
                currentBuildPlatforms = BuildPlatforms.AmazonTab;
            }
#if UNITY_EDITOR
            if (isToTestAmazonTV) {
                currentBuildPlatforms = BuildPlatforms.AmazonTv;
            } else {
                currentBuildPlatforms = BuildPlatforms.AmazonTab;
            }
#endif
        }

        public static bool IsInAmazonTv() {
            return GetCurrentPlatform() == BuildPlatforms.AmazonTv;
        }
        #endregion

        #region Controller
        public static ControllerTypes GetCurrentController() {
            return currentControllerType;
        }
        public static void SetCurrentController(ControllerTypes controllerTypes) {
            currentControllerType = controllerTypes;
            UserDataManager.SetController(controllerTypes);
        }
        #endregion

        void Update() {

            #region Sensor Controller
            OnAcceleration(Input.acceleration);
            OnHorizontalAxis(Input.GetAxis("Horizontal"));
            #endregion

            #region Touch Controller
            if (Input.GetKey(KeyCode.A)) { OnKeyA(); }
            if (Input.GetKey(KeyCode.D)) { OnKeyD(); }
            if (Input.GetKeyUp(KeyCode.A)) { OnKeyUpA(); }
            if (Input.GetKeyUp(KeyCode.D)) { OnKeyUpD(); }
            #endregion

            #region Mouse Press
            if (Input.GetMouseButtonDown(0)) { OnMousePressed(); }
            if (Input.GetMouseButtonUp(0)) { OnMouseReleased(); }
            #endregion

            #region Back Press
            if (Input.GetKeyDown(KeyCode.Escape)) { OnBackButton(); }
            #endregion

            #region Amazon TV Remote
            if (Input.GetKeyDown(KeyCode.Menu) || Input.GetKeyDown(KeyCode.R)) { OnInputMenu(); }
            if (Input.GetKeyDown(KeyCode.JoystickButton0)) { OnInputJoystick_0(); }

            if (Input.GetKey(KeyCode.LeftArrow)) { OnInputLeft(); }
            if (Input.GetKey(KeyCode.RightArrow)) { OnInputRight(); }
            if (Input.GetKeyUp(KeyCode.LeftArrow)) { OnInputEndLeft(); }
            if (Input.GetKeyUp(KeyCode.RightArrow)) { OnInputEndRight(); }

            if (Input.GetKey(KeyCode.UpArrow)) { OnInputUp(); }
            if (Input.GetKeyUp(KeyCode.UpArrow)) { OnInputEndUp(); }

            if (Input.GetKey(KeyCode.DownArrow)) { OnInputDown(); }
            if (Input.GetKeyUp(KeyCode.DownArrow)) { OnInputEndDown(); }
            #endregion
        }

        #region Event Calls
        void OnMousePressed() {
            GameEvents.OnMouseDown();
        }
        void OnMouseReleased() {
            GameEvents.OnMouseUp();
        }
        void OnBackButton() {
            GameEvents.OnInputBack();
        }

        void OnAcceleration(Vector3 acceleration) {
            GameEvents.OnInputAcceleration(acceleration);
        }
        void OnHorizontalAxis(float axisValue) {
            GameEvents.OnInputHorizontalAxis(axisValue);
        }

        void OnKeyA() {
            GameEvents.OnKeyA();
        }
        void OnKeyUpA() {
            GameEvents.OnKeyUpA();
        }
        void OnKeyD() {
            GameEvents.OnKeyD();
        }
        void OnKeyUpD() {
            GameEvents.OnKeyUpD();
        }

        //Amazon TV Remote
        void OnInputMenu() {
            GameEvents.OnInputMenu();
        }
        void OnInputJoystick_0() {
            GameEvents.OnInputJoystick_0();
        }
        void OnInputLeft() {
            GameEvents.OnInputLeft();
        }
        void OnInputRight() {
            GameEvents.OnInputRight();
        }
        void OnInputEndLeft() {
            GameEvents.OnInputEndLeft();
        }
        void OnInputEndRight() {
            GameEvents.OnInputEndRight();
        }
        void OnInputUp() {
            GameEvents.OnInputUp();
        }
        void OnInputEndUp() {
            GameEvents.OnInputEndUp();
        }
        void OnInputDown() {
            GameEvents.OnInputDown();
        }
        void OnInputEndDown() {
            GameEvents.OnInputEndDown();
        }
        #endregion

        #region Public Methods
        public static bool IsInRightHalfOfTheScreen() {
            return Input.mousePosition.x > Screen.width / 2
                   && Input.mousePosition.y < Screen.height * 0.7f
                   && Input.mousePosition.y > Screen.height * 0.2f;
        }

        public static bool IsInLeftHalfOfTheScreen() {
            return Input.mousePosition.x < Screen.width / 2
                   && Input.mousePosition.y < Screen.height * 0.7f
                   && Input.mousePosition.y > Screen.height * 0.2f;
        }
        /// <summary>
        /// Incase controller is not set anyhow not set, set to 1 (sensor)
        /// </summary>
        public static void CheckForDefaultController() {
            if (GetCurrentController() == ControllerTypes.NotSet
                    && GameManager.isInGamePlay) {
                Debug.LogError("No controller set, setting default controller!");
                //UserDataManager.SetController(ControllerTypes.Sensor);
                //SetCurrentController(UserDataManager.GetController());
            }
        }
        #endregion
    }
}