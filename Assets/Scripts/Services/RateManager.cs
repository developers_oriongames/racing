﻿#define ACTIVE_RI_SDK

using GameServices;
using GameServices.UserData;
using MEC;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RateManager : MonoBehaviour {

    private static RateManager instance;

    private static bool isRateAlreadyGiven;
    public static bool isToShowRatePopup;
    public static bool isEligibleToRate;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
#if ACTIVE_RI_SDK
        isRateAlreadyGiven = UserDataManager.GetRateStatus();
#else
        isRateAlreadyGiven = true;
#endif
    }

    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
        GameEvents.OnGameLevelLoad += OnLevelLoad;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        GameEvents.OnGameLevelLoad -= OnLevelLoad;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (Navigation.GetCurrentScene() == Scenes.Menu) {
            //print("before if isToShowRatePopup:  " + isToShowRatePopup);
            //print("before if GameManager.gamePlayCount:  " + GameManager.gamePlayCount);
            //print("LevelManager.selectedLevel: " + LevelManager.selectedLevel);
            //print("LevelManager.currentSelectedLevelNo: " + LevelManager.currentSelectedLevelNo);
            if (isRateAlreadyGiven) { return; }
            if (LevelManager.selectedLevel + 1 < 4) { return; }
            if (isToShowRatePopup ||
                    (GameManager.gamePlayCount != 0
                    && GameManager.gamePlayCount >= 4)) {
                GameManager.gamePlayCount = 0;
                isToShowRatePopup = false;
                //print("isToShowRatePopup:  " + isToShowRatePopup);
                //print("GameManager.gamePlayCount:  " + GameManager.gamePlayCount);
                Timing.RunCoroutine(_WaitAndOpen());
            }
        }
    }
    private void OnLevelLoad(int levelNo) {
        //print("Level NO >>>>>>>>>>>>>>>>>>>>>>>> " + levelNo);
        if (levelNo == 4) {
            isToShowRatePopup = true;
        }
    }

    IEnumerator<float> _WaitAndOpen() {
        yield return Timing.WaitForSeconds(0.5f);
        UIManager.Instance.uIMenu.OpenRate();
        yield return Timing.WaitForOneFrame;
    }

    public static void RateApp() {
        isRateAlreadyGiven = true;
        UserDataManager.SetRateStatus(isRateAlreadyGiven);
        if (isEligibleToRate) {
            UIManager.Instance.uIMenu.ratePanel.ActualRateWorks();
            Application.OpenURL(GetURL());
        } else {
            UIManager.Instance.uIMenu.ratePanel.FakeRateWorks();
        }
    }

    private static string GetURL() {
        switch (InputManager.GetCurrentPlatform()) {
            case BuildPlatforms.Android:
                return "market://details?id=" + Application.identifier;    //Google Play
            case BuildPlatforms.IOS:
                break;
            case BuildPlatforms.AmazonTab:
            case BuildPlatforms.AmazonTv:
                return "amzn://apps/android?p=" + Application.identifier;   //Amazon
        }
        return null;
    }
}