﻿using UnityEngine;

namespace GameServices {
	public class CurrencyManager : MonoBehaviour {
		private static CurrencyManager instance;

		private static int totalCash;

		public static int TotalCash { get => UserData.UserDataManager.GetTotalCash(); }


		void Awake() {
			if (instance == null) {
				instance = this;
			}
		}

		//public static void GetTotalCash() {
		//	totalCash = UserDataManager.GetTotalCash();
		//}
		public static void GiveMoney(int amount) {
			CalculateMoney(amount);
		}

		public static void TakeMoney(int amount) {
			CalculateMoney(-amount);
		}

		static void CalculateMoney(int amount) {
			totalCash = UserData.UserDataManager.GetTotalCash();
			totalCash += amount;
			UserData.UserDataManager.SaveTotalCash(totalCash);
		}
	}
}