﻿#define ACTIVE_RI_SDK

using UnityEngine;
using UnityEngine.Analytics;
#if ACTIVE_RI_SDK
using RI_Extension;
#endif
using System.Collections.Generic;

namespace GameServices {

    public enum AnalyticsEvents {
        None = 0,
        Screen_Opened,
        Play_Tutorial,
        Loaded_Level_No,
        Level_Start,
        Level_Paused,
        Level_Clear,
        Purchased_Started,
        Purchased_Successful,
        Purchase_Failed,
    }

    public class AnalyticsManager : MonoBehaviour {

        static AnalyticsManager instance;

        #region Analytics Strings
        const string SCREEN = "Screen";
        const string ORIGIN = "Origin";
        const string LEVEL = "Level";
        const string RANK = "Rank";
        const string SKU = "SKU";
        const string FAILURE_REASON = "Failure_Reason";

        const string CAR_SELECTION = "Car_Selection";
        const string LEVEL_SELECTION = "Level_Selection";
        const string GAME_MODE_SELECTION = "Game_Mode_Selection";
        const string CAR_PREVIEW = "Car_Preview";
        const string STORE = "Store";
        const string SETTINGS = "Settings";
        const string EXIT = "Exit";
        const string HELP = "Help";
        #endregion

        void Awake() {
            if (instance == null) {
                instance = this;
            }
        }

        void OnEnable() {
            GameEvents.OnUICarSelection += OnScreenChangedCarSelection;
            GameEvents.OnUILevelSelection += OnScreenChangedLevelSelection;
            GameEvents.OnUIGameMode += OnScreenChangedGameMode;
            GameEvents.OnUICarPreview += OnScreenChangedCarPreview;
            GameEvents.OnUIStore += OnScreenChangedStore;
            GameEvents.OnUISettings += OnScreenChangedSettings;
            GameEvents.OnUIExit += OnScreenChangedExit;
            GameEvents.OnUIHelp += OnScreenChangedHelp;

            GameEvents.OnPlayTutorial += OnPlayTutorial;
            GameEvents.OnGameLevelLoad += OnGameLevelLoad;
            GameEvents.OnGamePlayScreen += OnGamePlayStarted;
            GameEvents.OnPauseScreen += OnGamePaused;
            GameEvents.OnGameCompleteScreen += OnGameCompleted;

            GameEvents.OnPurchaseStarted += OnPurchaseStarted;
            GameEvents.OnPurchaseComplete += OnPurchaseSuccessed;
            GameEvents.OnPurchaseFailed += OnPurchaseFailed;
        }

        void OnDisable() {
            GameEvents.OnUICarSelection -= OnScreenChangedCarSelection;
            GameEvents.OnUILevelSelection -= OnScreenChangedLevelSelection;
            GameEvents.OnUIGameMode -= OnScreenChangedGameMode;
            GameEvents.OnUICarPreview -= OnScreenChangedCarPreview;
            GameEvents.OnUIStore -= OnScreenChangedStore;
            GameEvents.OnUISettings -= OnScreenChangedSettings;
            GameEvents.OnUIExit -= OnScreenChangedExit;
            GameEvents.OnUIHelp -= OnScreenChangedHelp;

            GameEvents.OnPlayTutorial += OnPlayTutorial;
            GameEvents.OnGameLevelLoad -= OnGameLevelLoad;
            GameEvents.OnGamePlayScreen -= OnGamePlayStarted;
            GameEvents.OnPauseScreen -= OnGamePaused;
            GameEvents.OnGameCompleteScreen -= OnGameCompleted;

            GameEvents.OnPurchaseStarted -= OnPurchaseStarted;
            GameEvents.OnPurchaseComplete -= OnPurchaseSuccessed;
            GameEvents.OnPurchaseFailed -= OnPurchaseFailed;
        }

        #region Menu
        private void OnScreenChangedCarSelection(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, CAR_SELECTION); }
        }
        private void OnScreenChangedLevelSelection(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, LEVEL_SELECTION); }
        }
        private void OnScreenChangedGameMode(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, GAME_MODE_SELECTION); }
        }
        private void OnScreenChangedCarPreview(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, CAR_PREVIEW); }
        }
        private void OnScreenChangedStore(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, STORE); }
        }
        private void OnScreenChangedSettings(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, SETTINGS); }
        }
        private void OnScreenChangedExit(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, EXIT); }
        }
        private void OnScreenChangedHelp(bool obj) {
            if (obj) { LogEventWithSingleKey(AnalyticsEvents.Screen_Opened.ToString(), SCREEN, HELP); }
        }
        #endregion

        #region Game Info
        private void OnPlayTutorial(string position) {
            LogEventWithSingleKey(AnalyticsEvents.Play_Tutorial.ToString(), ORIGIN, position);
        }
        private void OnGameLevelLoad(int levelNo) {
            //LogEvent(AnalyticsEvents.Loaded_Level_No.ToString(), 
            // LEVEL + levelNo.ToString());
        }

        private void OnGamePlayStarted() {
            LogEventWithSingleKey(AnalyticsEvents.Level_Start.ToString(),
                LEVEL, (LevelManager.selectedLevel + 1).ToString());
        }
        private void OnGamePaused() {
            LogEventWithSingleKey(AnalyticsEvents.Level_Paused.ToString(),
                LEVEL, (LevelManager.selectedLevel + 1).ToString());
        }
        private void OnGameCompleted() {
            LogEventWithDoubleKey(AnalyticsEvents.Level_Clear.ToString(),
                LEVEL, (LevelManager.selectedLevel + 1).ToString(),
                RANK, GameController.Instance.carUserControl.mPlrRank.ToString());
        }
        #endregion

        #region In-App-Purchase
        private void OnPurchaseStarted(string skuId) {
            LogEventWithSingleKey(AnalyticsEvents.Purchased_Started.ToString(), SKU, skuId);
        }
        private void OnPurchaseSuccessed(string skuId) {
            LogEventWithSingleKey(AnalyticsEvents.Purchased_Successful.ToString(), SKU, skuId);
        }
        private void OnPurchaseFailed(string skuId, string failureReason) {
            LogEventWithDoubleKey(AnalyticsEvents.Purchase_Failed.ToString(),
                SKU, skuId, FAILURE_REASON, failureReason);
        }
        #endregion

        #region Alanaytics Calls
        private static void LogEventWithSingleKey(string eventName, string eventKey, string eventValue) {
            Dictionary<string, string> parameters = new Dictionary<string, string> { { eventKey, eventValue } };
            LogAnalyticsEvent(eventName, parameters);
            Debug.Log("Analytics Event : " + eventName + eventValue);
        }

        private static void LogEventWithDoubleKey(string eventName, string eventKey1, string eventValue1, string eventKey2, string eventValue2) {
            Dictionary<string, string> parameters = new Dictionary<string, string> {
                { eventKey1, eventValue1 },
                { eventKey2, eventValue2 },
            };
            LogAnalyticsEvent(eventName, parameters);
            Debug.Log("Analytics Event : " + eventName + eventValue1 + eventValue2);
        }

        private static void LogAnalyticsEvent(string eventName, Dictionary<string, string> parameters) {
#if ACTIVE_RI_SDK
            AnalyticsClient.LogEvent(eventName, parameters);
#endif
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (KeyValuePair<string, string> pair in parameters) {
                dict.Add(pair.Key, pair.Value);
            }
            Analytics.CustomEvent(eventName, dict);
        }
        #endregion
    }
}