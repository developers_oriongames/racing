﻿using UnityEngine;

namespace GameServices.UserData {
    public class UserDataManager : MonoBehaviour {

        #region Tutorial
        public static bool GetTutorialShownStatus() {
            return PlayerPrefsX.GetBool(GameKeys.TUTORIAL_STATUS_KEY, false);
        }
        public static void SetTutorialShownStatus(bool isShown) {
            PlayerPrefsX.SetBool(GameKeys.TUTORIAL_STATUS_KEY, isShown);
        }
        #endregion

        #region Cash
        public static int GetTotalCash() {
            return PlayerPrefs.GetInt(GameKeys.TOTAL_CASH_AMOUNT_KEY, 0);
        }
        public static void SaveTotalCash(int amount) {
            PlayerPrefs.SetInt(GameKeys.TOTAL_CASH_AMOUNT_KEY, amount);
        }
        #endregion

        //#region Car Price
        //public static int GetCarData(string i, int defaultPrice) {
        //    return PlayerPrefs.GetInt(GameKeys.KEY_CAR_PRICE_ + i, defaultPrice);
        //}
        //public static void SaveCarData(string i, int amount) {
        //    PlayerPrefs.SetInt(GameKeys.KEY_CAR_PRICE_ + i, amount);
        //}
        //#endregion

        #region Car Unlock Status
        public static bool GetCarUnlockStatusData(string i) {
            return PlayerPrefsX.GetBool(GameKeys.KEY_CAR_UNLOCK_STATUS_ + i, false);
        }
        public static void SaveCarUnlockStatusData(string i, bool status) {
            PlayerPrefsX.SetBool(GameKeys.KEY_CAR_UNLOCK_STATUS_ + i, status);
        }
        #endregion

        #region Car Selection
        public static int GetLastSelectedCar() {
            return PlayerPrefs.GetInt(GameKeys.LAST_CAR_NUMBER_KEY, 0);
        }
        public static void SaveLastSelectedCar(int lastCarNo) {
            PlayerPrefs.SetInt(GameKeys.LAST_CAR_NUMBER_KEY, lastCarNo);
        }
        #endregion

        #region Level

        public static int GetMaxUnlockedLevel()
        {
            return PlayerPrefs.GetInt(GameKeys.KEY_MAX_UNLOCKED_LEVEL, 0);
        }

        public static void SetMaxUnlockedLevel(int levelNo)
        {
            if (levelNo >= GetMaxUnlockedLevel())
            {
                PlayerPrefs.SetInt(GameKeys.KEY_MAX_UNLOCKED_LEVEL, levelNo);
            }
        }


        public static int GetLevelStarData(string i, int defaultStar) {
            return PlayerPrefs.GetInt(GameKeys.KEY_LEVEL_STAR_ + i, defaultStar);
        }
        public static void SaveLevelStarData(string i, int starAmount) {
            PlayerPrefs.SetInt(GameKeys.KEY_LEVEL_STAR_ + i, starAmount);
        }
        #endregion

        #region IAP
        public static bool GetRemoveAdsPurchaseStatus() {
            return PlayerPrefsX.GetBool(GameKeys.REMOVED_ADS_KEY, false);
        }
        public static bool GetUnlockFullGamePurchaseStatus() {
            return PlayerPrefsX.GetBool(GameKeys.UNLOCK_FULL_GAME_KEY, false);
        }
        public static bool GetUnlockAllCarsPurchaseStatus() {
            return PlayerPrefsX.GetBool(GameKeys.UNLOCK_ALL_CARS_KEY, false);
        }
        public static bool GetUnlockAllLevelsPurchaseStatus() {
            return PlayerPrefsX.GetBool(GameKeys.UNLOCK_ALL_LEVELS_KEY, false);
        }

        public static void SetRemoveAdsPurchaseStatus(bool status) {
            PlayerPrefsX.SetBool(GameKeys.REMOVED_ADS_KEY, status);
        }
        public static void SetUnlockFullGamePurchaseStatus(bool status) {
            PlayerPrefsX.SetBool(GameKeys.UNLOCK_FULL_GAME_KEY, status);
        }
        public static void SetUnlockAllCarsPurchaseStatus(bool status) {
            PlayerPrefsX.SetBool(GameKeys.UNLOCK_ALL_CARS_KEY, status);
        }
        public static void SetUnlockAllLevelsPurchaseStatus(bool status) {
            PlayerPrefsX.SetBool(GameKeys.UNLOCK_ALL_LEVELS_KEY, status);
        }
        #endregion

        #region Music Selection
        public static float GetMusicVolume() {
            return PlayerPrefs.GetFloat(GameKeys.LAST_MUSIC_VOLUME_KEY, 0.5f);
        }
        public static void SaveMusicVolume(float lastMusicVolume) {
            PlayerPrefs.SetFloat(GameKeys.LAST_MUSIC_VOLUME_KEY, lastMusicVolume);
        }
        #endregion

        #region Sound Selection
        public static float GetSoundVolume() {
            return PlayerPrefs.GetFloat(GameKeys.LAST_SOUND_VOLUME_KEY, 0.75f);
        }
        public static void SaveSoundVolume(float lastSoundVolume) {
            PlayerPrefs.SetFloat(GameKeys.LAST_SOUND_VOLUME_KEY, lastSoundVolume);
        }
        #endregion

        #region In-Game Camera Zoom Level
        public static int GetCameraZoomLevel() {
            return PlayerPrefs.GetInt(GameKeys.LAST_IN_GAME_CAMERA_ZOOM_KEY, 0);
        }
        public static void SaveCameraZoomLevel(int lastZoomLevel) {
            PlayerPrefs.SetInt(GameKeys.LAST_IN_GAME_CAMERA_ZOOM_KEY, lastZoomLevel);
        }
        #endregion

        #region Rate
        public static bool GetRateStatus() {
            return PlayerPrefsX.GetBool(GameKeys.RATE_GIVEN_KEY, false);
        }

        public static void SetRateStatus(bool isRated) {
            PlayerPrefsX.SetBool(GameKeys.RATE_GIVEN_KEY, isRated);
        }
        #endregion

        #region Controller
        public static ControllerTypes GetController() {
            int controller = PlayerPrefs.GetInt(GameKeys.SELECTED_CONTROLLER_KEY, (int)ControllerTypes.NotSet); //0 for not set, 1 for sensor, 2 for touch
            switch (controller) {
                case (int)ControllerTypes.Sensor:
                    return ControllerTypes.Sensor;
                case (int)ControllerTypes.Touch:
                    return ControllerTypes.Touch;
                case (int)ControllerTypes.Remote:
                    return ControllerTypes.Remote;
                default:
                    return ControllerTypes.NotSet;
            }
        }
        public static void SetController(ControllerTypes selectedController) {
            PlayerPrefs.SetInt(GameKeys.SELECTED_CONTROLLER_KEY, (int)selectedController);
        }
        #endregion

        #region Mega Offer
        public static bool GetMegaOfferShownStatus()
        {
            return PlayerPrefsX.GetBool(GameKeys.MEGA_OFFER_STATUS_KEY, false);
        }
        public static void SetMegaOfferShownStatus(bool isShown)
        {
            PlayerPrefsX.SetBool(GameKeys.MEGA_OFFER_STATUS_KEY, isShown);
        }
        #endregion
    }
}