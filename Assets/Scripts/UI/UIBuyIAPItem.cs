﻿using GameServices;
using UnityEngine;
//using UnityEngine.EventSystems;

/// <summary>
/// Where the IAP took places (Store or Other)
/// </summary>
public enum IAPButtonType {
    None,
    Store,
    Others,
}

public class UIBuyIAPItem : MonoBehaviour/*, IPointerClickHandler*/ {

    [SerializeField] private IAPButtonType buttonType;
    
    [SerializeField] private NonConsumableIAPItems item;

    void Start() {
        GameEvents.OnCarSelectionChange += OnCarSelectionChange;
        OnCarSelectionChange(CarManager.Instance.currentSelectedCar);
    }

    void OnDestroy() {
        GameEvents.OnCarSelectionChange -= OnCarSelectionChange;
    }

    void OnCarSelectionChange(Car car) {
        switch (buttonType) {
            case IAPButtonType.None:
                break;
            case IAPButtonType.Store:
                break;
            case IAPButtonType.Others:
                item = car.GetAmazonIAPItem();
                break;
            default:
                break;
        }
    }

    public void OnPointerClick(/*PointerEventData eventData*/) {
        //print("eroihgoierjgoijeroijgoirtjg " + eventData);
        AudioManager.PlayButtonSound();
        switch (item) {
            case NonConsumableIAPItems.NONE:
                break;
            case NonConsumableIAPItems.UNLOCK_ALL:
            case NonConsumableIAPItems.UNLOCK_CARS:
            case NonConsumableIAPItems.UNLOCK_LEVELS:
            case NonConsumableIAPItems.REMOVE_ADS:
                PurchaseManager.BuyNonConsumableItem(item);
                break;
            case NonConsumableIAPItems.BUY_M_KOOPER:
            case NonConsumableIAPItems.BUY_P_FIREBIRD:
            case NonConsumableIAPItems.BUY_H_RS:
            case NonConsumableIAPItems.BUY_F_FIESTA:
            case NonConsumableIAPItems.BUY_B_MW:
            case NonConsumableIAPItems.BUY_C_IMPALA:
            case NonConsumableIAPItems.BUY_M_BENZ_512:
            case NonConsumableIAPItems.BUY_F_FOUS_RS:
            case NonConsumableIAPItems.BUY_D_CHALLENGER:
            case NonConsumableIAPItems.BUY_F_CALIFORNIA:
            case NonConsumableIAPItems.BUY_A_R8:
            case NonConsumableIAPItems.BUY_B_CONTENTAL:
            case NonConsumableIAPItems.BUY_L_LP_670:
            case NonConsumableIAPItems.BUY_M_MP3_12C:
                //item = CarManager.GetCurrentSelectedCar().GetAmazonIAPItem();
                CarManager.BuyCar(item);
                break;
            case NonConsumableIAPItems.FREE:
                break;
        }
    }
}
