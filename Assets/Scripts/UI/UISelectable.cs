﻿using UnityEngine;
//using UnityEngine.EventSystems;
using UnityEngine.UI;

public struct Selector {
    public float AnchoredPositionX;
    public float AnchoredPositionY;
    public float SizeDeltaX;
    public float SizeDeltaY;

    public Selector(float anchoredPositionX, float anchoredPositionY, float sizeDeltaX, float sizeDeltaY) {
        AnchoredPositionX = anchoredPositionX;
        AnchoredPositionY = anchoredPositionY;
        SizeDeltaX = sizeDeltaX;
        SizeDeltaY = sizeDeltaY;
    }
}

[RequireComponent(typeof(Selectable))]
public class UISelectable : MonoBehaviour/*, IPointerClickHandler*/ {

    [SerializeField] private Selectable selectable;
    [Header("Anchored Position: PosX and PosY")]
    [SerializeField] float[] selectionAnchoredPosition = { 0, 0 };
    [Header("Size Delta: Width, Height")]
    [SerializeField] float[] selectionSizeDelta = { 256.0f, 128.0f };

    public Selector selector;

    [ContextMenu("Assign")]
    void Assign() {
        selectable = GetComponent<Selectable>();
    }

    void Reset() {
        selectable = GetComponent<Selectable>();
    }

    void Awake() {
        if (!selectable) {
            selectable = GetComponent<Selectable>();
        }
        selector = new Selector(
            selectionAnchoredPosition[0],
            selectionAnchoredPosition[1],
            selectionSizeDelta[0],
            selectionSizeDelta[1]);
    }

    //public void OnPointerClick(PointerEventData eventData) {
    //    print("OnPointerClick::::::: " + eventData);
    //}
}