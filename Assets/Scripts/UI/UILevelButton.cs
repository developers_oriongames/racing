﻿using GameServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILevelButton : MonoBehaviour {

	public GameModes gameMode;

	public int levelNumber;
	public Button levelButton;
	public GameObject levelLockPanelMobile;
	public GameObject levelLockPanelTV;
	public TMP_Text levelUnlockConditionText;
	public TMP_Text gameModeText;
	public GameObject lockObject;
	public GameObject starPanelObject;
	public GameObject unlockObject;
	public Image[] starImages = { null, null, null, null, null };


	[ContextMenu("AssignNumber")]
	void AssignNumber() {
		//levelNumber = Convert.ToInt32(gameObject.name.Substring(gameObject.name.Length - 2)) - 1;
	}

	[ContextMenu("AssignRef")]
	void AssignRef() {
		//levelLockPanelMobile = transform.GetChild(1).GetChild(1).gameObject;
		//levelLockPanelTV = transform.GetChild(1).GetChild(2).gameObject;
		gameModeText = transform.GetChild(4).GetComponent<TMP_Text>();
		switch (gameMode) {
			case GameModes.Classic:
				gameModeText.text = Constants.CLASSIC;
				break;
			case GameModes.Versus:
				gameModeText.text = Constants.VERSUS;
				break;
			case GameModes.Elimination:
				gameModeText.text = Constants.ELIMINATION;
				break;
		}
		//levelUnlockConditionText = transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<TMP_Text>();
	}

	void Reset() {
		//levelButton = GetComponent<Button>();
		//unLoackConditionText = transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>();
		//lockObject = transform.GetChild(1).gameObject;
		//starPanelObject = transform.GetChild(0).gameObject;
		//unlockObject = transform.GetChild(2).gameObject;

		//for (int i = 0; i < transform.GetChild(0).childCount; i++) {
		//	starImages[i] = transform.GetChild(0).GetChild(i).GetComponent<Image>();
		//}
	}

	public void IsToActiveButton(bool status) {
		levelButton.enabled = status;
	}
}