﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(Button))]
public class UIButtonPressAnimation : MonoBehaviour, IPointerDownHandler, IPointerUpHandler  {

	[SerializeField] private RectTransform rect;
    private Vector3 originalScale = Vector3.one;
	void Reset(){
		rect = gameObject.GetComponent<RectTransform> ();
		GetComponent<Button> ().transition = Selectable.Transition.None;
    }

    public void OnPointerDown(PointerEventData eventData) {
        originalScale = rect.localScale;
        if (rect != null) {
            rect.DOScale(originalScale * 0.9F, 0.1F).SetEase(Ease.OutQuad).SetUpdate(true);
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (rect != null) {
            rect.DOScale(originalScale, 0.1F).SetEase(Ease.OutQuad).SetUpdate(true);
        }
    }
}
