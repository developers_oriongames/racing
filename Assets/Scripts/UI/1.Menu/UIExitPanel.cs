﻿using UnityEngine;

public class UIExitPanel : MonoBehaviour {
	public bool IsOpen { get; private set; }

	[Header("Exit")]
	[SerializeField] private Transform _transform;

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUIExit(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}