﻿#define ACTIVE_RI_SDK

using DG.Tweening;
using GameServices;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UICarSelectionPanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("Car Selection")]
	[SerializeField] private Transform _transform;
	[SerializeField] private TMP_Text carNameText;
	[SerializeField] private TMP_Text carPriceText;
	[SerializeField] private Image carAccImage;
	[SerializeField] private Image carTopSpeedImage;
	[SerializeField] private Image carHandilingImage;
	[SerializeField] private Image carNitroImage;
	[SerializeField] private GameObject carPriceObj;
	[SerializeField] private RectTransform buyButtonObj;
	[SerializeField] private RectTransform nextButtonObj;
	[SerializeField] private Button buyButton;
	[SerializeField] private Button nextButton;
	[SerializeField] private Image cashImage;
	[SerializeField] private TMP_Text dollarText;
	[SerializeField] private Button rateButton;
	[SerializeField] private Button storeButton;

	[SerializeField] private Button tryThisCarButton;
	[SerializeField] public Text rewardAdNotAvailableText;

	public bool hasWatchedAd = false;
	public int tryThisCarGetId = -1;

	void OnEnable() {
		GameEvents.OnItemPurchased += PurchaseStatusUpadate;
		GameEvents.OnUICarSelection += OnUICarSelection;
		GameEvents.OnCarSelectionChange += OnCarSelectionChange;
	}

    void OnDisable() {
		GameEvents.OnItemPurchased -= PurchaseStatusUpadate;
		GameEvents.OnUICarSelection -= OnUICarSelection;
		GameEvents.OnCarSelectionChange -= OnCarSelectionChange;

	}

	void Start() {
#if ACTIVE_RI_SDK
		ButtonHide();
#else
		rateButton.gameObject.SetActive(false);
		storeButton.gameObject.SetActive(false);
#endif
		ButtonShowAndHide(CarManager.GetCurrentSelectedCar().GetPurchaseStatus());

		if (InputManager.IsInAmazonTv())
			tryThisCarButton.gameObject.SetActive(false);
	}

	public void UpdateWork() {
		Car currentSelectedCar = CarManager.GetCurrentSelectedCar();
		carNameText.text = currentSelectedCar.GetCarName();

		bool isCurrenctActive = GameManager.IsInGameCurrencyActive;
		dollarText.gameObject.SetActive(!isCurrenctActive);
		cashImage.gameObject.SetActive(isCurrenctActive);
		carPriceText.text = isCurrenctActive
			? currentSelectedCar.GetInGameCarPrice().ToString()
			: currentSelectedCar.GetStoreCarPrice().ToString();
		float animationSpeed = Time.deltaTime * 5;

		carAccImage.fillAmount = Mathf.Lerp(carAccImage.fillAmount, currentSelectedCar.GetAcceleration() / 25f, animationSpeed);
		carTopSpeedImage.fillAmount = Mathf.Lerp(carTopSpeedImage.fillAmount, currentSelectedCar.GetTopSpeed() / 400f, animationSpeed);
		carHandilingImage.fillAmount = Mathf.Lerp(carHandilingImage.fillAmount, currentSelectedCar.GetHandling() / 1.5f, animationSpeed);
		carNitroImage.fillAmount = Mathf.Lerp(carNitroImage.fillAmount, currentSelectedCar.GetNitro() / 0.18f, animationSpeed);

		//carPriceObj.SetActive(!currentSelectedCar.GetPurchaseStatus());
		//buyButtonObj.gameObject.SetActive(!currentSelectedCar.GetPurchaseStatus()); 
		//nextButtonObj.gameObject.SetActive(currentSelectedCar.GetPurchaseStatus());
//#if ACTIVE_RI_SDK
		//ButtonShowAndHide(currentSelectedCar.GetPurchaseStatus());
//#endif
	}

	void OnUICarSelection(bool a) {
        if ( a && !CarManager.GetCurrentSelectedCar().GetPurchaseStatus()) {
			if (hasWatchedAd)
			{
				if (CarManager.GetCurrentSelectedCar().GetCarID() == tryThisCarGetId)
				{
					SelectedUIControlller.instance.UpdatedSelectionToEventSystem(nextButton.gameObject);
					return;
				}
			}
			SelectedUIControlller.instance.UpdatedSelectionToEventSystem(buyButtonObj.gameObject);
		}

	}


	public void RewardAdNotAvailableTextEnable(bool enable)
	{
		rewardAdNotAvailableText.gameObject.SetActive(enable);
	}

	public void GiveRewardGiftCar()
	{
		hasWatchedAd = true;
		tryThisCarGetId = CarManager.GetCurrentSelectedCar().GetCarID();
		ButtonShowAndHide(true);
		tryThisCarButton.gameObject.SetActive(false);
		SelectedUIControlller.instance.UpdatedSelectionToEventSystem(nextButton.gameObject);
	}

	private void OnCarSelectionChange(Car obj) {
		if (hasWatchedAd)
		{
			if (obj.GetCarID() == tryThisCarGetId)
			{
				ButtonShowAndHide(true);
				tryThisCarButton.gameObject.SetActive(false);
				//SelectedUIControlller.instance.UpdatedSelectionToEventSystem(nextButton.gameObject);
			}
			else {
				ButtonShowAndHide(obj.GetPurchaseStatus());
				if (obj.GetPurchaseStatus() == true)
				{
					//SelectedUIControlller.instance.UpdatedSelectionToEventSystem(nextButton.gameObject);
				}
				else 
				{
					if (!InputManager.IsInAmazonTv() && RI_Extension.AnalyticsClient.tryNow)
					{
						tryThisCarButton.gameObject.SetActive(true);
					}
					//SelectedUIControlller.instance.UpdatedSelectionToEventSystem(buyButton.gameObject);
				}
			}
		}
		else
		{
			ButtonShowAndHide(obj.GetPurchaseStatus());
			if (obj.GetPurchaseStatus() == true)
			{
				tryThisCarButton.gameObject.SetActive(false);
				//SelectedUIControlller.instance.UpdatedSelectionToEventSystem(nextButton.gameObject);
			}
			else
			{
				if (!InputManager.IsInAmazonTv() && RI_Extension.AnalyticsClient.tryNow)
				{
					tryThisCarButton.gameObject.SetActive(true);
				}
				//SelectedUIControlller.instance.UpdatedSelectionToEventSystem(buyButton.gameObject);
			}
		}
	}

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUICarSelection(status);
		if (status) {
			AdManager.ShowPromoAd(PromoSpot.SPOT_MENU);
		} else {
			AdManager.HidePromoAd();
		}
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}

	void PurchaseStatusUpadate(IAPButtonType iAPButtonType) {
		ButtonShowAndHide(CarManager.GetCurrentSelectedCar().GetPurchaseStatus());
		if (iAPButtonType == IAPButtonType.Others) {
			if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
				//Update the selectoin for tv
				SelectedUIControlller.instance.UpdatedSelectionToEventSystem(nextButtonObj.gameObject);
			}
		}
	}


	#region Shop
	void ButtonHide() {
		nextButtonObj.DOAnchorPos3DY(-100, 0.2f);
		buyButtonObj.DOAnchorPos3DY(-100, 0.2f);
	}
	public void ButtonShowAndHide(bool isUnlocked) {
	 
#if !ACTIVE_RI_SDK
		isUnlocked = true;
#endif
		if (isUnlocked) {
			nextButtonObj.DOAnchorPos3DY(100, 0.2f).SetEase(Ease.Linear).OnComplete(() => {
				//set the button interective
				nextButton.interactable = true;
			});
			buyButtonObj.DOAnchorPos3DY(-100, 0.2f).SetEase(Ease.Linear).OnComplete(() => {
				//set the button un-interective
				buyButton.interactable = false;
			});
			tryThisCarButton.gameObject.SetActive(false);

			/// new added
			//if (nextButton.interactable == true)
			//{
			//	SelectedUIControlller.instance.UpdatedSelectionToEventSystem(nextButton.gameObject);
			//}
			//else
			//{
			//	SelectedUIControlller.instance.UpdatedSelectionToEventSystem(buyButton.gameObject);
			//}

		}
		else {
			buyButtonObj.DOAnchorPos3DY(100, 0.2f).SetEase(Ease.Linear).OnComplete(() => {
				//set the button interective
				buyButton.interactable = true;
			});
			nextButtonObj.DOAnchorPos3DY(-100, 0.2f).SetEase(Ease.Linear).OnComplete(() => {
				//set the button un-interective
				nextButton.interactable = false;
			});

			//tryThisCarButton.gameObject.SetActive(true);
			//SelectedUIControlller.instance.UpdatedSelectionToEventSystem(tryThisCarButton.gameObject);
		}
	}
#endregion
}