﻿using GameServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITopBarPanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("Top Bar")]
	[SerializeField] private Transform _transform;
	[SerializeField] private Image topBarBgImage;
	[SerializeField] private Image backButtonImage;
	[SerializeField] private Image totalCashImage;
	[SerializeField] private Image totalStarImage;
	[SerializeField] private TMP_Text totalCashText;
	[SerializeField] private TMP_Text totalStarText;
	[SerializeField] private Button backButton;
	[SerializeField] private Button settingsButton;

	void OnEnable() {
		GameEvents.OnUIStore += SetTopBarElements;
		GameEvents.OnUICarPreview += SetTopBarElements;
		GameEvents.OnUISettings += SetTopBarElements;
		GameEvents.OnUIExit += SetTopBarElements;
		GameEvents.OnUIHelp += SetTopBarElements;
		GameEvents.OnUIRate += SetTopBarElements;
	}

	void OnDisable() {
		GameEvents.OnUIStore -= SetTopBarElements;
		GameEvents.OnUICarPreview -= SetTopBarElements;
		GameEvents.OnUISettings -= SetTopBarElements;
		GameEvents.OnUIExit -= SetTopBarElements;
		GameEvents.OnUIHelp -= SetTopBarElements;
		GameEvents.OnUIRate -= SetTopBarElements;
	}

	void Start() {
		totalCashImage.gameObject.SetActive(GameManager.IsInGameCurrencyActive);
		InvokeRepeating("_UpdateTextInfo", 0, 1);
	}

	void SetTopBarElements(bool isOpen) {
		//print("open:::: " + "isOpen: " + isOpen + "    UIManager.CurrentScreen:" + UIManager.CurrentScreen);
		if (GameManager.IsInGameCurrencyActive) { totalCashImage.gameObject.SetActive(!isOpen); }
		totalStarImage.gameObject.SetActive(!isOpen);
		settingsButton.gameObject.SetActive(!isOpen);
	}

	/// <summary>
	/// Invoked Method
	/// </summary>
	public void _UpdateTextInfo() {
		totalCashText.text = CurrencyManager.TotalCash.ToString();
		totalStarText.text = LevelManager.GetTotalStar() + Constants.SLASH + (LevelManager.LevStar.Length * 5);
	}

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		GameEvents.OnUITopBar(status);
		_transform.gameObject.SetActive(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}

	public void SelectTopBarBackButton() {
		SelectedUIControlller.instance.UpdatedSelectionToEventSystem(backButton.gameObject);
	}
}
