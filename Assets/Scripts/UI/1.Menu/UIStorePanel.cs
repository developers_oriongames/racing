﻿using DG.Tweening;
using GameServices;
using GameServices.UserData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIStorePanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("Store")]
	[SerializeField] private Transform _transform;
	[Space]
	[SerializeField] private ScrollRect shopScrollRect;
	[SerializeField] private RectTransform storeCashPasks;
	[SerializeField] private RectTransform storeOtherPasks;
	[Space]
	[SerializeField] private Image storeTotalCashImage;
	[SerializeField] private TMP_Text storeTotalCashText;
	[SerializeField] private TMP_Text storeTitleText;
	[Space]
	[SerializeField] private RectTransform storeIAPSetsRect;
	[SerializeField] private Button storeIAPSet1Button;
	[SerializeField] private Button storeIAPSet2Button;
	[Space]
	[SerializeField] private RectTransform storeNavigationRect;
	[SerializeField] private Image storeIAPSet1DotImage;
	[SerializeField] private Image storeIAPSet2DotImage;
	[Space]
	[SerializeField] private Button storeRemoveAdsButton;
	[SerializeField] private Button storeUnlockFullGameButton;
	[SerializeField] private Button storeUnlockAllCarsButton;
	[SerializeField] private Button storeUnlockAllLevelsButton;
	[Space]
	[SerializeField] private TMP_Text storeRemoveAdsPurchasedText;
	[SerializeField] private TMP_Text storeUnlockFullGamePurchasedText;
	[SerializeField] private TMP_Text storeUnlockAllCarsPurchasedText;
	[SerializeField] private TMP_Text storeUnlockAllLevelsPurchasedText;

	void OnEnable() {
		GameEvents.OnItemPurchased += PurchaseStatusUpadate;
		PurchaseStatusUpadate();
		//Invoke("SelectPurchaseStatus", 0.1f);
	}

	void OnDisable() {
		GameEvents.OnItemPurchased -= PurchaseStatusUpadate;
	}

    void Awake() {
		//SelectPurchaseStatus();
	}

	void Start() {
		IapSet(InAppPurchaseType.Consumable);
		bool isToActive = GameManager.IsInGameCurrencyActive;
		storeTotalCashImage.gameObject.SetActive(isToActive);
		storeIAPSetsRect.gameObject.SetActive(isToActive);
		storeNavigationRect.gameObject.SetActive(isToActive);
		storeRemoveAdsButton.gameObject.SetActive(InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv);
		PurchaseStatusUpadate();
	}

	void PurchaseStatusUpadate(IAPButtonType iAPButtonType = IAPButtonType.Store) {

		storeRemoveAdsPurchasedText.gameObject.SetActive(false);
		storeUnlockFullGamePurchasedText.gameObject.SetActive(false);
		storeUnlockAllCarsPurchasedText.gameObject.SetActive(false);
		storeUnlockAllLevelsPurchasedText.gameObject.SetActive(false);

		if (UserDataManager.GetRemoveAdsPurchaseStatus()) {
			UISelectionOnTV(storeRemoveAdsButton, storeRemoveAdsPurchasedText);
		}
		if (UserDataManager.GetUnlockFullGamePurchaseStatus()) { 
			UISelectionOnTV(storeUnlockFullGameButton, storeUnlockFullGamePurchasedText);
		}
		if (UserDataManager.GetUnlockAllCarsPurchaseStatus()) { 
			UISelectionOnTV(storeUnlockAllCarsButton, storeUnlockAllCarsPurchasedText);
		}
		if (UserDataManager.GetUnlockAllLevelsPurchaseStatus()) {
            UISelectionOnTV(storeUnlockAllLevelsButton, storeUnlockAllLevelsPurchasedText);
        }

        void UISelectionOnTV(Button button, TMP_Text text) {	
			button.interactable = false;
			//button.enabled = false;
			text.gameObject.SetActive(true);
			if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
				SelectedUIControlller.instance.RemoveSelectionFromEventSystem();
				UIManager.Instance.uIMenu.topBarPanel.SelectTopBarBackButton();
			}
		}
    }

	void Update() {
		storeTotalCashText.text = CurrencyManager.TotalCash.ToString();
	}

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUIStore(status);

	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}

	public void IapSet(InAppPurchaseType inAppPurchaseType) {
		if (!GameManager.IsInGameCurrencyActive) {
			inAppPurchaseType = InAppPurchaseType.NonConsumable; // If in-game-currency is disabled, then only show other IAPs
		}
		storeIAPSet1DotImage.color = storeIAPSet2DotImage.color = UIManager.Instance.uIMenu.setDotDeactiveColor;
		storeIAPSet1Button.image.sprite = storeIAPSet2Button.image.sprite = UIManager.Instance.uIMenu.setButtonDeactiveSprite;
		switch (inAppPurchaseType) {
			case InAppPurchaseType.Consumable:
				shopScrollRect.content = storeCashPasks;
				storeCashPasks.gameObject.SetActive(true);
				storeOtherPasks.gameObject.SetActive(false);
				storeIAPSet1DotImage.DOColor(UIManager.Instance.uIMenu.setDotActiveColor, 0.2f);
				storeIAPSet2DotImage.DOColor(UIManager.Instance.uIMenu.setDotDeactiveColor, 0.2f);
				storeIAPSet1Button.image.sprite = UIManager.Instance.uIMenu.setButtonActiveSprite;
				break;
			case InAppPurchaseType.NonConsumable:
				shopScrollRect.content = storeOtherPasks;
				storeCashPasks.gameObject.SetActive(false);
				storeOtherPasks.gameObject.SetActive(true);
				storeIAPSet1DotImage.DOColor(UIManager.Instance.uIMenu.setDotDeactiveColor, 0.2f);
				storeIAPSet2DotImage.DOColor(UIManager.Instance.uIMenu.setDotActiveColor, 0.2f);
				storeIAPSet2Button.image.sprite = UIManager.Instance.uIMenu.setButtonActiveSprite;
				break;
		}
	}

	public void UpdateTitle(string message) {
		storeTitleText.text = GameManager.IsInGameCurrencyActive ? message : Constants.STORE;
	}
}