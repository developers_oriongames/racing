﻿using UnityEngine;

public class UIHelpPanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("Help Panel")]
	[SerializeField] private Transform _transform;

	[Header("Remote")]
	[SerializeField] private RectTransform remoteRect;

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUIHelp(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}
