﻿using UnityEngine;
using UnityEngine.UI;
using GameServices;
using TMPro;

public class UISettingsPanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("Setting")]
	[SerializeField] private Transform _transform;

	[Header("Normal Popup")]
	[SerializeField] private RectTransform popupRect;
	[SerializeField] private Slider soundSlider;
	[SerializeField] private Slider musicSlider;
	[SerializeField] private Image sensorControlImage;
	[SerializeField] private Image sensorSelectImage;
	[SerializeField] private Image touchControlImage;
	[SerializeField] private Image touchSelectImage;
	[SerializeField] private Toggle sensorToggle;
	[SerializeField] private Toggle touchToggle;
	[Header("TV Popup")]
	[SerializeField] private RectTransform popupRectTv;
	[SerializeField] private Slider soundSliderTv;
	[SerializeField] private Slider musicSliderTv;
	[Header("Version")]
	[SerializeField] private TMP_Text versionText;

	void OnEnable() {
		popupRect.gameObject.SetActive(false);
		popupRectTv.gameObject.SetActive(false);
		if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
			popupRectTv.gameObject.SetActive(true);
		} else {
			popupRect.gameObject.SetActive(true);
		}
	}

    void Start() {
		soundSlider.value = soundSliderTv.value = AudioManager.soundVolume;
		musicSlider.value = musicSliderTv.value = AudioManager.musicVolume;
		versionText.text = "Version: " + Constants.BUNDLE_VERSION;
	}

	public void UpdateWork() {
        //Color clr = sensorControlImage.color;
        //clr.a = GameManager.currentControllerType == ControllerType.Sensor ? 1 : 0.5f;
        //sensorControlImage.color = clr;
        //sensorSelectImage.gameObject.SetActive(GameManager.currentControllerType == ControllerType.Sensor);
        sensorToggle.isOn = InputManager.GetCurrentController() == ControllerTypes.Sensor;
        //clr = touchControlImage.color;
        //clr.a = GameManager.currentControllerType == ControllerType.Touch ? 1 : 0.5f;
        //touchControlImage.color = clr;
        //touchSelectImage.gameObject.SetActive(GameManager.currentControllerType == ControllerType.Touch);
        touchToggle.isOn = InputManager.GetCurrentController() == ControllerTypes.Touch;
	}

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUISettings(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}