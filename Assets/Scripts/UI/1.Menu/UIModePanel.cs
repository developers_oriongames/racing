﻿using GameServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIModePanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("GameMode")]
	[SerializeField] private Transform _transform;
	[SerializeField] private TMP_Text gameModeNameText;
	[SerializeField] private Image trackImage;


	public void UpdateWork() {
		gameModeNameText.text = GameModeManager.GameModeName();
		trackImage.sprite = UIManager.GetMapSprite();
	}

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUIGameMode(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}
