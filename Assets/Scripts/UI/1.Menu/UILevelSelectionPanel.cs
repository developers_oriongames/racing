﻿using DG.Tweening;
using GameServices;
using GameServices.UserData;
using UnityEngine;
using UnityEngine.UI;

public class UILevelSelectionPanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("Level")]
	[SerializeField] private Transform _transform;
	[SerializeField] private UILevelButton[] levelButtons;
	[Space]
	[SerializeField] private ScrollRect levelScrollRect;
	[SerializeField] private RectTransform classicLevelsHolder;
	[SerializeField] private RectTransform eliminationLevelsHolder;
	[SerializeField] private RectTransform versusHolder;
	[Space]
	[SerializeField] private RectTransform levelSetsRect;
	[SerializeField] private Button levelClassicButton;
	[SerializeField] private Button levelEliminationButton;
	[SerializeField] private Button levelVersusButton;
	[Space]
	[SerializeField] private RectTransform levelNavigationRect;
	[SerializeField] private Image levelClassicDotImage;
	[SerializeField] private Image levelEliminationDotImage;
	[SerializeField] private Image levelVersusDotImage;

	void Start() {
		if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
			levelScrollRect.movementType = ScrollRect.MovementType.Clamped;
		} else {
			levelScrollRect.movementType = ScrollRect.MovementType.Elastic;
		}
	}

    public void UpdateWork()
    {
        for (int i = 0; i < levelButtons.Length; i++)
        {
            SetButtonElements(i);
        }
	
        void SetButtonElements(int i)
        {
            levelButtons[i].levelLockPanelMobile.SetActive(false);
            levelButtons[i].levelLockPanelTV.SetActive(true);
            SetText(i);
            levelButtons[i].lockObject.SetActive(
                InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv
                ? !LevelUnlockConditionTV(levelButtons[i].levelNumber)
                : !LevelUnlockConditionMobile(levelButtons[i].levelNumber));
            bool isActive = levelButtons[i].lockObject.activeSelf;
            levelButtons[i].starPanelObject.SetActive(!isActive);
            levelButtons[i].unlockObject.SetActive(!isActive && LevelManager.IsToShowTickMark(i));
            for (int j = 0; j < levelButtons[i].starImages.Length; j++)
            {
                if (j < LevelManager.LevStar[levelButtons[i].levelNumber])
                {
                    levelButtons[i].starImages[j].sprite = UIManager.Instance.starSprite;
                }
            }
        }

        void SetText(int i)
        {
            if (InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv)
            {
				////Level Unlock Condition Text
				//if (LevelManager.LevelUnlockConditionMobile[levelButtons[i].levelNumber, 1] < 1)
				//{
				//    //unlock with stars	//uncomment to show level unlock info in tab
				//    levelButtons[i].levelUnlockConditionText.text
				//        = LevelManager.LevelUnlockConditionMobile[levelButtons[i].levelNumber, 0] + Constants._NEEDED_TO_UNLOCK;
				//}
				//else
				//{
				//    //unlock with cars //uncomment to show level unlock info in tab
				//    levelButtons[i].levelUnlockConditionText.text
				//        = CarManager.Instance.cars[LevelManager.LevelUnlockConditionMobile[levelButtons[i].levelNumber, 1]].GetCarName() + Constants._TO_UNLOCK;
				//}
				levelButtons[i].levelUnlockConditionText.text = "";
            }
            else
            {
                ////Level Unlock Condition Text
                //levelButtons[i].levelUnlockConditionText.text = "Buy Full Game To Unlock!"; //uncomment to show level unlock info in tv
            }
        }
    }

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUILevelSelection(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}

	public void LevelSet(GameModes gameModes) {
		levelClassicButton.image.sprite
			= levelEliminationButton.image.sprite
			= levelVersusButton.image.sprite
			= UIManager.Instance.uIMenu.setButtonDeactiveSprite;
		levelClassicDotImage.color
			= levelEliminationDotImage.color
			= levelVersusDotImage.color
			= UIManager.Instance.uIMenu.setDotDeactiveColor;
		classicLevelsHolder.gameObject.SetActive(false);
		eliminationLevelsHolder.gameObject.SetActive(false);
		versusHolder.gameObject.SetActive(false);
		switch (gameModes) {
			case GameModes.Classic:
				levelScrollRect.content = classicLevelsHolder;
				levelClassicButton.image.sprite
					= UIManager.Instance.uIMenu.setButtonActiveSprite;
				levelClassicDotImage.DOColor(UIManager.Instance.uIMenu.setDotActiveColor, 0.2f);
				classicLevelsHolder.gameObject.SetActive(true);
				break;
			case GameModes.Elimination:
				levelScrollRect.content = eliminationLevelsHolder;
				levelEliminationButton.image.sprite
					= UIManager.Instance.uIMenu.setButtonActiveSprite;
				levelEliminationDotImage.DOColor(UIManager.Instance.uIMenu.setDotActiveColor, 0.2f);
				eliminationLevelsHolder.gameObject.SetActive(true);
				break;
			case GameModes.Versus:
				levelScrollRect.content = versusHolder;
				levelVersusButton.image.sprite
					= UIManager.Instance.uIMenu.setButtonActiveSprite;
				levelVersusDotImage.DOColor(UIManager.Instance.uIMenu.setDotActiveColor, 0.2f);
				versusHolder.gameObject.SetActive(true);
				break;
			default:
				levelScrollRect.content = classicLevelsHolder;
				levelClassicButton.image.sprite
					= UIManager.Instance.uIMenu.setButtonActiveSprite;
				levelClassicDotImage.DOColor(UIManager.Instance.uIMenu.setDotActiveColor, 0.2f);
				classicLevelsHolder.gameObject.SetActive(true);
				break;
		}
	}
	bool LevelUnlockConditionMobile(int k) {
		//int requiedStars = LevelManager.LevelUnlockConditionMobile[k, 0];
		//int requiedCarNo = LevelManager.LevelUnlockConditionMobile[k, 1];
		//return (GameManager.isToUnlockAllLevels                                                     //for Test unlock all cars
		//		|| ((LevelManager.GetTotalStar() >= requiedStars)                                       //have required amount of stars
		//			&& CarManager.Instance.cars[requiedCarNo].GetPurchaseStatus())                 //have required car
		//		|| UserDataManager.GetUnlockAllLevelsPurchaseStatus());                             //unlock full game purchased
        return LevelManager.IsLevelUnlocked(k);
	}

	bool LevelUnlockConditionTV(int k) {
		//int requiedStars = LevelManager.LevelUnlockConditionTV[k, 0];
		//int requiedCarNo = LevelManager.LevelUnlockConditionTV[k, 1];
		//return (GameManager.isToUnlockAllLevels                                                     //for Test unlock all cars
		//		|| ((LevelManager.GetTotalStar() >= requiedStars)                                       //have required amount of stars
		//			&& CarManager.Instance.cars[requiedCarNo].GetPurchaseStatus())                 //have required car
		//		|| UserDataManager.GetUnlockAllLevelsPurchaseStatus());                             //unlock full game purchased
        return LevelManager.IsLevelUnlocked(k);
    }
}