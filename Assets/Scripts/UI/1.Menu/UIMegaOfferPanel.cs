﻿using GameServices.UserData;
using UnityEngine;

public class UIMegaOfferPanel : MonoBehaviour
{
	public bool IsOpen { get; private set; }

	[Header("Mega Offer Panel")]
	[SerializeField] private Transform _transform;
	[SerializeField] private GameObject offerObj;
	[SerializeField] private GameObject resultObj;
	[SerializeField] private GameObject okButtonObj;

	void OnEnable()
	{
		GameEvents.OnItemPurchased += PurchaseStatusUpadate;
		offerObj.SetActive(true);
		resultObj.SetActive(false);
		PurchaseStatusUpadate();
	}
    void OnDisable()
	{
		GameEvents.OnItemPurchased -= PurchaseStatusUpadate;
	}

    private void PurchaseStatusUpadate(IAPButtonType iAPButtonType = IAPButtonType.Store)
	{
		if (UserDataManager.GetUnlockFullGamePurchaseStatus())
		{
			offerObj.SetActive(false);
			resultObj.SetActive(true);
			SelectedUIControlller.instance.UpdatedSelectionToEventSystem(okButtonObj);
			//UIManager.Instance.uIMenu.CloseMegaOffer();
		}
	}

	public void ActiveDeactive(bool status)
	{
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUIMegaOffer(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement)
	{
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}
