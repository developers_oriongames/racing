﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIRatePanel : MonoBehaviour
{
    public bool IsOpen { get; private set; }

    [Header("Rate Panel")]
    [SerializeField] private Transform _transform;
	[SerializeField] private Button[] rateStarButtons;
	[SerializeField] private Button laterButton;
	[SerializeField] private Button rateButton;
	[SerializeField] private Button okButton;
	[SerializeField] private TMP_Text rateText;

	public Sprite rateStarBgSprite;
	public Sprite rateStarSprite;

    void OnEnable() {
        for (int i = 0; i < rateStarButtons.Length; i++) {
			rateStarButtons[i].image.sprite = rateStarBgSprite;
        }
		rateButton.interactable = false;
		rateText.fontSize = 60;
		rateText.text = "Why not share the love with a great review!";
		laterButton.gameObject.SetActive(true);
		rateButton.gameObject.SetActive(true);
		okButton.gameObject.SetActive(false);
	}

    public void AssignStarSprite(int index) {
		rateButton.interactable = true;
		if (index >= 4) {
			RateManager.isEligibleToRate = true;
		} else {
			RateManager.isEligibleToRate = false;
		}
		for (int i = 0; i < rateStarButtons.Length; i++) {
			rateStarButtons[i].image.sprite = rateStarBgSprite;
        }
        for (int i = 0; i < index; i++) {
			rateStarButtons[i].image.sprite = rateStarSprite;
        }
    }

	public void FakeRateWorks() {
		rateText.fontSize = 70;
		rateText.text = "Thank you for your feedback!";
		laterButton.gameObject.SetActive(false);
		rateButton.gameObject.SetActive(false);
		okButton.gameObject.SetActive(true);
	}

	public void ActualRateWorks() {
		UIManager.Instance.uIMenu.CloseRate();
	}

	public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUIRate(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}
