﻿using GameServices;
using UnityEngine;
using UnityEngine.UI;

public class UICarPreviewPanel : MonoBehaviour {

	public bool IsOpen { get; private set; }

	[Header("Preview")]
	[SerializeField] private Transform _transform;

	[SerializeField] private Button closeButton;

    void Awake() {
        closeButton.gameObject.SetActive(InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv);
    }

    public void ActiveDeactive(bool status) {
		IsOpen = status;
		_transform.gameObject.SetActive(status);
		GameEvents.OnUICarPreview(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}
