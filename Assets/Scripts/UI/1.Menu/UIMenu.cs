﻿using UnityEngine;
using GameServices;
using GameServices.UserData;

public class UIMenu : MonoBehaviour {

	[Header("Common Assets")]
	public Color setDotActiveColor;
	public Color setDotDeactiveColor;
	public Sprite setButtonActiveSprite;
	public Sprite setButtonDeactiveSprite;

	[Header("Ref")]
	public UICarSelectionPanel carSelectionPanel;
	public UILevelSelectionPanel levelSelectionPanel;
	public UIModePanel modePanel;
	public UITopBarPanel topBarPanel;
	public UIStorePanel storePanel;
	public UICarPreviewPanel carPreviewPanel;
	public UISettingsPanel settingsPanel;
	public UIExitPanel exitPanel;
	public UIHelpPanel helpPanel;
	public UIRatePanel ratePanel;
	public UIMegaOfferPanel megaOfferPanel;

	void Awake() {
		//active
		carSelectionPanel.ActiveDeactive(true);
		topBarPanel.ActiveDeactive(true);

		//inactive
		levelSelectionPanel.ActiveDeactive(false);
		modePanel.ActiveDeactive(false);
		settingsPanel.ActiveDeactive(false);
		exitPanel.ActiveDeactive(false);
		storePanel.ActiveDeactive(false);
		carPreviewPanel.ActiveDeactive(false);
		helpPanel.ActiveDeactive(false);
		ratePanel.ActiveDeactive(false);
		megaOfferPanel.ActiveDeactive(false);

		levelSelectionPanel.DoAnimation(AnimationPlacement.Right);
		modePanel.DoAnimation(AnimationPlacement.Right);
		settingsPanel.DoAnimation(AnimationPlacement.Right);
		exitPanel.DoAnimation(AnimationPlacement.Left);
		storePanel.DoAnimation(AnimationPlacement.Right);
		carPreviewPanel.DoAnimation(AnimationPlacement.Left);
		helpPanel.DoAnimation(AnimationPlacement.Left);
		ratePanel.DoAnimation(AnimationPlacement.Right);
		megaOfferPanel.DoAnimation(AnimationPlacement.Right);
	}

	void Start()
	{
		if (!UserDataManager.GetMegaOfferShownStatus() && !UserDataManager.GetUnlockFullGamePurchaseStatus())
		{
			OpenMegaOffer();
			UserDataManager.SetMegaOfferShownStatus(true);
		}
	}

    void Update() {
		if (Navigation.IsInMenuScene()) {
			switch (UIManager.CurrentScreen) {
				case GameScreen.LevelSelectScreen:
					levelSelectionPanel.UpdateWork();
					break;
				case GameScreen.CarSelectScreen:
					carSelectionPanel.UpdateWork();
					break;
				case GameScreen.GameModeScreen:
					modePanel.UpdateWork();
					break;
				case GameScreen.SettingScreen:
					settingsPanel.UpdateWork();
					break;
			}
		}
	}

	#region Car Selection
	public void OpenCarSelection() {
		if (UIManager.CurrentScreen == GameScreen.GameModeScreen) {
			return;
		}
		UIManager.CurrentScreen = GameScreen.CarSelectScreen;
		carSelectionPanel.ActiveDeactive(true);
		carSelectionPanel.DoAnimation(AnimationPlacement.Middle);
	}

	public void CloseCarSelection() {
		UIManager.PreviousScreen = GameScreen.CarSelectScreen;
		carSelectionPanel.DoAnimation(AnimationPlacement.Left);
		carSelectionPanel.ActiveDeactive(false);
	}
	#endregion

	#region Game Level
	public void OpenLevel() {
		UIManager.CurrentScreen = GameScreen.LevelSelectScreen;
		levelSelectionPanel.ActiveDeactive(true);
		levelSelectionPanel.DoAnimation(AnimationPlacement.Middle);
		CarManager.SelectCar();
		CloseCarSelection();
	}
	public void CloseLevel() {
		UIManager.PreviousScreen = GameScreen.LevelSelectScreen;
		OpenCarSelection();
		switch (UIManager.CurrentScreen) {
			case GameScreen.CarSelectScreen:
				levelSelectionPanel.DoAnimation(AnimationPlacement.Right);
				break;
			case GameScreen.GameModeScreen:
				levelSelectionPanel.DoAnimation(AnimationPlacement.Left);
				break;
		}
		levelSelectionPanel.ActiveDeactive(false);
	}

    public void SelectLevelSet(GameModes gameModes) {
		levelSelectionPanel.LevelSet(gameModes);
	}
	#endregion

	#region Game Mode
	public void OpenGameMode() {
		UIManager.CurrentScreen = GameScreen.GameModeScreen;
		modePanel.ActiveDeactive(true);
		modePanel.DoAnimation(AnimationPlacement.Middle);
		CloseLevel();
	}
	public void CloseGameMode() {
		UIManager.PreviousScreen = GameScreen.GameModeScreen;
		OpenLevel();
		modePanel.DoAnimation(AnimationPlacement.Right);
		levelSelectionPanel.DoAnimation(AnimationPlacement.Middle);
		modePanel.ActiveDeactive(false);
	}
	#endregion

	#region Settings
	public void OpenSetting() {
		UIManager.PreviousScreen = UIManager.CurrentScreen;
		UIManager.CurrentScreen = GameScreen.SettingScreen;
		settingsPanel.ActiveDeactive(true);
		settingsPanel.DoAnimation(AnimationPlacement.Middle);
		//deactive the previous screen	
		switch (UIManager.PreviousScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.DoAnimation(AnimationPlacement.Left);
				carSelectionPanel.ActiveDeactive(false);
				break;
			case GameScreen.LevelSelectScreen:
				levelSelectionPanel.DoAnimation(AnimationPlacement.Left);
				levelSelectionPanel.ActiveDeactive(false);
				break;
			case GameScreen.GameModeScreen:
				modePanel.DoAnimation(AnimationPlacement.Left);
				modePanel.ActiveDeactive(false);
				break;
		}
	}
	public void CloseSetting() {
		UIManager.CurrentScreen = UIManager.PreviousScreen;
		UIManager.PreviousScreen = GameScreen.SettingScreen;
		//active the previous screen
		switch (UIManager.CurrentScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.ActiveDeactive(true);
				carSelectionPanel.DoAnimation(AnimationPlacement.Middle);
				break;
			case GameScreen.LevelSelectScreen:
				levelSelectionPanel.ActiveDeactive(true);
				levelSelectionPanel.DoAnimation(AnimationPlacement.Middle);
				break;
			case GameScreen.GameModeScreen:
				modePanel.ActiveDeactive(true);
				modePanel.DoAnimation(AnimationPlacement.Middle);
				break;
		}
		settingsPanel.DoAnimation(AnimationPlacement.Right);
		settingsPanel.ActiveDeactive(false);
	}
	#endregion

	#region Help
	public void OpenHelp() {
		//UIManager.PreviousScreen = GameScreen.SettingScreen;
		UIManager.CurrentScreen = GameScreen.HelpScreen;
		helpPanel.ActiveDeactive(true);
		helpPanel.DoAnimation(AnimationPlacement.Middle);
		topBarPanel.ActiveDeactive(false);
		settingsPanel.ActiveDeactive(false);
	}
	public void CloseHelp() {
		//UIManager.PreviousScreen = GameScreen.HelpScreen;
		UIManager.CurrentScreen = GameScreen.SettingScreen;
		helpPanel.DoAnimation(AnimationPlacement.Left);
		helpPanel.ActiveDeactive(false);
		topBarPanel.ActiveDeactive(true);
		settingsPanel.ActiveDeactive(true);
	}
	#endregion

	#region Rate
	public void OpenRate() {
		UIManager.PrePreviousScreen = UIManager.PreviousScreen;
		UIManager.PreviousScreen = UIManager.CurrentScreen;
		UIManager.CurrentScreen = GameScreen.RateScreen;
		ratePanel.ActiveDeactive(true);
		ratePanel.DoAnimation(AnimationPlacement.Middle);
		topBarPanel.ActiveDeactive(false);
		//deactive the previous screen	
		switch (UIManager.PreviousScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.DoAnimation(AnimationPlacement.Left);
				carSelectionPanel.ActiveDeactive(false);
				break;
			case GameScreen.ExitScreen:
				exitPanel.DoAnimation(AnimationPlacement.Left);
				exitPanel.ActiveDeactive(false);
				break;
			case GameScreen.SettingScreen:
				settingsPanel.DoAnimation(AnimationPlacement.Left);
				settingsPanel.ActiveDeactive(false);
				break;
		}
	}
	public void CloseRate() {
		UIManager.CurrentScreen = UIManager.PreviousScreen;
		UIManager.PreviousScreen = UIManager.PrePreviousScreen;
		//active the previous screen
		switch (UIManager.CurrentScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.ActiveDeactive(true);
				carSelectionPanel.DoAnimation(AnimationPlacement.Middle);
				break;
			case GameScreen.ExitScreen:
				exitPanel.ActiveDeactive(true);
				exitPanel.DoAnimation(AnimationPlacement.Middle);
				break;
			case GameScreen.SettingScreen:
				settingsPanel.ActiveDeactive(true);
				settingsPanel.DoAnimation(AnimationPlacement.Middle);
				break;
		}
		topBarPanel.ActiveDeactive(true);
		ratePanel.ActiveDeactive(false);
		ratePanel.DoAnimation(AnimationPlacement.Right);
	}
	#endregion

	#region Mega Offer
	public void OpenMegaOffer()
	{
		CloseCarSelection();
		UIManager.PreviousScreen = GameScreen.CarSelectScreen;
		UIManager.CurrentScreen = GameScreen.MegaOfferScreen;
		megaOfferPanel.ActiveDeactive(true);
		megaOfferPanel.DoAnimation(AnimationPlacement.Middle);
		topBarPanel.ActiveDeactive(false);
	}
	public void CloseMegaOffer()
	{
		OpenCarSelection();
		UIManager.PreviousScreen = GameScreen.HelpScreen;
		UIManager.CurrentScreen = GameScreen.CarSelectScreen;
		megaOfferPanel.DoAnimation(AnimationPlacement.Left);
		megaOfferPanel.ActiveDeactive(false);
		topBarPanel.ActiveDeactive(true);
	}
	#endregion

	#region Car Preview
	public void OpenCarPreview() {
		carPreviewPanel.ActiveDeactive(true);
		carSelectionPanel.DoAnimation(AnimationPlacement.Left);
		carSelectionPanel.ActiveDeactive(false);
		carPreviewPanel.DoAnimation(AnimationPlacement.Middle);
		UIManager.CurrentScreen = GameScreen.PreviewScreen;
		UIManager.PreviousScreen = GameScreen.CarSelectScreen;
		Camera.main.GetComponent<TouchOrbit>().OnTouchCamera();
	}

	public void CloseCarPreview() {
		carPreviewPanel.DoAnimation(AnimationPlacement.Left);
		carSelectionPanel.ActiveDeactive(true);
		carSelectionPanel.DoAnimation(AnimationPlacement.Middle);
		Camera.main.GetComponent<TouchOrbit>().SetCamera();
		UIManager.CurrentScreen = GameScreen.CarSelectScreen;
		UIManager.PreviousScreen = GameScreen.PreviewScreen;
		topBarPanel.ActiveDeactive(true);
		carPreviewPanel.ActiveDeactive(false);
	}
	#endregion

	#region Exit pop-up
	public void ShowExitPopup() {
		GameShop.SetCorrectCar();
		UIManager.CurrentScreen = GameScreen.ExitScreen;
		UIManager.PreviousScreen = GameScreen.CarSelectScreen;
		exitPanel.ActiveDeactive(true);
		exitPanel.DoAnimation(AnimationPlacement.Middle);
		//deactive the previous screen	
		switch (UIManager.PreviousScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.DoAnimation(AnimationPlacement.Right);
				carSelectionPanel.ActiveDeactive(false);
				break;
		}
	}
	public void ExitYes() {
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}
	public void ExitNo() {
		UIManager.CurrentScreen = GameScreen.CarSelectScreen;
		UIManager.PreviousScreen = GameScreen.ExitScreen;
		//active the previous screen
		switch (UIManager.CurrentScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.ActiveDeactive(true);
				carSelectionPanel.DoAnimation(AnimationPlacement.Middle);
				break;
		}
		exitPanel.DoAnimation(AnimationPlacement.Left);
		exitPanel.ActiveDeactive(false);
	}
	#endregion

	#region Store
	public void OpenStore(string message) {
		storePanel.UpdateTitle(message);
		UIManager.PreviousScreen = UIManager.CurrentScreen;
		UIManager.CurrentScreen = GameScreen.StoreScreen;
		storePanel.ActiveDeactive(true);
		storePanel.DoAnimation(AnimationPlacement.Middle);
		storePanel.IapSet(InAppPurchaseType.Consumable);
		//deactive the previous screen	
		switch (UIManager.PreviousScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.DoAnimation(AnimationPlacement.Left);
				carSelectionPanel.ActiveDeactive(false);
				break;
			case GameScreen.LevelSelectScreen:
				levelSelectionPanel.DoAnimation(AnimationPlacement.Left);
				levelSelectionPanel.ActiveDeactive(false);
				break;
		}
	}
	public void CloseStore() {
		UIManager.CurrentScreen = UIManager.PreviousScreen;
		UIManager.PreviousScreen = GameScreen.StoreScreen;
		//active the previous screen
		switch (UIManager.CurrentScreen) {
			case GameScreen.CarSelectScreen:
				carSelectionPanel.ActiveDeactive(true);
				carSelectionPanel.DoAnimation(AnimationPlacement.Middle);
				break;
			case GameScreen.LevelSelectScreen:
				levelSelectionPanel.ActiveDeactive(true);
				levelSelectionPanel.DoAnimation(AnimationPlacement.Middle);
				break;
		}
		storePanel.DoAnimation(AnimationPlacement.Right);
		storePanel.ActiveDeactive(false);
	}
	public void SelectIapSet(InAppPurchaseType inAppPurchaseType) {
		storePanel.IapSet(inAppPurchaseType);
	}
	#endregion

	public void ShowAdsPopup() {
		if (!AdManager.IsRewardedVideoAdsLoaded()) {
			//noButtonText.text = "Ok";
		} else {
			//noButtonText.text = "No";
		}
		//watchVideoButtonObj.gameObject.SetActive(AdManager.IsRewardedVideoAdsLoaded());
	}
}