﻿using UnityEngine;
using DG.Tweening;

public class UIButtonScaleAnimation : MonoBehaviour {

	[SerializeField] private RectTransform rect;
	private Vector3 originalScale = Vector3.one;
	private Tween tween;

	void Reset() {
		rect = gameObject.GetComponent<RectTransform>();
	}

	void OnEnable() {
		originalScale = rect.localScale;
		ButtonAnimation();
	}

	void OnDisable() {
		tween.Kill();
	}

	void ButtonAnimation() {
		if (rect != null) {
			tween = rect.DOScale(originalScale * 0.9f, Random.Range( 2.0f, 3.0f))
				.SetEase(Ease.InOutQuad)
				.SetUpdate(true)
				.SetLoops(-1, LoopType.Yoyo);
		}
	}
}
