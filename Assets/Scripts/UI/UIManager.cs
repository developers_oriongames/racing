﻿using UnityEngine;
using GameServices;
using DG.Tweening;
using UnityEngine.UI;

public enum UI {
	Menu = 0,
	Game = 1,
	Tutorial = 2,
}

public enum GameScreen {
	GameLoading = 0,
	CarSelectScreen = 1,
	LevelSelectScreen = 2,
	PreviewScreen = 3,
	GameStartScreen = 4,
	GamePlayScreen = 5,
	GameOverScreen = 6,
	PauseScreen = 7,
	GameCompleteScreen = 8,
	EliminationScreen = 9,
	NotEnoughScreen = 10,
	GameModeScreen = 11,
	SettingScreen = 12,
	ExitScreen = 13,
	StoreScreen = 14,
	TutorialScreen = 15,
	HelpScreen = 16,
    RateScreen = 17,
	MegaOfferScreen = 18,
};

public enum AnimationPlacement {
	Left = -1,
	Middle = 0,
	Right = 1,
}

public class UIManager : MonoBehaviour {

	public static UIManager Instance;
	public static UI CurrentUI = UI.Menu;
	public static GameScreen CurrentScreen = GameScreen.CarSelectScreen;
	public static GameScreen PreviousScreen = GameScreen.GameLoading;
	public static GameScreen PrePreviousScreen = GameScreen.GameLoading;
	static float xPos;

	[Header("Asset Ref")]
	public Sprite starSprite;
	
	[Header("Map Sprites")]
	[SerializeField] private Sprite map0Sprite;
	[SerializeField] private Sprite map1Sprite;
	[SerializeField] private Sprite map2Sprite;
	[SerializeField] private Sprite map3Sprite;
	[SerializeField] private Sprite map4Sprite;

	[Header("Game Ref")]
	public UIMenu uIMenu;
	public UIGame uIGame;
	public UITutorial uITutorial;
	[Space]
	public Canvas menuCanvas;
	public Canvas gameCanvas;
	public Canvas tutorialCanvas;

	void Awake() {
		if(Instance == null) {
			Instance = this;
		}
		uIMenu.gameObject.SetActive(false);
		uIGame.gameObject.SetActive(false);
		uITutorial.gameObject.SetActive(false);
	}

	void Start() {
		if (Navigation.IsInMenuScene()) {
			uIMenu.gameObject.SetActive(true);
			menuCanvas.gameObject.SetActive(true);
			uIGame.gameObject.SetActive(false);
			uITutorial.gameObject.SetActive(false);
			CurrentUI = UI.Menu;
			CurrentScreen = GameScreen.CarSelectScreen;
			AudioManager.PlayMenuMusic();
		} else if (Navigation.IsInGameScene()) {
			uIMenu.gameObject.SetActive(false);
			uIGame.gameObject.SetActive(true);
			gameCanvas.gameObject.SetActive(true);
			uITutorial.gameObject.SetActive(false);
			CurrentUI = UI.Game;
			CurrentScreen = GameScreen.GameStartScreen;
			GameEvents.OnGameStartScreen();
			AudioManager.PlayGamePlayMusic();
		} else if (Navigation.IsInTutorialScene()) {
			uIMenu.gameObject.SetActive(false);
			uIGame.gameObject.SetActive(true);
			gameCanvas.gameObject.SetActive(true);
			uITutorial.gameObject.SetActive(true);
			tutorialCanvas.gameObject.SetActive(true);
			CurrentUI = UI.Tutorial;
			CurrentScreen = GameScreen.GameStartScreen;
			GameEvents.OnGameStartScreen();
			AudioManager.PlayGamePlayMusic();
		}
	}

    //void Update() {
    //    Debug.LogError("CurrentScreen >>>>>>> " + CurrentScreen);
    //    Debug.LogError("PreviousScreen >>>>>>> " + PreviousScreen);
    //}

    public static void UIAnimation(Transform _transform, AnimationPlacement animationPlacement, float animationTime = 0.2f, float delay = 0) {
		switch (animationPlacement) {
			case AnimationPlacement.Left:
				xPos = -1920;
				break;
			case AnimationPlacement.Middle:
				//rectTransform.gameObject.SetActive(true);  //auto active
				xPos = 0;
				break;
			case AnimationPlacement.Right:
				xPos = 1920;
				break;
		}
		_transform.GetComponent<RectTransform>()
			.DOAnchorPos3DX(xPos, animationTime)
			.SetDelay(delay)
			.SetUpdate(UpdateType.Normal, true)
			.OnComplete(() => {
				//rectTransform.gameObject.SetActive(animationPlacement == AnimationPlacement.Middle);  //auto deactive
			});
	}

	public static Sprite GetMapSprite() {
		switch (TrackManager.GetCurrentMapNumber()) {
			case 0:
			return Instance.map0Sprite;
			case 1:
				return Instance.map1Sprite;
			case 2:
				return Instance.map2Sprite;
			case 3:
				return Instance.map3Sprite;
			case 4:
				return Instance.map4Sprite;
			default: 
				return null;
		}
	}

	public static void BackNavigation() {
		switch (CurrentScreen) {
			//************** Menu *************//
			case GameScreen.CarSelectScreen:
				//show exit pop-up
				Instance.uIMenu.ShowExitPopup();
				break;
			case GameScreen.LevelSelectScreen:
				//close game level
				Instance.uIMenu.CloseLevel();
				break;
			case GameScreen.GameModeScreen:
				//close game mode
				Instance.uIMenu.CloseGameMode();
				break;
			case GameScreen.StoreScreen:
				//close store
				Instance.uIMenu.CloseStore();
				break;
			case GameScreen.SettingScreen:
				//close settings pop-up
				Instance.uIMenu.CloseSetting();
				break;
			case GameScreen.PreviewScreen:
				//close car preview
				Instance.uIMenu.CloseCarPreview();
				break;
			case GameScreen.ExitScreen:
				//close exit pop-up
				Instance.uIMenu.ExitNo();
				break;
			case GameScreen.RateScreen:
				//close rate popup
				Instance.uIMenu.CloseRate();
				break;
			case GameScreen.MegaOfferScreen:
				Instance.uIMenu.CloseMegaOffer();
				break;
			//************** Game Play *************//
			case GameScreen.GamePlayScreen:
				// Pauses the game
				GameManager.Pause();
				break;
			case GameScreen.PauseScreen:
				//resume game
				GameManager.Resume();
				break;
			case GameScreen.GameStartScreen:
				break;
			case GameScreen.GameOverScreen:
				break;
			case GameScreen.GameCompleteScreen:
				break;
			case GameScreen.EliminationScreen:
				break;
			case GameScreen.GameLoading:
				break;
			case GameScreen.TutorialScreen:
				break;
		}
	}
}
