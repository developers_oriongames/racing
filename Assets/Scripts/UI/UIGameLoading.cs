﻿using GameServices;
using UnityEngine;
using UnityEngine.UI;

public class UIGameLoading : MonoBehaviour {

	[Header("Assets")]
	[SerializeField] private Sprite welcomeScreenLoadingSprite;
	[SerializeField] private Sprite gameScreenLoadingSprite;

	[Header("References")]
	[SerializeField] private Image splashLoadingImage;
	[SerializeField] private Image inGameLoadingImage;
	//[SerializeField] private Image blurImage;
	[SerializeField] private RectTransform remoteControlRect;
	[SerializeField] private RectTransform loadingCircleRect;
	[SerializeField] private Image loagingBarImage;
	[SerializeField] private Image loadingFillBarImage;
	[SerializeField] private Transform loadingTransform0;
	[SerializeField] private Transform loadingTransform1;
	[SerializeField] private Transform loadingTransform2;
	[SerializeField] private RectTransform dotBigRect;

	public int progress = 0;
	readonly float[] x = { -40f, -20f, 0f, 20f, 40f };
	int cnt = 0, cnt1 = -0;

    void Awake() {
		loadingCircleRect.gameObject.SetActive(false);
		//blurImage.gameObject.SetActive(false);
		remoteControlRect.gameObject.SetActive(false);
	}

    public void Loading() {
		if(Navigation. GetCurrentScene() != Scenes.Loading) {
		loadingCircleRect.gameObject.SetActive(true);
		}
		splashLoadingImage.gameObject.SetActive(false);
		inGameLoadingImage.gameObject.SetActive(false);
		if (Navigation.IsInGameLoadScene()) {
			//loadingBgImage.sprite = welcomeScreenLoadingSprite;
			splashLoadingImage.gameObject.SetActive(true);
		} else {
			//loadingBgImage.sprite = gameScreenLoadingSprite;
			inGameLoadingImage.gameObject.SetActive(true);
			if(InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
				//blurImage.gameObject.SetActive(true);
				remoteControlRect.gameObject.SetActive(true);
            }
		}
	}

	void Update() {
		loadingFillBarImage.fillAmount = progress / 100f;
		loadingTransform0.Rotate(Vector3.forward * (Time.deltaTime * 100));
		loadingTransform1.Rotate(Vector3.forward * (Time.deltaTime * 50));
		loadingTransform2.Rotate(Vector3.back * (Time.deltaTime * 100));
		dotBigRect.anchoredPosition = new Vector2(x[cnt], -15f);
		cnt1++;
		if (cnt1 % 6 == 0) {
			cnt++;
		}
		cnt %= x.Length;
	}

	public void EnableLoading() {
		progress = 7;
		loadingFillBarImage.fillAmount = 0.05f;
		loagingBarImage.enabled = true;
	}
}