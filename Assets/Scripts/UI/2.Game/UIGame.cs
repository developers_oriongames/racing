﻿using UnityEngine;
using GameServices;

public class UIGame : MonoBehaviour {

	[Header("Ref")]
	public UIGamePlayPanel gamePlayPanel;
	public UIPausePanel pausePanel;
	public UIGameCompletePanel gameCompletePanel;
	public UIGameOverPanel gameOverPanel;


	void Start() {
		gamePlayPanel.ActiveDeactive(true);
		pausePanel.ActiveDeactive(false);
		gameOverPanel.ActiveDeactive(false);
		gameCompletePanel.ActiveDeactive(false);

		pausePanel.DoAnimation(AnimationPlacement.Left);
		gameOverPanel.DoAnimation(AnimationPlacement.Right);
	}

	void Update() {
		gameCompletePanel.UpdateWork();
		gamePlayPanel.UpdateWork();
	}

	public void GameOverWork() {
		gameCompletePanel.ActiveDeactive(true);
		//gameOverPanel.ActiveDeactive(true);
	}
	public void GameOverElimination() {
		GameEvents.OnGameCompleteScreen();
		gameOverPanel.ActiveDeactive(true);
		GameController.Instance.SetGameOver();
		gameOverPanel.DoAnimation(AnimationPlacement.Middle);
	}

	public void PauseAnimation() {
		//print("fffffffffffffffffffffff");
		pausePanel.ActiveDeactive(true);
		pausePanel.DoAnimation(AnimationPlacement.Middle);
	}


	#region Game Buttons
	public void Restart() {
		pausePanel.DoAnimation(AnimationPlacement.Left);
		pausePanel.ActiveDeactive(false);
		Navigation.LoadScene(Navigation.GetCurrentScene(), GameScreen.GameStartScreen);
	}
	public void Resume() {
		pausePanel.DoAnimation(AnimationPlacement.Left);
		pausePanel.ActiveDeactive(false);
		UIManager.CurrentScreen = GameScreen.GamePlayScreen;
		//GameEvents.OnGamePlayScreen();
	}

	public void QuitRace() {
		GameManager.isInPause = false;
		GameManager.isInGamePlay = false;
		AudioManager.StopMusic();
		Navigation.LoadScene(Scenes.Menu, GameScreen.CarSelectScreen);
		Time.timeScale = 1;
	}
	public void Retry() {
		Navigation.LoadScene(Navigation.GetCurrentScene(), GameScreen.GameStartScreen);
	}
	public void CompleteRace() {
		if (GameManager.isInTutorial) {
			Debug.Log("Complete the tutorial: GameManager.isInTutorial: " + GameManager.isInTutorial);
			TutorialManager.CompleteTutorial();
		} else {
			Debug.Log("Complete the race:  GameManager.isInTutorial: " + GameManager.isInTutorial);
			gameOverPanel.ActiveDeactive(true);
			GameController.Instance.SetGameOver();
			gameOverPanel.DoAnimation(AnimationPlacement.Middle);
			UIManager.CurrentScreen = GameScreen.GameOverScreen;
			GameEvents.OnGameOverScreen();
		}
	}
	public void NextFromGameOver() {
		gameOverPanel.DoAnimation(AnimationPlacement.Left);
		Navigation.LoadScene(Scenes.Menu, GameScreen.GameLoading);
	}
	#endregion
}