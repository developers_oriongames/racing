﻿using GameServices;
using UnityEngine;

public class UIGameCompletePanel : MonoBehaviour
{
	[Header("Game Complete")]
	[SerializeField] private Transform _transform;

	public void UpdateWork() {
		_transform.gameObject.SetActive(
			UIManager.CurrentScreen == GameScreen.GameCompleteScreen 
			&& !GameManager.isInTutorial);
	}

	public void ActiveDeactive(bool status) {
		_transform.gameObject.SetActive(status);
		if (status) {
			AdManager.ShowPromoAd(PromoSpot.SPOT_LEVEL_COMPLETE);
		} else {
			AdManager.HidePromoAd();
		}
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}
}
