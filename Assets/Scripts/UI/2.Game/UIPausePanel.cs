﻿using UnityEngine;
using GameServices;

public class UIPausePanel : MonoBehaviour
{
	[Header("Game Pause")]
	[SerializeField] private Transform _transform;

	public void ActiveDeactive(bool status) {
		_transform.gameObject.SetActive(status);
		if (status) {
			AdManager.ShowPromoAd(PromoSpot.SPOT_LEVEL_PAUSE);
		} else {
			AdManager.HidePromoAd();
		}
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}


}
