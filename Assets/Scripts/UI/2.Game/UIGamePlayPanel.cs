﻿using GameServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using MEC;

public class UIGamePlayPanel : MonoBehaviour {

	[Header("Game Play")]
	[SerializeField] private Transform _transform;
	[SerializeField] private TMP_Text worngDirectionText;
	[SerializeField] private Image playerRankImage; 
	[SerializeField] private TMP_Text playerRankText;
	[SerializeField] private Image currentSpeedImage; 
	[SerializeField] private TMP_Text currentSpeedText; 
	[SerializeField] private Image lapImage;
	[SerializeField] private TMP_Text lapText;
	[SerializeField] private Image timeImage;
	[SerializeField] private TMP_Text timeText;
	[SerializeField] private Image boostMeterBgImage;
	[SerializeField] private Image boostMeterImage;
	[SerializeField] private Image eliminationImage;
	[SerializeField] private TMP_Text eliminationText;
	[SerializeField] private TMP_Text playerOutText; 
	[SerializeField] private TMP_Text perfectText;
	[SerializeField] private Button cameraButton; 
	[SerializeField] private Image boostImage;
	[SerializeField] private Image breakImage; 
	[SerializeField] private Button leftButton;
	[SerializeField] private Button rightButton;
	[SerializeField] private Button pauseButton;
	[SerializeField] private Button resetButton; 
    [SerializeField] private Button rewardAdBoostButton;
    [SerializeField] private Text rewardAdNotAvailableText;
	[SerializeField] private RawImage minimapImage;

    private Vector2 boostImageDefaultPosition = new Vector2(710, -340);
	private Vector2 breakImageDefaultPosition = new Vector2(-710, -340);

    private float fadeAlpha = 0.25f;
	private float fullAlpha = 1.0f;
	private Color tempColor = Color.white;


	void OnEnable() {
		GameEvents.OnBreakFeatureShowed += ShowBreakInTutorial;
		GameEvents.OnBoostFeatureShowed += ShowBootInTutorial;
    }

    void OnDisable() {
		GameEvents.OnBreakFeatureShowed -= ShowBreakInTutorial;
		GameEvents.OnBoostFeatureShowed -= ShowBootInTutorial;
	}

    void Start()
    {
        switch (InputManager.GetCurrentPlatform())
        {
            case BuildPlatforms.Android:
            case BuildPlatforms.IOS:
            case BuildPlatforms.AmazonTab:
                boostImage.DOFade(fadeAlpha, 0.2f);
                breakImage.DOFade(fadeAlpha, 0.2f);
                if (GameManager.isInTutorial)
                {
                    boostImage.gameObject.SetActive(false);
                    breakImage.gameObject.SetActive(false);
                    RewardAdBoostEnable(false);
                }

                break;
            case BuildPlatforms.AmazonTv:
                boostImage.gameObject.SetActive(false);
                breakImage.gameObject.SetActive(false);
                RewardAdBoostEnable(false);
                break;
        }

        playerOutText.DOFade(0, 0.1f);
        perfectText.gameObject.SetActive(false);
        pauseButton.gameObject.SetActive(!GameManager.isInTutorial);
        cameraButton.gameObject.SetActive(InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv &&
                                          !GameManager.isInTutorial);
        boostMeterBgImage.gameObject.SetActive(!GameManager.isInTutorial);
        leftButton.gameObject.SetActive(false);
        rightButton.gameObject.SetActive(false);
        worngDirectionText.gameObject.SetActive(false);
        resetButton.gameObject.SetActive(!GameManager.isInTutorial);

        RewardAdBoostEnable(false);
        SetResetButtonInteractive(false);

		minimapImage.gameObject.SetActive(!GameManager.isInTutorial);

	}

	public void RewardAdBoostEnable(bool enable)
	{
		rewardAdBoostButton.gameObject.SetActive(enable);
	}

	public void RewardAdNotAvailableTextEnable(bool enable)
	{
		rewardAdNotAvailableText.gameObject.SetActive(enable);
	}

	public bool RewardAdNotAvailableTextIsActive()
	{
		return rewardAdNotAvailableText.gameObject.activeInHierarchy;
	}

	public void RewardAdBoostButtonPressed()
	{
		GameManager.Pause();
		AdManager.instance.rewardAdState = AdManager.RewardAdState.IN_GAME_BOOST;
		AdManager.instance.ShowRewardedVideo();
	}


	public void UpdateWork() {
		ActiveDeactive(UIManager.CurrentScreen == GameScreen.GamePlayScreen);
		eliminationImage.gameObject.SetActive(
			GameModeManager.GetCurrentGameMode
			== GameModes.Elimination && !GameManager.isInTutorial);
		playerOutText.gameObject.SetActive(
			GameModeManager.GetCurrentGameMode
			== GameModes.Elimination && !GameManager.isInTutorial);

		bool isToActive = InputManager.GetCurrentController() == ControllerTypes.Touch;
		if (GameManager.isInGamePlay) {
            leftButton.gameObject.SetActive(isToActive);
			rightButton.gameObject.SetActive(isToActive);
		}
		if (GameManager.isInTutorial) {
			if (TutorialManager.isControlerSelected) {
				leftButton.gameObject.SetActive(isToActive);
				rightButton.gameObject.SetActive(isToActive);
			}
			if (TutorialManager.isBoostFeatureShowed) {
				boostMeterBgImage.gameObject.SetActive(GameManager.isInTutorial);
			}
		}

		bool activeCondition =
			(UIManager.CurrentScreen == GameScreen.GamePlayScreen)
			&& !GameManager.isInTutorial;
		playerRankImage.gameObject.SetActive(activeCondition);
		currentSpeedImage.gameObject.SetActive(activeCondition);
		lapImage.gameObject.SetActive(activeCondition);
		timeImage.gameObject.SetActive(activeCondition);
	}

	public void ActiveDeactive(bool status) {
		_transform.gameObject.SetActive(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}

	public void EnableBreakImage() {
		tempColor.a = fullAlpha;
		breakImage.color = tempColor;
		breakImage.gameObject.transform.position = Input.mousePosition;
	}

	public void EnableBoostImage() {
		tempColor.a = fullAlpha;
		boostImage.color = tempColor;
		boostImage.gameObject.transform.position = Input.mousePosition;
	}

	public void DisableBreakAndBoostImages() {
        boostImage.DOFade(fadeAlpha, 0.2f);
		breakImage.DOFade(fadeAlpha, 0.2f);
		breakImage.GetComponent<RectTransform>().DOAnchorPos(breakImageDefaultPosition, 0.2f);
		boostImage.GetComponent<RectTransform>().DOAnchorPos(boostImageDefaultPosition, 0.2f);
	}

	public void UpdateBoostMeter(float boostVal) {
		boostMeterImage.fillAmount = boostVal;
	}

	public void UpdateGamePlayTexts(string playerRankText, string currentSpeedText, string lapText, string timeText) {
		this.playerRankText.text = playerRankText;      //Pos
		this.currentSpeedText.text = currentSpeedText;  //Distance
		this.lapText.text = lapText;                    //Lap
		this.timeText.text = timeText;                  //Time
	}

	public void EnableDisableEliminationImages() {
		lapImage.gameObject.SetActive(GameModeManager.GetCurrentGameMode != GameModes.Elimination);
		timeImage.gameObject.SetActive(GameModeManager.GetCurrentGameMode != GameModes.Elimination);
	}

	public void EnablePerfectText(bool isToEnable) {
		perfectText.gameObject.SetActive(isToEnable && !GameManager.isInTutorial);
	}

	public void UpdateWrongDirectionText(string text, bool isToEnable) {
		worngDirectionText.text = text;
		worngDirectionText.gameObject.SetActive(isToEnable && !GameManager.isInTutorial);
    }
	public void SetEliminationText(string text) {
		eliminationText.text = text;
	}

	public void SetPlayerOutText(string text) {
		playerOutText.text = text;
	}

	public bool IsPlayerOutTextActive() {
		return playerOutText.enabled;
	}

	public void SetPlayerOutTextStatus(bool isToActive) {
		playerOutText.gameObject.SetActive(isToActive);
	}

	public bool IsResetButtonInteractive()
    {
		return resetButton.IsInteractable();
	}

    public void SetResetButtonInteractive(bool status = true)
    {
        resetButton.interactable = status;

        if (!status)
        {
			Invoke(nameof(AutoEnableResetButton),5);
        }
        else
        {
           CancelInvoke(nameof(AutoEnableResetButton));
        }
    }

    private void AutoEnableResetButton()
    {
        SetResetButtonInteractive();
    }

    bool busy;
	public void SetCrossFadeAlpha(float alpha, float duration) {
		if(busy) { return; }
		busy = true;
        playerOutText.DOFade(alpha, duration).SetUpdate(UpdateType.Normal).OnComplete(() => {
			busy = false;
			Timing.RunCoroutine(_FadeOut());
		});
	}

	IEnumerator<float> _FadeOut() {
		yield return Timing.WaitForSeconds(1f);
		playerOutText.DOFade(0, 0.2f);
	}

	private void ShowBreakInTutorial(bool isBreakFeatureShowed) {
		if (GameManager.isInTutorial && isBreakFeatureShowed && !InputManager.IsInAmazonTv()) {
			breakImage.gameObject.SetActive(true);
		}
	}

	private void ShowBootInTutorial(bool isBoostFeatureShowed) {
		if (GameManager.isInTutorial && isBoostFeatureShowed && !InputManager.IsInAmazonTv()) {
			boostImage.gameObject.SetActive(true);
		}
	}
}