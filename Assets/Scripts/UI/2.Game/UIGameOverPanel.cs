﻿using GameServices;
using UnityEngine;
using UnityEngine.UI;

public class UIGameOverPanel : MonoBehaviour {

	[Header("Game Over")]
	[SerializeField] private Transform _transform;
	[SerializeField] private RectTransform gameOverTotalCashRect;

	[SerializeField] Text[] posiotionTexts;
	[SerializeField] Text[] nameTexts;
	[SerializeField] Text[] carTexts;
	[SerializeField] Text[] timeTexts;

	[SerializeField] Image[] starImages;
	[SerializeField] Text finalPositionText;
	[SerializeField] Text cashEarningText;

    void Start() {
		gameOverTotalCashRect.gameObject.SetActive(GameManager.IsInGameCurrencyActive);	
		for (int i = 0; i < 5; i++) {
			posiotionTexts[i].GetComponent<Text>().text = "-";
			nameTexts[i].GetComponent<Text>().text = "-";
			carTexts[i].GetComponent<Text>().text = "-";
			timeTexts[i].GetComponent<Text>().text = "-";
		}
	}

	public void ActiveDeactive(bool status) {
		_transform.gameObject.SetActive(status);
	}

	public void DoAnimation(AnimationPlacement animationPlacement) {
		UIManager.UIAnimation(_transform, animationPlacement);
	}

	public void Set() {
		for (int i = 0; i < starImages.Length; i++) {
			starImages[i].gameObject.SetActive(false);
		}
	}

	float delay = 0;
	void Update() {
		if (UIManager.CurrentScreen != GameScreen.GameOverScreen) {
			return;
		}
		finalPositionText.text = GameController.Instance.carUserControl.mPlrRank.ToString();
		for (int i = 0; i < GameController.noStar; i++) {
			starImages[i].gameObject.SetActive(true);
		}
		cashEarningText.text = GameController.tmpmoney.ToString();

		for (int i = 0; i < GameController.totalPlayer; i++) {
			if (GameController.Instance.carUserControl.mPlrRank == GameController.mRankArray[i]) {
				nameTexts[i].text = "You";
				carTexts[i].text = CarManager.GetSelectedCar().GetCarName();  //GameShop.CarName[GameShop.mCarSelected];
			} else {
				if (i == 4 || (GameModeManager.GetCurrentGameMode == GameModes.Versus && i == 1)) {
					nameTexts[i].text = GameController.Instance.Opponent.GetComponent<SetOpp>().Name[GameController.Instance.carUserControl.mPlrRank - 1];
				} else {
					nameTexts[i].text = GameController.Instance.Opponent.transform.GetChild(i % GameController.Instance.Opponent.transform.childCount).GetComponent<CarAIControl>().playerNameAI;
				}
				carTexts[i].text = GameController.Instance.Opponent.transform.GetChild(i % GameController.Instance.Opponent.transform.childCount).transform.name;
			}
			posiotionTexts[i].text = GameController.mRankArray[i].ToString();
			timeTexts[i].text = GameController.FormatTime(GameController.mTimeArray[i]);
		}
		if (delay > 0.5f) {
			if (GameController.tmpmoney < CurrencyManager.TotalCash) {
				GameController.tmpmoney += CurrencyManager.TotalCash / 50;
			} else {
				GameController.tmpmoney = CurrencyManager.TotalCash;
			}
		}
		if (delay <= 0.5f) {
			delay += Time.deltaTime;
		}
	}
}
