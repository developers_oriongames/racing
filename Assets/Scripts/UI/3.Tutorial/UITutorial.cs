﻿using GameServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITutorial : MonoBehaviour {

	[SerializeField] private TMP_Text[] continueButtonTexts;

	[Header("Welcome Panel")]
	[SerializeField] private RectTransform welcomePanelRectTransform;
	[Header("Control Selection Panel")]
	[SerializeField] private RectTransform controlerSelectionRectTransform;
	[SerializeField] private RectTransform mobileControlRectTransform;
	[SerializeField] private RectTransform tvControlRectTransform;
	[Header("Tutorial GamePlay Panel")]
	[SerializeField] private RectTransform tutorialGamePlayRectTransform;
	[Header("Controler Selecton")]
	[SerializeField] private RectTransform sensorPanelRectTransform;
	[SerializeField] private RectTransform touchPanelRectTransform;
	[SerializeField] private RectTransform remotePanelRectTransform;
	[Header("Brake")]
	[SerializeField] private RectTransform brakePanelRectTransform;
	[SerializeField] private RectTransform mobileBrakePanelRectTransform;
	[SerializeField] private RectTransform tvBrakePanelRectTransform;
	[SerializeField] private Image breakImage;
	[Header("Boost")]
	[SerializeField] private RectTransform boostPanelRectTransform;
	[SerializeField] private RectTransform mobileBoostPanelRectTransform;
	[SerializeField] private RectTransform tvBoostPanelRectTransform;
	[SerializeField] private Image boostMeterBgImage;
	[SerializeField] private Image boostMeterImage;
	[SerializeField] private Image boostImage;
	[Header("Tutorial Complete Panel")]
	[SerializeField] private RectTransform tutorialCompletePanelRectTransform;

	void Awake() {
		welcomePanelRectTransform.gameObject.SetActive(true);

		controlerSelectionRectTransform.gameObject.SetActive(false);
		mobileControlRectTransform.gameObject.SetActive(false);
		tvControlRectTransform.gameObject.SetActive(false);

		tutorialGamePlayRectTransform.gameObject.SetActive(false);
		sensorPanelRectTransform.gameObject.SetActive(false);
		touchPanelRectTransform.gameObject.SetActive(false);

		brakePanelRectTransform.gameObject.SetActive(false);
		mobileBrakePanelRectTransform.gameObject.SetActive(false);
		tvBrakePanelRectTransform.gameObject.SetActive(false);

		boostPanelRectTransform.gameObject.SetActive(false);
		mobileBoostPanelRectTransform.gameObject.SetActive(false);
		tvBoostPanelRectTransform.gameObject.SetActive(false);

		tutorialCompletePanelRectTransform.gameObject.SetActive(false);
	}

    void Start() {
		foreach (TMP_Text continueButtonText in continueButtonTexts) {
            switch (InputManager.GetCurrentPlatform()) {
                case BuildPlatforms.AmazonTab:
				case BuildPlatforms.Android:
				case BuildPlatforms.IOS:
					continueButtonText.text = Constants.Tutorial_Continue_Mobile;
					break;
                case BuildPlatforms.AmazonTv:
					continueButtonText.text = Constants.Tutorial_Continue_TV;
					break;
            }        
		}
	}

    void Update() {
		tutorialCompletePanelRectTransform.gameObject.SetActive(UIManager.CurrentScreen == GameScreen.GameCompleteScreen);
	}

	public void ShowWelcomePanel() {
		Debug.Log("Tutorial Welcome Showed!");
		welcomePanelRectTransform.gameObject.SetActive(true);
	}

	public void UpdateBoostMeter(float boostVal) {
		boostMeterImage.fillAmount = boostVal;
	}

	public void ContinueFromWelcome() {
		welcomePanelRectTransform.gameObject.SetActive(false);
		GameAnimationManager.PlayAnimation();
	}
	public void ShowControlerSelectionPanel() {
		controlerSelectionRectTransform.gameObject.SetActive(true);
        switch (InputManager.GetCurrentPlatform()) {
            case BuildPlatforms.AmazonTab:
            case BuildPlatforms.Android:
            case BuildPlatforms.IOS:
				mobileControlRectTransform.gameObject.SetActive(true);
                break;
			case BuildPlatforms.AmazonTv:
				tvControlRectTransform.gameObject.SetActive(true);
				break;
		}
    }
    public void HideControlerSelectionPanel() {
		controlerSelectionRectTransform.gameObject.SetActive(false);
	}
	public void ShowSensorControlerPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(true);
		sensorPanelRectTransform.gameObject.SetActive(true);
	}
	public void HideSensorControlerPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(false);
		sensorPanelRectTransform.gameObject.SetActive(false);
	}
	public void ShowTouchControlerPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(true);
		touchPanelRectTransform.gameObject.SetActive(true);
	}
	public void HideTouchControlerPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(false);
		touchPanelRectTransform.gameObject.SetActive(false);
	}
	public void ShowRemoteControlerPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(true);
		remotePanelRectTransform.gameObject.SetActive(true);
	}
	public void HideRemoteControlerPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(false);
		remotePanelRectTransform.gameObject.SetActive(false);
	}
	public void ShowBrakePanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(true);
		brakePanelRectTransform.gameObject.SetActive(true);
		switch (InputManager.GetCurrentPlatform()) {
			case BuildPlatforms.AmazonTab:
			case BuildPlatforms.Android:
			case BuildPlatforms.IOS:
				mobileBrakePanelRectTransform.gameObject.SetActive(true);
				break;
			case BuildPlatforms.AmazonTv:
				tvBrakePanelRectTransform.gameObject.SetActive(true);
				break;
		}
	}
	public void HideBrakePanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(false);
		brakePanelRectTransform.gameObject.SetActive(false);
	}
	public void ShowBoostPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(true);
		boostPanelRectTransform.gameObject.SetActive(true);
		switch (InputManager.GetCurrentPlatform()) {
			case BuildPlatforms.AmazonTab:
			case BuildPlatforms.Android:
			case BuildPlatforms.IOS:
				mobileBoostPanelRectTransform.gameObject.SetActive(true);
				break;
			case BuildPlatforms.AmazonTv:
				tvBoostPanelRectTransform.gameObject.SetActive(true);
				break;
		}
	}
	public void HideBoostPanel() {
		tutorialGamePlayRectTransform.gameObject.SetActive(false);
		boostPanelRectTransform.gameObject.SetActive(false);
	}
	public void ShowTutorialCompletePanel() {
		tutorialCompletePanelRectTransform.gameObject.SetActive(true);
	}
	public void HideTutorialCompletePanel() {
		tutorialCompletePanelRectTransform.gameObject.SetActive(false);
	}
}
