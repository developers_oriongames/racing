﻿using UnityEngine;
using UnityEngine.UI;
using GameServices;
using GameServices.UserData;

public class UIButtons : MonoBehaviour {

    #region Common
    public void PlayButtonSound() {
		AudioManager.PlayButtonSound();
	}
	public void CommonBack() {
		//UIManager.Instance.uIMenu.CommonBack();
		UIManager.BackNavigation();
	}
	#endregion

	#region Car Selection
	public void CarSelection(int direction) {
		GameShop.CarSelection(direction);
	}
	public void CarPreview() {
		UIManager.Instance.uIMenu.OpenCarPreview();
	}
	public void NextFromShop() {
		UIManager.Instance.uIMenu.OpenLevel();
	}
	#endregion

	#region Level
	public void Level(int levelNo) {
		GameManager.SelectLevel(levelNo);
	}
	public void SelectLevelSet(int selectedSet) {
		UIManager.Instance.uIMenu.SelectLevelSet((GameModes)selectedSet);
	}
	#endregion

	#region Try_car
	public void TryThisCarWithRewardAd() {
		int currentSelectedCar = UserDataManager.GetLastSelectedCar();
		AdManager.instance.rewardAdState = AdManager.RewardAdState.TRY_THIS_CAR;
		AdManager.instance.ShowRewardedVideo();
	}

    #endregion

    #region GameMode
    public void LoadGamePlay() {
		GameManager.LoadGamePlay();
	}
	#endregion

	#region Store
	public void OpenIAP() {
		UIManager.Instance.uIMenu.OpenStore(Constants.BUY_SOME_CASH);
	}
	public void CloseIAP() {
		UIManager.Instance.uIMenu.CloseStore();
	}
	public void SelectIapSet(int selectedSet) {
		UIManager.Instance.uIMenu.SelectIapSet((InAppPurchaseType) selectedSet);
	}
	public void BuyCash(int amount) {
		//PurchaseManager.BuyCash(amount);
	}
	public void BuyRemoveAds() {
		//PurchaseManager.BuyRemoveAds();
		PurchaseManager.BuyNonConsumableItem(NonConsumableIAPItems.REMOVE_ADS);
	}
	public void BuyUnlockFullGame() {
		//PurchaseManager.BuyUnlockFullGame();
		PurchaseManager.BuyNonConsumableItem(NonConsumableIAPItems.UNLOCK_ALL);
	}
	public void BuyUnlockAllCars() {
		//PurchaseManager.BuyUnlockAllCars();
		PurchaseManager.BuyNonConsumableItem(NonConsumableIAPItems.UNLOCK_CARS);
	}
	public void BuyUnlockAllLevels() {
		//PurchaseManager.BuyUnlockAllLevels();
		PurchaseManager.BuyNonConsumableItem(NonConsumableIAPItems.UNLOCK_LEVELS);
	}
	#endregion

	#region Settings
	public void OpenSetting() {
		UIManager.Instance.uIMenu.OpenSetting();
	}
	public void CloseSetting() {
		UIManager.Instance.uIMenu.CloseSetting();
	}
	public void SensorControll() {
		InputManager.SetCurrentController(ControllerTypes.Sensor);
	}
	public void TouchControll() {
		InputManager.SetCurrentController(ControllerTypes.Touch);
	}
	public void UpdateMusicVolume(Slider slider) {
		AudioManager.UpdateMusicVolume(slider.value);
	}
	public void UpdateSoundVolume(Slider slider) {
		AudioManager.UpdateSoundVolume(slider.value);
	}
	public void PlayTutorial() {
		GameManager.AgainPlayTutorial();
	}
	public void Like() {
		Application.OpenURL("");

	}
    #endregion

    #region Help
    public void OpenHelp() {
		UIManager.Instance.uIMenu.OpenHelp();
    }
	public void CloseHelp() {
		UIManager.Instance.uIMenu.CloseHelp();
    }
	#endregion

	#region Mega Offer
	public void OpenMegaOffer()
	{
		UIManager.Instance.uIMenu.OpenMegaOffer();
	}
	public void CloseMegaOffer()
	{
		UIManager.Instance.uIMenu.CloseMegaOffer();
	}
	#endregion

	#region Rate
	public void AssignStar(int spot) {
		UIManager.Instance.uIMenu.ratePanel.AssignStarSprite(spot);
    }
	public void OpenRate() {
		UIManager.Instance.uIMenu.OpenRate();
	}
	public void CloseRate() {
		UIManager.Instance.uIMenu.CloseRate();
	}
	public void RateApp() {
		RateManager.RateApp();
	}
	#endregion

	#region Game Play
	public void Steer(float value) {
		CarUserControl.steerDirection = value;
	}
	public void PauseGame() {
		GameManager.Pause();
	}
	public void Restart() {
		GameManager.Restart();
	}
	public void Resume() {
		GameManager.Resume();
	}
	public void QuitRace() {
		UIManager.Instance.uIGame.QuitRace();
	}
	public void CompleteRace() {
		UIManager.Instance.uIGame.CompleteRace();
	}
	public void Retry() {
		UIManager.Instance.uIGame.Retry();
	}
	public void NextFromGameOver() {
		UIManager.Instance.uIGame.NextFromGameOver();
	}
	public void ChangeCameraAngle() {
		GameCamera.Instance.ChangeCameraView();
	}
    public void ResetCar()
    {
        GameController.Instance.ResetCar();

    }
    #endregion

    #region Tutorial
    public void ContinueFromWelcome() {
		TutorialManager.ContinueFromWelcome();
	}
	public void SelectSensorControler() {
		TutorialManager.SelectSensorControler();
	}
	public void SelectTouchControler() {
		TutorialManager.SelectTouchControler();
	}
	public void SelectRemoteControler() {
		TutorialManager.SelectRemoteControler();
	}
	public void ContinueFromTutorial(int stage) {
		TutorialManager.ContinueFromTutorial(stage);
	}
	#endregion

	#region Other Popups
	public void CloseNotEnoughCash() {
		//UIManager.Instance.uIMenu.CloseNotEnoughCash();
	}
	public void WatchRewardAd() {
		//UIManager.Instance.uIMenu.WatchRewardAd();
	}
	public void ExitYes() {
		UIManager.Instance.uIMenu.ExitYes();
	}
	public void ExitNo() {
		UIManager.Instance.uIMenu.ExitNo();
	}
	#endregion

	#region Testing
	public void ResetAllData() {
		Debug.LogError("Deleted all data!");
		PlayerPrefs.DeleteAll();
		//PlayerPrefs.Save();
		Navigation.LoadScene(Scenes.Loading);
	}
	public void TestingUnlockAll() {
		PurchaseManager.GiveUnlockFullGame();
	}
	#endregion
}
