﻿using UnityEngine;
using DG.Tweening;
using GameServices;

public class UIAmazonTV : MonoBehaviour {

    [SerializeField] private RectTransform selectedImageRect;
    private Vector3 originalScale;
    private Tween scaleTween;

    void OnEnable() {
        originalScale = Vector3.one;    // selectedImageRect.localScale;
        //if(!GameManager.isInTutorial) ScaleAnimation();
    }
    void OnDisable() {
        scaleTween.Kill();
        selectedImageRect.localScale = Vector3.one;//originalScale;
    }

    void Start() {
        gameObject.GetComponent<Canvas>().enabled = (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv);//SelectedUIControlller.ISAmazonTV();
#if UNITY_EDITOR
        gameObject.GetComponent<Canvas>().enabled = true;
#endif
    }

    int i;
    void ScaleAnimation() {
        //print("i: " + ++i);
        scaleTween = selectedImageRect
                       .DOScale(originalScale * 1.05f, 0.25f)
                       //.SetLoops(-1, LoopType.Yoyo)
                       .SetRecyclable()
                       .SetDelay(2.0f)
                       .SetUpdate(UpdateType.Normal, true)
                        .OnComplete(() => {
                            //print("1 > " + i);
                            selectedImageRect
                            .DOScale(originalScale, 0.2f).OnComplete(() => {
                               // print("2 > " + i);
                                ScaleAnimation();
                            });
                        });
    }
}