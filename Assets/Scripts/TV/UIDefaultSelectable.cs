﻿using GameServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIDefaultSelectable : MonoBehaviour {

    public GameObject defaultButtonObject;

    public bool autoSelectIfNull = true;
    public bool assignOriginAsDefault = false; //make the previous buttons as selectable

    void OnEnable() {
        SetSelectable();
    }
    void OnDisable() {
        RemoveSelectable();
    }

    void SetSelectable() {
        if (InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv) { return; }
        if (defaultButtonObject == null && autoSelectIfNull) {
            Button button = GetComponentInChildren<Button>();
            if (button != null) {
                defaultButtonObject = button.gameObject;
            }
        }
        if (assignOriginAsDefault) {
            //// Assign the button that caused it to pop up
            defaultButtonObject = EventSystem.current.currentSelectedGameObject;
        }
        SelectedUIControlller.instance.AddToSelectableStake(this);
    }

    void RemoveSelectable() {
        if (InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv) { return; }
        SelectedUIControlller.instance.RemoveFromSelectableStake(this);
    }
}