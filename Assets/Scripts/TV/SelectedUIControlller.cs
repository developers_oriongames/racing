﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MEC;
using GameServices;

public class SelectedUIControlller : MonoBehaviour {

    public static SelectedUIControlller instance;
    public EventSystem eventSystem;
    public GameObject selectionUI = null;
    private GameObject selectionUIInstance = null;
    //[HideInInspector]
    public List<UIDefaultSelectable> defaultSelectablesStack = new List<UIDefaultSelectable>();
    [SerializeField] private GameObject selectedUI = null;

    public GameObject SelectedUI {
        get { return selectedUI; }
        set {
            selectedUI = value;
            if (selectedUI != null) { AssginSelectionUI(selectedUI.transform); } else { UnassignSelectionUI(); }
        }
    }

    void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
    void Update() {
        if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
            if (eventSystem != EventSystem.current) {
                eventSystem = EventSystem.current;
                UpdatedSelectionToEventSystem(eventSystem.currentSelectedGameObject);
            }
            if (!Navigation.IsInGameLoadScene() && eventSystem.currentSelectedGameObject != selectedUI) {
                SelectedUI = eventSystem.currentSelectedGameObject;
            }
        }
    }

    /// <summary>
    /// Change order of the canvas
    /// </summary>
    /// <param name="sortOrder"></param>
    public void CanvasOrderChanger(int sortOrder) {
        if (selectionUIInstance != null) {
            selectionUIInstance.GetComponent<Canvas>().sortingOrder = sortOrder;
        }
    }
    public void AddToSelectableStake(UIDefaultSelectable defaultSelectable) {
        defaultSelectablesStack.Add(defaultSelectable);
        if (defaultSelectablesStack.Count > 0) {
            UpdatedSelectionToEventSystem(defaultSelectablesStack[defaultSelectablesStack.Count - 1].defaultButtonObject);
        }
    }
    public void RemoveFromSelectableStake(UIDefaultSelectable defaultSelectable) {
        if (defaultSelectablesStack[defaultSelectablesStack.Count - 1] == defaultSelectable) {
            defaultSelectablesStack.Remove(defaultSelectable);
            //Debug.LogWarning("defaultSelectablesStack.Count: " + defaultSelectablesStack.Count);
            if (defaultSelectablesStack.Count > 0) {
                //Debug.LogWarning("defaultSelectablesStack[defaultSelectablesStack.Count - 1]: " + defaultSelectablesStack[defaultSelectablesStack.Count - 1]);
                if (defaultSelectablesStack[defaultSelectablesStack.Count - 1].defaultButtonObject.activeInHierarchy) {// can be problematic
                    UpdatedSelectionToEventSystem(defaultSelectablesStack[defaultSelectablesStack.Count - 1].defaultButtonObject);
                } else {
                    defaultSelectablesStack[defaultSelectablesStack.Count - 1].defaultButtonObject
                        = defaultSelectablesStack[defaultSelectablesStack.Count - 1].gameObject.GetComponentInChildren<Button>().gameObject;
                    UpdatedSelectionToEventSystem(defaultSelectablesStack[defaultSelectablesStack.Count - 1].defaultButtonObject);
                }
            }
        } else {
            defaultSelectablesStack.Remove(defaultSelectable);
        }
    }

    private RectTransform parentRect;
    private RectTransform selectionRect;
    private RectTransform topParent;
    public UISelectable button;
    void AssginSelectionUI(Transform parent = null) {
        if (selectionUIInstance == null) {
            selectionUIInstance = Instantiate(selectionUI);
        } else {
            selectionUIInstance.SetActive(true);
        }
        if (parent != null) {
            if (parent.GetComponent<Scrollbar>() != null) {
                selectionUIInstance.transform.SetParent(parent.GetComponent<Scrollbar>().handleRect);
            } else {
                selectionUIInstance.transform.SetParent(parent);
            }
            parentRect = selectionUIInstance.transform.parent.GetComponent<RectTransform>();
            selectionRect = selectionUIInstance.transform.GetChild(0).GetComponent<RectTransform>();
            topParent = selectionRect.transform.parent.GetComponent<RectTransform>();
            button = selectionRect.GetComponentInParent<UISelectable>();
            topParent.localScale = Vector3.one;
            topParent.localRotation = Quaternion.identity;
            topParent.sizeDelta = new Vector2(parentRect.sizeDelta.x, parentRect.sizeDelta.y);
            topParent.anchorMin = new Vector2(0.5f, 0.5f);
            topParent.anchorMax = new Vector2(0.5f, 0.5f);
            topParent.anchoredPosition = new Vector2(0, 0);
            selectionRect.localScale = Vector3.one;
            selectionRect.localRotation = Quaternion.identity;
            selectionRect.sizeDelta = new Vector2(button.selector.SizeDeltaX, button.selector.SizeDeltaY);
            selectionRect.anchorMin = new Vector2(0.5f, 0.5f);
            selectionRect.anchorMax = new Vector2(0.5f, 0.5f);
            selectionRect.anchoredPosition = new Vector2(button.selector.AnchoredPositionX, button.selector.AnchoredPositionY);
            Timing.RunCoroutine(_WaitOneFrame());
        } else {
            UnassignSelectionUI();
        }
    }
    void UnassignSelectionUI() {
        if (selectionUIInstance.transform.parent != this) {
            selectionUIInstance.transform.SetParent(null);
            selectionUIInstance.SetActive(false);
        }
    }
    public void UpdatedSelectionToEventSystem(GameObject selected) {
        if (EventSystem.current.currentSelectedGameObject != selected) {
            EventSystem.current.SetSelectedGameObject(selected);
        }
    }
    public void RemoveSelectionFromEventSystem() {
        EventSystem.current.SetSelectedGameObject(null);
        UnassignSelectionUI();
    }
    IEnumerator<float> _ActivationAwaiter() {
        while (!defaultSelectablesStack[defaultSelectablesStack.Count - 1].defaultButtonObject.activeInHierarchy) {
            yield return Timing.WaitForOneFrame;
        }
        UpdatedSelectionToEventSystem(defaultSelectablesStack[defaultSelectablesStack.Count - 1].defaultButtonObject);
    }
    public GameObject ReturnTopPanel() {
        if (defaultSelectablesStack.Count > 0) {
            return defaultSelectablesStack[defaultSelectablesStack.Count - 1].gameObject;
        }
        return null;
    }
    IEnumerator<float> _WaitOneFrame() {
        yield return Timing.WaitForOneFrame;
        selectionUIInstance.GetComponent<Canvas>().overrideSorting = true;
    }
}