﻿using System.Collections.Generic;
using UnityEngine;

public class Constants {

    #region UI Text
    public const string LEVEL_IS_LOCKED = "Level is Locked! Buy to unlock all Levels!";
    public const string BUY_SOME_CASH = "Buy Some Cash!";
    public const string NOT_ENOUGH_CASH = "Not Enough Cash!";
    public const string STORE = "Store";
    public const string _NEEDED_TO_UNLOCK = " needed to unlock";
    public const string _TO_UNLOCK = " to unlock";
    public const string SLASH = "/";

    public const string CLASSIC = "Classic";
    public const string ELIMINATION = "Elimination";
    public const string VERSUS = "V/S";
    public const string NONE = "None";

    public const string Tutorial_Continue_Mobile = "Tap to Continue";
    public const string Tutorial_Continue_TV = "Continue";
    #endregion

    #region Game Text
    public const string YOU_ARE_ELIMINITED = "You are Eliminited";
    public const string PLAYER_ = "Player ";
    public const string _ELIMINITED = " Eliminited";
    public const string WRONG_DIRECTION = "Wrong Direction";

    public const string POS_ = "Pos ";
    public const string _KM = " km";
    public const string LAP = "Lap:";
    #endregion

    #region Common
    public const string ZERO = "0";
    public const string DOUBLE_ZERO = "00";
    #endregion

    #region Application
    public static readonly string BUNDLE_IDENTIFIER = Application.identifier;
    public static string BUNDLE_IDENTIFIER_AND_VERSION { get { return Application.identifier + "_" + Application.version; } }
    public static string BUNDLE_VERSION = Application.version;
    #endregion

    #region Game Play Tags
    public const string PLAYER_TAG = "Player";
    public const string OPPONENT_TAG = "Opponent";
    public const string LAP_TAG = "Lap";
    public const string AD_ENDING = "Ad_Ending";
    public const string ROAD_TAG = "Road";
    public const string ROAD_OBJECT_TAG = "RoadObject";
    public const string RAILING_TAG = "Railing";
    public const string RAMP_TAG = "Ramp";
    public const string SMALL_RAMP_TAG = "SmallRamp";
    public const string TURN_RAMP_L_TAG = "TurnRampL";
    public const string TURN_RAMP_R_TAG = "TurnRampR";
    #endregion

    #region GameObject Name
    public const string PLAYER_NAME = "Player";
    public const string RIVAL_CAR_NAME = "RivalCar";
    public const string COLLIDER_NAME = "Collider";
    #endregion

    #region External Links
    public const string FB_PAGE_LINK = "https://www.facebook.com/*********";

#if UNITY_ANDROID
	public const string STORE_URL = "https://play.google.com/store/apps/dev?id=*****";
#elif UNITY_IOS
	public const string STORE_URL = "https://itunes.apple.com/us/developer/*****/id*****";
#endif

#if UNITY_ANDROID
	public const string RATE_URL = "https://play.google.com/store/apps/details?id=*******";
#elif UNITY_IOS
	public const string RATE_URL =  "https://itunes.apple.com/us/app/*******/id*******?ls=1&mt=8";
#endif
    #endregion

    #region Game Play Service
    //Leaderboards
    public const string LEADERBOARD_ID = "CgkIrqelqtsLEAIQAQ";

    //Achievements
    public static Dictionary<int, string> ACHIEVEMENT_IDS = new Dictionary<int, string>() {
        {50, "CgkIrqelqtsLEAIQCA"},
        {100, "CgkIrqelqtsLEAIQCQ"},
        {300, "CgkIrqelqtsLEAIQCg"},
        {500, "CgkIrqelqtsLEAIQCw"},
    };
    #endregion

    #region Analytics
    public const string DEFAULT = "Default";
    public const string FROM_SETTINGS = "From_Settings";
    #endregion
}


