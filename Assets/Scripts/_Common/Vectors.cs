﻿using UnityEngine;

public static class Vectors {

	public static readonly Vector3 v3_zero = new Vector3 (0, 0, 0);
	public static readonly Vector3 v3_one = new Vector3 (1, 1, 1);
	public static readonly Vector3 v3_two = new Vector3 (2, 2, 2);
	public static readonly Vector3 v3_left = new Vector3 (-1, 0, 0);
	public static readonly Vector3 v3_right = new Vector3 (1, 0, 0);
	public static readonly Vector3 v3_up = new Vector3 (0, 1, 0);
	public static readonly Vector3 v3_down = new Vector3 (0, -1, 0);
	public static readonly Vector3 v3_forward = new Vector3 (0, 0, 1);
	public static readonly Vector3 v3_backward = new Vector3 (0, 0, -1);
	public static readonly Vector3 v3_zero_point_one = v3_one * 0.1F;
	public static readonly Vector3 v3_zero_point_two = v3_one * 0.2F;
	public static readonly Vector3 v3_zero_point_three = v3_one * 0.3F;
	public static readonly Vector3 v3_zero_point_four = v3_one * 0.4F;
	public static readonly Vector3 v3_zero_point_five = v3_one * 0.5F;
	public static readonly Vector3 v3_zero_point_six = v3_one * 0.6F;
	public static readonly Vector3 v3_zero_point_seven = v3_one * 0.7F;
	public static readonly Vector3 v3_zero_point_eight = v3_one * 0.8F;
	public static readonly Vector3 v3_zero_point_nine = v3_one * 0.9F;
	public static readonly Vector3 v3_one_point_one = v3_one * 1.1F;
	public static readonly Vector3 v3_one_point_two = v3_one * 1.2F;
	public static readonly Vector3 v3_one_point_three = v3_one * 1.3F;
	public static readonly Vector3 v3_one_point_four = v3_one * 1.4F;
	public static readonly Vector3 v3_one_point_five = v3_one * 1.5F;

	public static readonly Vector2 v2_zero = new Vector2 (0, 0);
	public static readonly Vector2 v2_one = new Vector2 (1, 1);
	public static readonly Vector2 v2_two = new Vector2 (2, 2);
	public static readonly Vector2 v2_left = new Vector2 (-1, 0);
	public static readonly Vector2 v2_right = new Vector2 (1, 0);
	public static readonly Vector2 v2_up = new Vector2 (0, 1);
	public static readonly Vector2 v2_down = new Vector2 (0, -1);
	public static readonly Vector2 v2_zero_point_one = v2_one * 0.1F;
	public static readonly Vector2 v2_zero_point_two = v2_one * 0.2F;
	public static readonly Vector2 v2_zero_point_three = v2_one * 0.3F;
	public static readonly Vector2 v2_zero_point_four = v2_one * 0.4F;
	public static readonly Vector2 v2_zero_point_five = v2_one * 0.5F;
	public static readonly Vector2 v2_zero_point_six = v2_one * 0.6F;
	public static readonly Vector2 v2_zero_point_seven = v2_one * 0.7F;
	public static readonly Vector2 v2_zero_point_eight = v2_one * 0.8F;
	public static readonly Vector2 v2_zero_point_nine = v2_one * 0.9F;
	public static readonly Vector2 v2_one_point_one = v2_one * 1.1F;
	public static readonly Vector2 v2_one_point_two = v2_one * 1.2F;
	public static readonly Vector2 v2_one_point_three = v2_one * 1.3F;
	public static readonly Vector2 v2_one_point_four = v2_one * 1.4F;
	public static readonly Vector2 v2_one_point_five = v2_one * 1.5F;
	public static readonly Vector2 v2_one_point_six = v2_one * 1.6F;
	public static readonly Vector2 v2_one_point_seven = v2_one * 1.7F;
	public static readonly Vector2 v2_one_point_eight = v2_one * 1.8F;
	public static readonly Vector2 v2_one_point_nine = v2_one * 1.9F;

	public static readonly Vector3 v3_0_180_0 = new Vector3 (0, 180, 0);
	public static readonly Vector3 v3_0_90_0 = new Vector3 (0, 90, 0);
	public static readonly Vector3 v3_M35_0_0 = new Vector3 (-35, 0, 0);
	public static readonly Vector3 v3_M90_0_0 = new Vector3 (-90, 0, 0);
	public static readonly Vector3 v3_90_0_0 = new Vector3 (90, 0, 0);
	public static readonly Vector3 v3_35_0_0 = new Vector3 (35, 0, 0);
	public static readonly Vector3 v3_0_0_0 = new Vector3 (0, 0, 0);

	public static readonly Vector3 v3_M45_0_0 = new Vector3 (-45, 0, 0);
	public static readonly Vector3 v3_45_0_0 = new Vector3 (45, 0, 0);

	public static readonly Vector3 v3_M60_0_0 = new Vector3 (-45, 0, 0);
	public static readonly Vector3 v3_60_0_0 = new Vector3 (45, 0, 0);
}

