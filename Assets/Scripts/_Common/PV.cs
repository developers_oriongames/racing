﻿using UnityEngine;

public class PV : MonoBehaviour {

    public static readonly Vector2 V_0_0    = new Vector2(0, 0);
    public static readonly Vector2 V_1_1    = new Vector2(1, 1);
    public static readonly Vector3 V_P5_P5  = new Vector2(0.5F, 0.5F);

    public static readonly Vector3 V_0_0_0  = new Vector3(0, 0, 0);
    public static readonly Vector3 V_1_1_1  = new Vector3(1, 1, 1);
    public static readonly Vector3 V_2_2_2  = new Vector3(2, 2, 2);


    //**************************************************************************//
    //Unity Predefine Vector3s
    public static readonly Vector3 FORWARD  = new Vector3(0, 0, 1);
    public static readonly Vector3 UP       = new Vector3(0, 1, 0);
    public static readonly Vector3 RIGHT    = new Vector3(1, 0, 0);

    public static readonly Vector3 BACK     = new Vector3(0, 0, -1);
    public static readonly Vector3 DOWN     = new Vector3(0, -1, 0);
    public static readonly Vector3 LEFT     = new Vector3(-1, 0, 0);

    public static readonly Vector3 V_0_1_1 = new Vector3(0, 1, 1);
    public static readonly Vector3 V_1_0_1 = new Vector3(1, 0, 1);
    public static readonly Vector3 V_1_1_0 = new Vector3(1, 1, 0);
    //**************************************************************************//

    public static readonly Vector3 V_0_90_0     = new Vector3 (0, 90, 0);
	public static readonly Vector3 V_0_M90_0    = new Vector3 (0, -90, 0);
	public static readonly Vector3 V_0_180_0    = new Vector3 (0, 180, 0);
    public static readonly Vector3 V_0_0_360    = new Vector3(0, 0, 360);

    public static readonly Quaternion QUATERNION_IDENTITY = Quaternion.identity;

    public static readonly Vector3 V_0_0_0_0 = new Vector4(0, 0, 0, 0);
    public static readonly Vector3 V_1_1_1_1 = new Vector4(1, 1, 1, 1);

    public static readonly Color COLOR_0_0_0_0 = new Color(0, 0, 0, 0);
    public static readonly Color COLOR_1_1_1_1 = new Color(1, 1, 1, 1);
}
