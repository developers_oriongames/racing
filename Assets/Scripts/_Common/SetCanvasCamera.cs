﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SetCanvasCamera : MonoBehaviour {

    [HideInInspector]
    public Camera mainCamera;
    public Canvas[] canvas;

    void Awake() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDestroy() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void Start() {
        //AssignCamera();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        //AssignCamera();
    }

    void AssignCamera() {
        mainCamera = Camera.main;
        //SetCameraOthoSize();
        for (int i = 0; i < canvas.Length; i++) {
            canvas[i].renderMode = RenderMode.ScreenSpaceCamera;
            canvas[i].pixelPerfect = false;
            canvas[i].worldCamera = mainCamera;
            canvas[i].planeDistance = 1;
        }
    }
}
