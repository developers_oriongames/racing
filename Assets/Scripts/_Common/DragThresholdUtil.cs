﻿//#define Method1
#define Method2
using UnityEngine;
using UnityEngine.EventSystems;

public class DragThresholdUtil : MonoBehaviour {

#if Method1
    public int baseTH = 6;
    public int basePPI = 210;
    public int dragTH = 0;
#endif

    void Start() {
#if Method1
        dragTH = baseTH * (int)Screen.dpi / basePPI;
        EventSystem es = GetComponent<EventSystem>();
        if (es) es.pixelDragThreshold = dragTH;
#endif
#if Method2
        int defaultValue = EventSystem.current.pixelDragThreshold;
        EventSystem.current.pixelDragThreshold = Mathf.Max(defaultValue, (int)(defaultValue * Screen.dpi / 160f));
#endif
    }
}