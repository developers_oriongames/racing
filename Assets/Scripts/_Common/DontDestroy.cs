﻿using UnityEngine;

public class DontDestroy : MonoBehaviour {

	static DontDestroy Instance;

	void Awake () {
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad (gameObject);
			Application.targetFrameRate = 60;
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			QualitySettings.vSyncCount = 0;
		} else {
            Destroy(gameObject);
        }
    }
}