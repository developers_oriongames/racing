﻿public class GameKeys {

    #region Tutorial
    public const string TUTORIAL_STATUS_KEY = "tutorial_status_key";
    #endregion

    #region Cash
    public const string TOTAL_CASH_AMOUNT_KEY = "total_cash_amount_key";
    #endregion

    #region Car
    public const string KEY_CAR_PRICE_ = "key_car_price_";
    public const string KEY_CAR_UNLOCK_STATUS_ = "key_car_unlock_status_";
    #endregion

    #region Level Star
    public const string KEY_MAX_UNLOCKED_LEVEL = "key_max_unlocked_level";
    public const string KEY_LEVEL_STAR_ = "key_level_star_";
    #endregion

    #region IAP
    public const string REMOVED_ADS_KEY = "removed_ads_key";
    public const string UNLOCK_FULL_GAME_KEY = "unlock_full_game_key";
    public const string UNLOCK_ALL_CARS_KEY = "unlock_all_cars_key";
    public const string UNLOCK_ALL_LEVELS_KEY = "unlock_all_levels_key";
    #endregion

    #region Car Selection
    public const string LAST_CAR_NUMBER_KEY = "last_car_number_key";
    #endregion

    #region Music Selection
    public const string LAST_MUSIC_VOLUME_KEY = "last_music_volume_key";
    #endregion

    #region Sound Selection
    public const string LAST_SOUND_VOLUME_KEY = "last_sound_volume_key";
    #endregion

    #region In-Game Camera Zoom Level
    public const string LAST_IN_GAME_CAMERA_ZOOM_KEY = "last_in_game_camera_zoom_key";
    #endregion

    #region Controller
    public const string SELECTED_CONTROLLER_KEY = "selected_controller_key";
    #endregion

    #region Rate
    public const string RATE_GIVEN_KEY = "rate_given_key";
    #endregion

    #region Mega Offer
    public const string MEGA_OFFER_STATUS_KEY = "mega_offer_status_key";
    #endregion

}

