﻿using UnityEngine;

public static class FloatExtension {

    public static bool IsEqual(this float value1, float value2){
        if (Mathf.Abs(value1 - value2) <= Mathf.Epsilon)
            return true;
        return false;
    }

}
