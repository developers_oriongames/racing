﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

    private static ObjectPool _instance;
    public static ObjectPool Instance {
        get {
            if (_instance == null) {
                Debug.LogError("ObjectPool instance is null: " + _instance);
            }
            return _instance;
        }
    }
    [SerializeField] private int preInstantiationCount = 10;
    [SerializeField] private Transform _container;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private List<GameObject> _pool;
    void Awake() {
        _instance = this;
    }

    void Start() {
        _pool = Create(preInstantiationCount);
    }

    private List<GameObject> Create(int amount) {
        for (int i = 0; i < amount; i++) {
            GameObject go = Instantiate(_prefab, _container);
            go.SetActive(false);
            _pool.Add(go);
        }
        return _pool;
    }

    public GameObject Request() {
        //loop througn the object list (pool)
        foreach (var pooledGameObj in _pool) {
            //cheaking for inactive objects
            if (!pooledGameObj.activeInHierarchy) {
                //if found set it active and return
                pooledGameObj.SetActive(true);
                return pooledGameObj;
            }
        }
        //if not found (all objects are active), 
        //generate a new objects
        GameObject new_go = Instantiate(_prefab, _container);
        _pool.Add(new_go);
        return new_go;
    }
}