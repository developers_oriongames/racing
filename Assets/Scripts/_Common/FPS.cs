﻿using GameServices;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
public class FPS : MonoBehaviour {

	public TMP_Text _Text;

	public bool isFPS;
	public bool isController;
	public bool isPlatform;
	public bool isGamePlay;
	public bool isTutorial;
	public bool isRemoveAdsGame;

	public bool isAdLoadedStart;
	public bool isAdLoadedMiddle;
	public bool isAdLoadedEnd;

	public bool isSelectedCar;
	public bool isCurrentSelectedCar;

	[ContextMenu("AssignText")]
	void AssignText() {
		_Text = GetComponent<TMP_Text>();
    }

    void Start(){
		if(isFPS) InvokeRepeating("ShowFPS", 0, 0.2F);
		if (isController) InvokeRepeating ("ShowController", 0, 2.0F);
		if (isPlatform) InvokeRepeating ("ShowPlatform", 0, 2.0F);
		if (isGamePlay) InvokeRepeating ("ShowGamePlay", 0, 0.2F);
		if (isTutorial) InvokeRepeating ("ShowTutorial", 0, 0.2F);
		if (isRemoveAdsGame) InvokeRepeating ("ShowRemoveAdsGame", 0, 0.2F);

		if (isAdLoadedStart) InvokeRepeating ("ShowAdLoadedStart", 0, 0.2F);
		if (isAdLoadedMiddle) InvokeRepeating ("ShowAdLoadedMiddle", 0, 0.2F);
		if (isAdLoadedEnd) InvokeRepeating ("ShowAdLoadedEnd", 0, 0.2F);

		if (isSelectedCar) InvokeRepeating("ShowSelectedCar", 0, 0.2F);
		if (isCurrentSelectedCar) InvokeRepeating("ShowCurrentSelectedCar", 0, 0.2F);

	}

	void ShowFPS () {
        _Text.text = Mathf.CeilToInt((1 / Time.deltaTime)).ToString();
	}
	void ShowController() {
		_Text.text = "C: " + InputManager.GetCurrentController().ToString();
	}
	void ShowPlatform() {
		_Text.text = "P: " + InputManager.GetCurrentPlatform().ToString();
	}
	void ShowGamePlay() {
		_Text.text = "Game-Play: " + GameManager.isInGamePlay.ToString();
	}
	void ShowTutorial() {
		_Text.text = "Tutorial: " + GameManager.isInTutorial.ToString();
	}

	void ShowRemoveAdsGame() {
		_Text.text = "In Game Remove Ads: " + AdManager.isAdsRemoved.ToString();
	}

	void ShowAdLoadedStart() {
		_Text.text = "Ad Loaded (start): " + AdManager.IsInstastatialAdsLoaded(AdSpot.start);
	}
	void ShowAdLoadedMiddle() {
		_Text.text = "Ad Loaded (middle): " + AdManager.IsInstastatialAdsLoaded(AdSpot.middle);
	}
	void ShowAdLoadedEnd() {
		_Text.text = "Ad Loaded (end): " + AdManager.IsInstastatialAdsLoaded(AdSpot.end);
	}

	void ShowSelectedCar() {
		_Text.text = /*"Selected: " +*/ CarManager.GetSelectedCar().GetCarName().ToString();
	}
	void ShowCurrentSelectedCar() {
		_Text.text = /*"Current: " +*/ CarManager.GetCurrentSelectedCar().GetCarName().ToString();
	}
}
