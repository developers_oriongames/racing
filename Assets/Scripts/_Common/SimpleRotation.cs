﻿using UnityEngine;

public enum RotationDirection {
    UP,
    DOWN,
    FORWARD,
    BACK,
    RIGHT,
    LEFT,
}

public class SimpleRotation : MonoBehaviour {

    public RotationDirection rotationDirection = RotationDirection.UP;
	public float rotationSpeed = 20;

    Transform _transform;
    Vector3 rotationVector;

    void Awake() {
        _transform = transform;
    }

    void Start() {
        switch (rotationDirection) {
            case RotationDirection.UP:
                rotationVector = PV.UP;
                break;
            case RotationDirection.DOWN:
                rotationVector = PV.DOWN;
                break;
            case RotationDirection.FORWARD:
                rotationVector = PV.FORWARD;
                break;
            case RotationDirection.BACK:
                rotationVector = PV.BACK;
                break;
            case RotationDirection.RIGHT:
                rotationVector = PV.RIGHT;
                break;
            case RotationDirection.LEFT:
                rotationVector = PV.LEFT;
                break;
        }
    }

    void Update () {
		_transform.Rotate (rotationVector * rotationSpeed * Time.deltaTime);
	}
}
