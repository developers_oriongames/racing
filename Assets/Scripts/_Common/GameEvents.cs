﻿#define ACTIVE_RI_SDK

#if ACTIVE_RI_SDK
using com.amazon.device.iap.cpt;
#endif
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents {

    //Input
    //->back<-//
    public static Action OnInputBack = delegate { };
    //->mouse<-//
    public static Action OnMouseDown = delegate { };
    public static Action OnMouseUp = delegate { };
    //->sensor controller<-//
    public static Action<Vector3> OnInputAcceleration = delegate { };
    public static Action<float> OnInputHorizontalAxis = delegate { };
    //->touch controller(for testing)<-//
    public static Action OnKeyA = delegate { };
    public static Action OnKeyUpA = delegate { };
    public static Action OnKeyD = delegate { };
    public static Action OnKeyUpD = delegate { };
    //->amazon tv remote<-//
    public static Action OnInputMenu = delegate { };
    public static Action OnInputJoystick_0 = delegate { };
    public static Action OnInputLeft = delegate { };
    public static Action OnInputRight = delegate { };
    public static Action OnInputEndLeft = delegate { };
    public static Action OnInputEndRight = delegate { };
    public static Action OnInputUp = delegate { };
    public static Action OnInputEndUp = delegate { };
    public static Action OnInputDown = delegate { };
    public static Action OnInputEndDown = delegate { };

    //UI Car Selection
    public static Action<Car> OnCarSelectionChange = delegate { };

    //UI Menu
    public static Action<bool> OnUICarSelection = delegate { };
    public static Action<bool> OnUILevelSelection = delegate { };
    public static Action<bool> OnUIGameMode = delegate { };
    public static Action<bool> OnUITopBar = delegate { };
    public static Action<bool> OnUICarPreview = delegate { };
    public static Action<bool> OnUIStore = delegate { };
    public static Action<bool> OnUISettings = delegate { };
    public static Action<bool> OnUIExit = delegate { };
    public static Action<bool> OnUIHelp = delegate { };
    public static Action<bool> OnUIRate = delegate { };
    public static Action<bool> OnUIMegaOffer = delegate { };

    public static Action<string> OnPlayTutorial = delegate { };

    //Track
    public static Action<int> OnGameLevelLoad = delegate { };

    //GamePlay
    public static Action OnGameStartScreen = delegate { };
    public static Action OnGamePlayScreen = delegate { };
    public static Action OnGameOverScreen = delegate { };
    public static Action OnPauseScreen = delegate { };
    public static Action OnGameCompleteScreen = delegate { };
    public static Action OnEliminationScreen = delegate { };

    //Tutorial
    public static Action <bool> OnTutorialPaused = delegate { };
    public static Action<bool> OnControlerSelected = delegate { };
    public static Action<bool> OnBreakFeatureShowed = delegate { };
    public static Action<bool> OnBoostFeatureShowed = delegate { };
    public static Action<bool> OnTutorialDemonstrationOver = delegate { };

    //IAP
    public static Action<string> OnPurchaseStarted = delegate { };
#if ACTIVE_RI_SDK
    public static Action<List<PurchaseReceipt>> OnRestorePurchase = delegate { };
#endif
    public static Action<string> OnPurchaseComplete = delegate { };
    public static Action<string, string> OnPurchaseFailed = delegate { };

    //AdExpert
    public static Action OnVideoAdWatch = delegate { };
    public static Action OnInterstitialAdWatch = delegate { };
    public static Action OnRevivalOffered = delegate { };

    //CurrencyManager
    public static Action OnCurrencyIncreasedInGamePlay = delegate { };
    public static Action OnNotEnoughCurrency = delegate { };

    //VideoCurrencyOffer
    public static Action OnWatchFreeVideo = delegate { };

    //GameManager
    public static Action OnGamePlay = delegate { };
    public static Action OnGameOver = delegate { };
    public static Action OnTutorial = delegate { };
    public static Action OnRevival = delegate { };

    //Play
    public static Action OnPlayButtonPressed = delegate { };

    //PurchaseManager
    public static Action OnItemSelect = delegate { };
    public static Action<IAPButtonType> OnItemPurchased = delegate { };

    //Navigation
    public static Action OnSceneLoadStarted = delegate { };
    public static Action OnSceneLoadFinished = delegate { };
    public static Action OnBackButtonPressedInTutorial = delegate { };

    //Notification
    public static Action OnNotificationPopupOpen = delegate { };
    public static Action OnNotificationPopupClose = delegate { };
    public static Action OnPauseBackButtonPressed = delegate { };
    public static Action OnApplicationExit = delegate { };

    //RateApp
    //public static Action<RateType> OnRateOffered = delegate { };
    //public static Action<RateType> OnRateDone = delegate { };

    //SoundManager
    public static Action OnFXToggled = delegate { };
    public static Action OnMusicToggled = delegate { };

    //Setting View
    public static Action OnSettingsPanelOpen = delegate { };
    public static Action OnSettingsPanelClose = delegate { };

    //Vibration Manager
    public static Action OnVibrationToggled = delegate { };

}