﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TrialCars
{
    public Dictionary<string, bool> dictionary = new Dictionary<string, bool>();
    
    public TrialCars(CarManager carManager)
    {
        Car[] cars = carManager.cars;
        AddCarsToDictionary(cars);
    }

    public void AddCarsToDictionary(Car[] cars)
    {
        foreach(Car car in cars)
        {
            if(!dictionary.ContainsKey(car.GetCarName().ToLower()))
            {
                dictionary.Add(car.GetCarName().ToLower(), true);
            }
        }
    }

    public bool IfCarIsOnTrial(Car car)
    {
        if(dictionary.ContainsKey(car.GetCarName().ToLower()))
        {
            return dictionary[car.GetCarName().ToLower()];
        }

        return false;
    }

    public void SetValueFromRemoteConfig(string key, bool isOnTrial)
    {
        if(dictionary.ContainsKey(key))
        {
            dictionary[key] = isOnTrial;
        }
    }

    public static Dictionary<string,string> GetPreparedDictionaryForRemoteConfig(Car[] cars)
    {
        Dictionary<string, string> tempDictionary = new Dictionary<string, string>();

        foreach(Car car in cars)
        {
            if(!tempDictionary.ContainsKey(car.GetCarName().ToLower()))
            {
                tempDictionary.Add(car.GetCarName().ToLower(), "1");
                Debug.Log("UNITY_TRIAL_CAR: " + car.GetCarName().ToLower() + " has been added to the dictionary.");
            }
        }


        return tempDictionary;
    }

    public static Dictionary<string,string> GetPreparedDictionary()
    {
        Dictionary<string, string> tempDictionary = new Dictionary<string, string>();

        tempDictionary.Add("tryNow", "true");
        tempDictionary.Add("booster", "false");

        return tempDictionary;
    }
}
