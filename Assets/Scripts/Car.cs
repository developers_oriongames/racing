﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Car", menuName = "Car")]
public class Car : ScriptableObject {

	[Header("Basic Infos")]
	[SerializeField] private int carID;
	[SerializeField] private string carName;
	[Header("Purchase Info")]
	[SerializeField] private bool isUnlocked;
	[SerializeField] private int carInGamePrice;
	[SerializeField] private float carStorePrice;
	[SerializeField] private NonConsumableIAPItems amazonIAPItem;
	[Header("Configarations")]
	[SerializeField] private float acceleration;
	[SerializeField] private float topSpeed;
	[SerializeField] private float handling;
	[SerializeField] private float nitro;
	[Header("Prefab")]
	[SerializeField] private GameObject carPrefab;

	public int GetCarID() {
		return carID;
	}
	public string GetCarName() {
		return carName;
	}
	public bool GetPurchaseStatus() {
		return isUnlocked;
	}
	public void SetPurchaseStatus(bool status) {
		isUnlocked = status;
	}
	public int GetInGameCarPrice() {
		return carInGamePrice;
	}
	public float GetStoreCarPrice() {
		return carStorePrice;
	}
	public NonConsumableIAPItems GetAmazonIAPItem() {
		return amazonIAPItem;
	} 
	public float GetAcceleration() {
		return acceleration;
	}
	public float GetTopSpeed() {
		return topSpeed;
	}
	public float GetHandling() {
		return handling;
	}
	public float GetNitro() {
		return nitro;
	}
}
