﻿using GameServices;
using GameServices.UserData;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using System;

public class TutorialManager : MonoBehaviour {

    private static TutorialManager instance;

    public static bool isTutorialPaused;
    public static bool isControlerSelected;
    public static bool isBreakFeatureShowed;
    public static bool isBoostFeatureShowed;
    public static bool isTutorialDemonstrationOver;

    private IEnumerator<float> _coroutine_control_option;
    private IEnumerator<float> _coroutine_sensor_panel;
    private IEnumerator<float> _coroutine_touch_panel;
    private IEnumerator<float> _coroutine_remote_panel;
    private IEnumerator<float> _coroutine_break_panel;
    private IEnumerator<float> _coroutine_boost_panel;
    private IEnumerator<float> _coroutine_demonstration_over;

    private const float wait_for_control_option = 3;
    private const float wait_for_sensor_panel = 1.5f;
    private const float wait_for_touch_panel = 1.5f;
    private const float wait_for_remote_panel = 1.5f;
    private const float wait_for_break_panel = 2;
    private const float wait_for_boost_panel = 5.5f;
    private const float wait_for_demonstration_over = 1.0f;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    void OnEnable() {
        GameEvents.OnBreakFeatureShowed += SetCarSpeed;
    }

    void OnDisable() {
        GameEvents.OnBreakFeatureShowed -= SetCarSpeed;
    }

    private void SetCarSpeed(bool isBreakFeatureShowed) {
       carTopSpeed = !isBreakFeatureShowed ? 150 : 200;
    }

    void Start() {
        _coroutine_control_option = ShowControlOption();
        _coroutine_sensor_panel = ShowSensorControlerPanel();
        _coroutine_touch_panel = ShowTouchControlerPanel();
        _coroutine_remote_panel = ShowRemoteControlerPanel();
        _coroutine_break_panel = ShowBreakPanel();
        _coroutine_boost_panel = ShowBoostPanel();
        _coroutine_demonstration_over = TutorialDemostrationOver();
        UIManager.Instance.uITutorial.ShowWelcomePanel();
    }

    #region Step 0
    public static void ContinueFromWelcome() {
        UIManager.Instance.uITutorial.ContinueFromWelcome();
        AudioManager.PlayGamePlaySound(GamePlaySoundType.Start);
    }
    #endregion

    #region Step 1 
    public static void GameStartAnimationEnded() {
        UIManager.CurrentScreen = GameScreen.GamePlayScreen;
        //GameEvents.OnGamePlayScreen();
        Timing.RunCoroutine(instance._coroutine_control_option);
    }

    static IEnumerator<float> ShowControlOption() {
        yield return Timing.WaitForSeconds(wait_for_control_option);
        PauseTutorial();
        UIManager.Instance.uITutorial.ShowControlerSelectionPanel();
        yield return Timing.WaitForOneFrame;
    }
    #endregion

    #region Step 2
    public static void SelectSensorControler() {
        UIManager.Instance.uITutorial.HideControlerSelectionPanel();
        UnPauseTutorial();
        Timing.RunCoroutine(instance._coroutine_sensor_panel);
    }
    static IEnumerator<float> ShowSensorControlerPanel() {
        yield return Timing.WaitForSeconds(wait_for_sensor_panel);
        PauseTutorial();
        UIManager.Instance.uITutorial.ShowSensorControlerPanel();
        yield return Timing.WaitForOneFrame;
    }
    public static void SelectTouchControler() {
        UIManager.Instance.uITutorial.HideControlerSelectionPanel();
        UnPauseTutorial();
        Timing.RunCoroutine(instance._coroutine_touch_panel);
    }
    static IEnumerator<float> ShowTouchControlerPanel() {
        yield return Timing.WaitForSeconds(wait_for_touch_panel);
        PauseTutorial();
        UIManager.Instance.uITutorial.ShowTouchControlerPanel();
        yield return Timing.WaitForOneFrame;
    }
    public static void SelectRemoteControler() {
        UIManager.Instance.uITutorial.HideControlerSelectionPanel();
        UnPauseTutorial();
        Timing.RunCoroutine(instance._coroutine_remote_panel);
    }
    static IEnumerator<float> ShowRemoteControlerPanel() {
        yield return Timing.WaitForSeconds(wait_for_remote_panel);
        PauseTutorial();
        UIManager.Instance.uITutorial.ShowRemoteControlerPanel();
        yield return Timing.WaitForOneFrame;
    }
    #endregion

    #region Step 3
    static void HideControlerPanels() {
        UIManager.Instance.uITutorial.HideSensorControlerPanel();
        UIManager.Instance.uITutorial.HideTouchControlerPanel();
        UIManager.Instance.uITutorial.HideRemoteControlerPanel();
        UnPauseTutorial();
        Timing.RunCoroutine(instance._coroutine_break_panel);
    }
    static IEnumerator<float> ShowBreakPanel() {
        yield return Timing.WaitForSeconds(wait_for_break_panel);
        PauseTutorial();
        UIManager.Instance.uITutorial.ShowBrakePanel();
        yield return Timing.WaitForOneFrame;
    }
    #endregion

    #region Step 4
    static void HideBreakPanel() {
        UIManager.Instance.uITutorial.HideBrakePanel();
        UnPauseTutorial();
        Timing.RunCoroutine(instance._coroutine_boost_panel);
    }
    static IEnumerator<float> ShowBoostPanel() {
        yield return Timing.WaitForSeconds(wait_for_boost_panel);
        PauseTutorial();
        UIManager.Instance.uITutorial.ShowBoostPanel();
        yield return Timing.WaitForOneFrame;
    }
    #endregion

    #region Step 5
    static void HideBoostPanel() {
        UnPauseTutorial();
        UIManager.Instance.uITutorial.HideBoostPanel();
        Timing.RunCoroutine(instance._coroutine_demonstration_over);
    }

    static IEnumerator<float> TutorialDemostrationOver() {
        yield return Timing.WaitForSeconds(wait_for_demonstration_over);
        GameEvents.OnTutorialDemonstrationOver(true);
        isTutorialDemonstrationOver = true;
        yield return Timing.WaitForOneFrame;
    }
    #endregion

    static void PauseTutorial() {
        Time.timeScale = 0;
        GameEvents.OnTutorialPaused(true);
        isTutorialPaused = true;
        GameManager.isInPause = true;
        AudioManager.StopCarSound();
    }

    static void UnPauseTutorial() {
        Time.timeScale = 1;
        GameEvents.OnTutorialPaused(false);
        isTutorialPaused = false;
        GameManager.isInPause = false;
        GameController.Instance.PlayBgSound();
    }

    /// <summary>
    /// set car features to modified valus during tutorial demonstration
    /// </summary>
    static float carTopSpeed = 150; 
    public static float GetTutorialCarTopSpeed() {
        return carTopSpeed;     
    }

    public static void ContinueFromTutorial(int stage) {
        switch (stage) {
            case 0:
                //Sensor Panel Ok
                HideControlerPanels();
                InputManager.SetCurrentController(ControllerTypes.Sensor);
                GameEvents.OnControlerSelected (true);
                isControlerSelected = true;
                break;
            case 1:
                //Touch Panel Ok
                HideControlerPanels();
                InputManager.SetCurrentController(ControllerTypes.Touch);
                GameEvents.OnControlerSelected (true);
                isControlerSelected = true;
                break;
            case 2:
                //Remote Panel Ok
                HideControlerPanels();
                InputManager.SetCurrentController(ControllerTypes.Remote);
                GameEvents.OnControlerSelected (true);
                isControlerSelected = true;
                break;
            case 3:
                //Break Panel Ok
                HideBreakPanel();
                GameEvents.OnBreakFeatureShowed (true);
                isBreakFeatureShowed = true;
                break;
            case 4:
                //Boost Panel Ok
                HideBoostPanel();
                GameEvents.OnBoostFeatureShowed (true);
                isBoostFeatureShowed = true;
                break;
        }
    }

    public static void CompleteTutorial() {
        GameManager.isInTutorial = false;
        UserDataManager.SetTutorialShownStatus(true);
        Navigation.LoadScene(Scenes.Menu, GameScreen.GameLoading);
    }
}
