﻿using UnityEngine;

public class RailingCollider : MonoBehaviour
{
    [SerializeField] private Transform wideWallCollider;
    [SerializeField] private Transform narrowWallCollider;

    [SerializeField] private MeshRenderer narrowMeshRenderer;

    void Awake()
    {
        if (narrowMeshRenderer != null)
        {
            narrowMeshRenderer.enabled = false;
        }
    }
    void Start()
    {
        //print("LevelManager.selectedLevel: " + LevelManager.selectedLevel);
        bool isToActiveNarrowCol = LevelManager.selectedLevel + 1 <= 5;
        //print("isToActiveNarrowCol: " + isToActiveNarrowCol);
        narrowWallCollider.gameObject.SetActive(isToActiveNarrowCol);
        wideWallCollider.gameObject.SetActive(!isToActiveNarrowCol);
    }
}
