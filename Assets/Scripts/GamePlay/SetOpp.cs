﻿using UnityEngine;
using GameServices;

public class SetOpp : MonoBehaviour {

	private readonly int[] mOppNo = { 0, 4, 8, 12 };
	public string[] Name = { 
		"Johnny Butler",
		"Ruby Campbell",
		"Mark Brooks",
		"Donna Morris",
		"Charles Wright",
		"Diane Moore",
		"Jessica Rogers",
		"Carolyn Davis",
		"Jane Parker",
		"Ryan Griffin"
	};
	
	void Awake() {
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).gameObject.SetActive(false);
		}
		if (GameModeManager.GetCurrentGameMode == GameModes.Versus) {
			int rnd = Random.Range(0, 4);
			int no = Random.Range(0, mOppNo.Length);
			mOppNo[no] += rnd;
			if (mOppNo[no] > 14) {
				mOppNo[no] = 14;
			}
			transform.GetChild(mOppNo[no]).gameObject.SetActive(true);
			transform.GetChild(mOppNo[no]).SetSiblingIndex(0);
		} else {
			for (int i = 0; i < mOppNo.Length; i++) {
				int rnd = Random.Range(0, 4);
				mOppNo[i] += rnd;
				if (mOppNo[i] > 14) {
					mOppNo[i] = 14;
				}
				transform.GetChild(mOppNo[i]).gameObject.SetActive(true);
				transform.GetChild(mOppNo[i]).SetSiblingIndex(i);
			}
		}
	}
}
