﻿using UnityEngine;

public class GameShop : MonoBehaviour {

	[SerializeField] private GameObject[] shopCars = new GameObject[15];
	[SerializeField] private GameObject[] shadowCars = new GameObject[15];
	private static GameShop instance;

	void Awake() {
		instance = this;
	}

    void Start() {
		Init();
    }

    void OnEnable() {
        Init();
    }

    private void Init() {
        int currentSelectedCar = CarManager.GetCurrentSelectedCar().GetCarID();
        for (int i = 0; i < CarManager.GetTotalCarsCount(); i++) {
            shopCars[i].SetActive(i == currentSelectedCar);
            shadowCars[i].SetActive(i == currentSelectedCar);
        }
    }

    public static void CarSelection(int direction) {
		int temp = CarManager.GetCurrentSelectedCar().GetCarID() + direction;
		int select;
		if (temp < 0) {
			select = CarManager.GetTotalCarsCount() - 1;
		} else if (temp > CarManager.GetTotalCarsCount() - 1) {
			select = 0;
		} else {
			select = temp;
		}
		CarManager.SetCurrentSelectedCar(select);
		Car currentSelectedCar = CarManager.GetCurrentSelectedCar();
		for (int i = 0; i < CarManager.GetTotalCarsCount(); i++) {
			instance.shopCars[i].SetActive(i == currentSelectedCar.GetCarID());
			instance.shadowCars[i].SetActive(i == currentSelectedCar.GetCarID());
		}
		GameEvents.OnCarSelectionChange(currentSelectedCar);
	}

	public static void SetCorrectCar() {
		for (int i = 0; i < CarManager.GetTotalCarsCount(); i++) {
			instance.shopCars[i].SetActive(false);
			instance.shadowCars[i].SetActive(false);
			if (CarManager.Instance.cars[i].GetPurchaseStatus()) {
				CarManager.SetCurrentSelectedCar(i);
			}
		}
		instance.shopCars[CarManager.GetCurrentSelectedCar().GetCarID()].SetActive(true);
		instance.shadowCars[CarManager.GetCurrentSelectedCar().GetCarID()].SetActive(true);
	}
}