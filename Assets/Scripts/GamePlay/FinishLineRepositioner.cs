﻿using Sirenix.OdinInspector;
using UnityEngine;

public class FinishLineRepositioner : MonoBehaviour
{
    // This script must be set on Finishline
    public Vector3 originalPosition;
    public Vector3 originalRotation;

    [Header("Shorten")] public Vector3 shortenPosition;
    public Vector3 shortenRotation;

    void Start()
    {
        if (LevelManager.selectedLevel < 10)
        {
            ShortenLocation();
        }
        else
        {
            OriginalLocation();
        }
    }

    [Button]
    public void OriginalLocation()
    {
        transform.position = originalPosition;
        transform.rotation = Quaternion.Euler(originalRotation);
    }

    [Button]
    public void ShortenLocation()
    {
        transform.position = shortenPosition;
        transform.rotation = Quaternion.Euler(shortenRotation);
    }
}