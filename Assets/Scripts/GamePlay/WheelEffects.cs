using System.Collections.Generic;
using UnityEngine;
using MEC;

public class WheelEffects : MonoBehaviour {

    public Transform SkidTrailPrefab;
    public static Transform skidTrailsDetachedParent;
    public ParticleSystem skidParticles;
    public bool Skidding { get; private set; }
    public bool PlayingAudio { get; private set; }

    private AudioSource m_AudioSource;
    private Transform m_SkidTrail;
    private WheelCollider m_WheelCollider;

    private void Start() {
        //skidParticles = transform.root.GetChild(transform.root.childCount-1).gameObject.GetComponent<ParticleSystem>();
        if (skidParticles == null) {
            //Debug.LogWarning(" no particle system found on car to generate smoke particles");
        } else {
            skidParticles.Stop();
        }
        m_WheelCollider = GetComponent<WheelCollider>();
        m_AudioSource = GetComponent<AudioSource>();
        PlayingAudio = false;
        if (skidTrailsDetachedParent == null) {
            skidTrailsDetachedParent = new GameObject("Skid Trails - Detached").transform;
        }
    }

    public void EmitTyreSmoke() {
        skidParticles.transform.position = transform.position - transform.up * m_WheelCollider.radius;
        skidParticles.Emit(1);
        if (!Skidding) {
            Timing.RunCoroutine(_StartSkidTrail());
        }
    }

    public void PlayAudio() {
        //			if (SettingHandle.isSound && CarUserControl.Gamescreen == CarUserControl.GamePlay && transform.parent.parent.tag !="AI") {
        //				if (m_AudioSource != null && CarUserControl.currentLap<=2) {
        //					 m_AudioSource.Play ();
        //					 PlayingAudio = true;
        //
        //				}
        //		    }
    }

    public void StopAudio() {
        if (m_AudioSource != null) {
            m_AudioSource.Stop();
            PlayingAudio = false;
        }
    }

    public IEnumerator<float> _StartSkidTrail() {
        Skidding = true;
        m_SkidTrail = Instantiate(SkidTrailPrefab); //TODO use object pooling
        while (m_SkidTrail == null) {
            yield return Timing.WaitForOneFrame;
        }
        m_SkidTrail.parent = transform;
        m_SkidTrail.localPosition = -Vector3.up * m_WheelCollider.radius;
    }

    public void EndSkidTrail() {
        if (!Skidding) {
            return;
        }
        Skidding = false;
        m_SkidTrail.parent = skidTrailsDetachedParent;
        Destroy(m_SkidTrail.gameObject, 10);    //TODO use object pooling
    }
}