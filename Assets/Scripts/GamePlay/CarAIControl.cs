using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using GameServices;
using MEC;

public class CarAIControl : MonoBehaviour {
	public enum BrakeCondition {
		NeverBrake,
		TargetDirectionDifference,
		TargetDistance,
	}
	[SerializeField] [Range(0, 1)] private float m_CautiousSpeedFactor = 0.05f;
	[SerializeField] [Range(0, 180)] private float m_CautiousMaxAngle = 50f;
	[SerializeField] private float m_CautiousMaxDistance = 100f;
	[SerializeField] private float m_CautiousAngularVelocityFactor = 30f;
	[SerializeField] private float m_SteerSensitivity = 0.05f;
	[SerializeField] private float m_AccelSensitivity = 0.04f;
	[SerializeField] private float m_BrakeSensitivity = 1f;
	[SerializeField] private float m_LateralWanderDistance = 3f;
	[SerializeField] private float m_LateralWanderSpeed = 0.1f;
	[SerializeField] [Range(0, 1)] private float m_AccelWanderAmount = 0.1f;
	[SerializeField] private float m_AccelWanderSpeed = 0.1f;
	[SerializeField] private BrakeCondition m_BrakeCondition = BrakeCondition.TargetDistance;
	[SerializeField] private bool m_Driving;
	[SerializeField] private Transform m_Target;

	private float m_RandomAcc, mBoostSpd, mNormalSpd;//m_RandomOffset
	public float m_AvoidOtherCarTime, m_AvoidOtherCarSlowdown, m_AvoidPathOffset, mPerfectTime;
	[SerializeField] float mSensorLen = 5, mSensorPoint = 0, mSensorSidePoint = 0;//mSideWaySenLen=5,
	[SerializeField] float mFsensorAng = 30, oppSpeed;

	public float mBoostPower = 10, mBoostTime = 0, mCarPower, mOppTime;
	public int mOppRank = 0, mBoostVal = 0, mActionType = 0;

	[SerializeField] int Hit = 0, currentLap = 0;

	bool isTakker, isShadow = true;
	public bool isRaceComp = false;

	public string playerNameAI;

	private GameObject BoostEffectObj;
	private CarController m_CarController;
	private Rigidbody m_Rigidbody;
	private WaypointFollow m_WaypointFollow;
	private Transform _transform;

	void Start() {
		_transform = transform;
		m_Rigidbody = GetComponent<Rigidbody>();
		m_WaypointFollow = GetComponent<WaypointFollow>();
		m_CarController = GetComponent<CarController>();
		m_Target = transform.GetChild(8).transform;
		BoostEffectObj = transform.GetChild(5).gameObject;

		m_RandomAcc = Random.value * 100f;
		currentLap = 0;
		BoostEffect(false);
		SetTarget(m_Target);
		isRaceComp = false;
		m_LateralWanderDistance = -6f + transform.GetSiblingIndex() * 5f;
		m_LateralWanderSpeed = 0.02f;
		mBoostSpd = m_CarController.MaxSpeed + 20f;
		mNormalSpd = m_CarController.MaxSpeed;
		mBoostVal = 1;
		mBoostPower = 15;
		mBoostTime = 8f;
		mPerfectTime = 0;
		isShadow = true;
		transform.GetChild(transform.childCount - 1).localPosition = new Vector3(0, 50, 0);
		playerNameAI = transform.GetComponentInParent<SetOpp>().Name[transform.GetSiblingIndex()];
	}

	float deltaTime;
	void Update() {
		deltaTime = Time.deltaTime;
		if (_transform.GetChild(9).GetChild(0).gameObject != null) {
			_transform.GetChild(9).GetChild(0).gameObject.SetActive(UIManager.CurrentScreen == GameScreen.GamePlayScreen);
		}
		if (UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
			oppSpeed = m_CarController.CurrentSpeed;
			_transform.GetChild(9).GetChild(0).GetChild(0).GetComponent<Text>().text = mOppRank + "";
			if (mBoostVal > 0) {
				m_CarController.MaxSpeed = Mathf.Lerp(m_CarController.MaxSpeed, mBoostSpd, deltaTime * 4f);
			} else {
				m_CarController.MaxSpeed = Mathf.Lerp(m_CarController.MaxSpeed, mNormalSpd, deltaTime);
			}
			mPerfectTime += deltaTime;
			if (mPerfectTime > 35) {
				mBoostVal = 1;
				mBoostPower = 20;
				mBoostTime += 10;
				mPerfectTime = -Random.Range(2, 4);
			}
			HideShadow();
			_transform.GetChild(7).gameObject.SetActive(isShadow);
		}
		if (UIManager.CurrentScreen == GameScreen.GameOverScreen && !isRaceComp) {
			isRaceComp = true;
			m_Driving = false;
			currentLap = CarUserControl.mTotalLap;
		}
		//Debug.Log ("     "+mOppRank+"        "+mOppTime);
	}
	
	void FixedUpdate() {
		if (m_Target == null) {
			return;
		}
		if (!m_Driving && isRaceComp) {
			AutoMove();
			ResetCar();
			ApplyPower(mCarPower);
			//_transform.GetComponent<Rigidbody>().AddForce(_transform.forward*(CarUserControl.Power)*.5f,ForceMode.Force);
		} else {
			if (UIManager.CurrentScreen != GameScreen.GameStartScreen) {
				if (m_Rigidbody.IsSleeping()) {
					m_Rigidbody.WakeUp();
				}

				Vector3 fwd = _transform.forward;
				if (m_Rigidbody.velocity.magnitude > m_CarController.MaxSpeed * 0.1f) {
					fwd = m_Rigidbody.velocity;
				}
				float desiredSpeed = m_CarController.MaxSpeed;
				switch (m_BrakeCondition) {
					case BrakeCondition.TargetDirectionDifference: {
							float approachingCornerAngle = Vector3.Angle(m_Target.forward, fwd);
							float spinningAngle = m_Rigidbody.angularVelocity.magnitude * m_CautiousAngularVelocityFactor;
							float cautiousnessRequired = Mathf.InverseLerp(0, m_CautiousMaxAngle, Mathf.Max(spinningAngle, approachingCornerAngle));
							desiredSpeed = Mathf.Lerp(m_CarController.MaxSpeed * m_CautiousSpeedFactor, m_CarController.MaxSpeed, cautiousnessRequired);
							break;
						}
					case BrakeCondition.TargetDistance: {
							Vector3 delta = m_Target.position - _transform.position;
							float distanceCautiousFactor = Mathf.InverseLerp(m_CautiousMaxDistance, 0, delta.magnitude);
							float spinningAngle = m_Rigidbody.angularVelocity.magnitude * m_CautiousAngularVelocityFactor;
							float cautiousnessRequired = Mathf.Max(Mathf.InverseLerp(0, m_CautiousMaxAngle, spinningAngle), distanceCautiousFactor);
							desiredSpeed = Mathf.Lerp(m_CarController.MaxSpeed, m_CarController.MaxSpeed * m_CautiousSpeedFactor, cautiousnessRequired);
							break;
						}
					case BrakeCondition.NeverBrake:
						break;
				}

				Vector3 offsetTargetPos = m_Target.position;

				if (Time.time < m_AvoidOtherCarTime) {
					if (isTakker) {
						desiredSpeed *= m_AvoidOtherCarSlowdown;
					} else {
						if (Hit > 0) {
							m_AvoidPathOffset = changeAng * oppSpeed * 0.01f;
							offsetTargetPos += m_Target.right * m_AvoidPathOffset;
							//desiredSpeed *= m_AvoidOtherCarSlowdown;
						}
					}
				} else {
					offsetTargetPos += m_Target.right * (Mathf.Sin(m_LateralWanderSpeed) * m_LateralWanderDistance);
					m_LateralWanderSpeed += Time.fixedDeltaTime * 0.1f;
				}
				float accelBrakeSensitivity = (desiredSpeed < m_CarController.CurrentSpeed) ? m_BrakeSensitivity : m_AccelSensitivity;
				float accel = Mathf.Clamp((desiredSpeed - m_CarController.CurrentSpeed) * accelBrakeSensitivity, -1, 1);
				accel *= (1 - m_AccelWanderAmount) + (Mathf.PerlinNoise(Time.time * m_AccelWanderSpeed, m_RandomAcc) * m_AccelWanderAmount);
				Vector3 localTarget = _transform.InverseTransformPoint(offsetTargetPos);
				float targetAngle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
				float steer = Mathf.Clamp(targetAngle * m_SteerSensitivity, -1, 1) * Mathf.Sign(m_CarController.CurrentSpeed);
				if (m_Driving) {
					Sensor2();
					m_CarController.Move(steer, accel, accel, 0f);
					ApplyPower(mCarPower);
					mOppTime = Time.time - CarUserControl.StartTime;
					Timing.RunCoroutine(_Boost(mBoostPower));
					Timing.RunCoroutine(_ApplyJumpForce(steer));
				}
				ResetCar();
			}
		}
	}

	float mResetCnt = 0;
	RaycastHit hit;
	void ResetCar() {
		Vector3 velocity = _transform.InverseTransformDirection(m_Rigidbody.velocity);
		if ((velocity.y < -1 && _transform.position.y < -25f) || (velocity.y > 1 && _transform.position.y > 25f)) {
			mResetCnt += deltaTime * 2f;
			if (mResetCnt > 1) {
				Vector3 pos = m_WaypointFollow.target.position;
				pos.y += 2;
				_transform.position = pos;
				Quaternion rot = Quaternion.LookRotation(m_WaypointFollow.circuit.GetRoutePoint(m_WaypointFollow.progressDistance).direction);
				rot.x = 0;
				rot.z = 0;
				_transform.rotation = rot;
				mResetCnt = 0;
				m_Rigidbody.velocity = Vector3.zero;
			}
		} else if (velocity.magnitude < 2 && mOppTime > 20f) {
			mResetCnt += deltaTime * 2f;
			if (mResetCnt > 1) {
				Vector3 pos = m_WaypointFollow.target.position;
				pos.y += 2;
				_transform.position = pos;
				_transform.rotation = Quaternion.LookRotation(m_WaypointFollow.circuit.GetRoutePoint(m_WaypointFollow.progressDistance).direction); ;
				m_Rigidbody.velocity = Vector3.zero;
				mResetCnt = 0;
			}
		} else if ((_transform.position.y < -100f) || (_transform.position.y > 100f)) {
			Vector3 pos = m_WaypointFollow.target.position;
			pos.y += 1.5f;
			_transform.position = pos;
			_transform.rotation = Quaternion.LookRotation(m_WaypointFollow.circuit.GetRoutePoint(m_WaypointFollow.progressDistance).direction); ;
			m_Rigidbody.velocity = Vector3.zero;
			mResetCnt = 0;
		}
	}

	float mBcnt = 0;
	IEnumerator<float> _Boost(float Power) {
		if (mBoostVal == 1) {
			m_Rigidbody.AddForce(_transform.forward * Power, ForceMode.Acceleration);
			if (mBcnt < mBoostTime)
				mBcnt += deltaTime * 2f;
			else {
				mBoostTime = 0;
				mBoostVal = 0;
				mBcnt = 0;
			}
		}
		BoostEffect(mBoostVal == 1);
		yield return Timing.WaitForOneFrame;
	}
	void BoostEffect(bool isShow) {
		for (int i = 0; i < BoostEffectObj.transform.childCount; i++) {
			//BoostEffectObj.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().emission.enabled = isShow;
			ParticleSystem.EmissionModule em = BoostEffectObj.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().emission;
			em.enabled = isShow;
		}
	}

	void AutoMove() {
		float desiredSpeed = 100;
		m_CarController.MaxSpeed = desiredSpeed;
		Vector3 offsetTargetPos = m_Target.position;
		m_LateralWanderSpeed = .1f;
		if (Time.time < m_AvoidOtherCarTime) {
			if (Hit > 0) {
				m_AvoidPathOffset = changeAng;
				offsetTargetPos += m_Target.right * m_AvoidPathOffset;
			}
		} else {
			offsetTargetPos += m_Target.right * ((float)Mathf.Sin(m_LateralWanderSpeed) * m_LateralWanderDistance);
		}
		m_LateralWanderSpeed += deltaTime * 0.1f;
		//offsetTargetPos += m_Target.right * (Mathf.PerlinNoise (Time.time * m_LateralWanderSpeed, m_RandomOffset) * 2 - 1) * m_LateralWanderDistance;

		float accelBrakeSensitivity = (desiredSpeed < m_CarController.CurrentSpeed) ? m_BrakeSensitivity : m_AccelSensitivity;
		float accel = Mathf.Clamp((desiredSpeed - m_CarController.CurrentSpeed) * accelBrakeSensitivity, -1, 1);
		accel *= (1 - m_AccelWanderAmount) + (Mathf.PerlinNoise(Time.time * m_AccelWanderSpeed, m_RandomAcc) * m_AccelWanderAmount);
		Vector3 localTarget = _transform.InverseTransformPoint(offsetTargetPos);
		float targetAngle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
		float steer = Mathf.Clamp(targetAngle * m_SteerSensitivity, -1, 1) * Mathf.Sign(m_CarController.CurrentSpeed);
		m_CarController.Move(steer, 0, 0, 0);
	}
	Vector3 jumpforce;
	private void OnCollisionEnter(Collision col) {
		if (col.gameObject.CompareTag(Constants.RAILING_TAG) && UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
			mPerfectTime = 0;
		}
	}
	private void OnCollisionStay(Collision col) {
		if (col.rigidbody != null) {
			var otherAI = col.rigidbody.GetComponent<CarAIControl>();
			if (otherAI != null) {
				isTakker = true;
				m_AvoidOtherCarTime = Time.time + 1;
				if (Vector3.Angle(_transform.forward, otherAI.transform.position - _transform.position) < 90) {
					m_AvoidOtherCarSlowdown = 0.5f;
					//	Debug.Log("innnnnnnnn");
				} else {
					mPerfectTime = 0;
					m_AvoidOtherCarSlowdown = 1;
				}
				var otherCarLocalDelta = _transform.InverseTransformPoint(otherAI.transform.position);
				float otherCarAngle = Mathf.Atan2(otherCarLocalDelta.x, otherCarLocalDelta.z);
				m_AvoidPathOffset = (m_LateralWanderDistance) - Mathf.Sign(otherCarAngle);
				mPerfectTime = 0;
			}
			if (col.gameObject.CompareTag(Constants.PLAYER_TAG)) {
				return; //TODO
				//Debug.Log(gameObject.name + " >>> Collided with : >>>>>>>" + col.gameObject.tag);
				var PlayerCar = col.rigidbody.GetComponent<CarUserControl>();
				m_AvoidOtherCarTime = Time.time + 1;
				if (Vector3.Angle(_transform.forward, PlayerCar.transform.position - _transform.position) < 90) {
					m_AvoidOtherCarSlowdown = 0.5f;
				} else {
					m_AvoidOtherCarSlowdown = 1;
				}
				var otherCarLocalDelta = _transform.InverseTransformPoint(PlayerCar.transform.position);
				float otherCarAngle = Mathf.Atan2(otherCarLocalDelta.x, otherCarLocalDelta.z);
				m_AvoidPathOffset = (m_LateralWanderDistance) - Mathf.Sign(otherCarAngle);
				mPerfectTime = 0;
			}
		}
	}
	void OnCollisionExit(Collision col) {
		isTakker = false;
	}
	public void SetTarget(Transform target) {
		m_Target = target;
		m_Driving = true;
	}
	void OnTriggerEnter(Collider col) {
		if (col.CompareTag(Constants.LAP_TAG) && !isRaceComp && GameModeManager.GetCurrentGameMode != GameModes.Elimination) {
			currentLap++;
			if (currentLap >= CarUserControl.mTotalLap) {
				m_Driving = false;
				changeAng = 0;
				Hit = 0;
				isRaceComp = true;
				mOppTime = Time.time - CarUserControl.StartTime;
				mOppRank = ++GameController.mFinalRank;
				GameController.mRankArray[GameController.mCnt] = mOppRank;
				GameController.mTimeArray[GameController.mCnt] = mOppTime;
				GameController.mCnt++;
			}
		}
	}

	float changeAng;
	void Sensor2() {
		float dist = 35f;
		Vector3 pos;
		Hit = 0;
		var rightAng = Quaternion.AngleAxis(mFsensorAng, _transform.up) * _transform.forward;
		var leftAng = Quaternion.AngleAxis(-mFsensorAng, _transform.up) * _transform.forward;
		pos = _transform.position;
		pos += _transform.forward * mSensorPoint;
		//Straight Right
		pos += _transform.right * mSensorSidePoint;
		if (Hit == 0 && Physics.Raycast(pos, _transform.forward, out RaycastHit hit, mSensorLen)) {
			if (hit.transform.name == "Collider") {
					Hit = 1;
					changeAng = -dist;
					//Debug.Log ("  Straight Right ");
					//Debug.Log("Innnnnnnnnnn      Straight Right"+hit.transform.gameObject.name);
					//Debug.DrawLine(pos, hit.point, Color.yellow);
			}
		}
		//FrontRightAng
		else
			if (Hit == 0 && Physics.Raycast(pos, rightAng, out hit, mSensorLen / 2)) {
			if (hit.transform.name == "Collider") {
					Hit = 1;
					changeAng = -dist;
					//Debug.Log("Innnnnnnnnnn      Straight Right Angleee");
					//Debug.DrawLine(pos, hit.point, Color.yellow);			
			}
		}
		//Straight Left
		pos = _transform.position;
		pos += _transform.forward * mSensorPoint;
		pos -= _transform.right * mSensorSidePoint;
		if (Hit == 0 && Physics.Raycast(pos, _transform.forward, out hit, mSensorLen)) {
			if (hit.transform.name == "Collider") {
					Hit = 1;
					changeAng = dist;
					//Debug.Log("Avoidddddd Straight Left");
					//Debug.DrawLine(pos, hit.point, Color.yellow);
				}
		}
		//FrontLeftAng
		else
			if (Hit == 0 && Physics.Raycast(pos, leftAng, out hit, mSensorLen / 2)) {
			if (hit.transform.name == "Collider") {
					//Debug.Log ("  Straight      FrontLeftAng    ");
					Hit = 1;
					changeAng = dist;
					//Debug.DrawLine(pos, hit.point, Color.yellow);			
			}
		}
		pos = _transform.position;
		pos += _transform.forward * mSensorPoint;
		//Center
		if (Hit == 0) {
			if (Physics.Raycast(pos, _transform.forward, out hit, mSensorLen)) {
				if (hit.transform.name == "Collider") {
						Hit = 1;
						changeAng = hit.normal.x < 0 ? dist : -dist;
						//Debug.Log("Avoidddddd Center");
					}
				//Debug.DrawLine(pos, hit.point, Color.yellow);
			}
		}
		if (Hit > 0) {
			Debug.Log("       innnnnnnnnn ");
			m_AvoidOtherCarTime = Time.time + 1f;
			//m_AvoidOtherCarSlowdown = .5f;
			//Debug.Log("Innnnnnn changeee");
		}
	}

	public bool jump;
	IEnumerator<float> _ApplyJumpForce(float mSteer) {
		if (jump) {
			float power = ((360f / oppSpeed) * 1600f) * deltaTime;
			if (oppSpeed < 70) {
				power = 0;
			}
			switch (mActionType) {
				case 0:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer * 2, power * 0.7f, power * 0.9f), ForceMode.Acceleration);
					break;
				case 1:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer * 2, power * 0.3f, power * 0.5f), ForceMode.Acceleration);
					break;
				case 2:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer * 2, power * 2f, power * 1.5f), ForceMode.Acceleration);
					m_Rigidbody.AddRelativeTorque(new Vector3(0, 0, 500 * deltaTime * 100), ForceMode.Acceleration);
					break;
				case 3:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer, power * 2f, power * 1.5f), ForceMode.Acceleration);
					m_Rigidbody.AddRelativeTorque(new Vector3(0, 0, -500 * deltaTime * 100), ForceMode.Acceleration);
					break;
			}
			yield return Timing.WaitForSeconds(2f);
			jump = false;
			if (!jump) {
				mBoostVal = 0;
				mBoostPower = 0;
				mBoostTime = 0;
			}
		}
	}
	int Cnt = 0;
	void ApplyPower(float power) {
		if (!jump) {
			m_Rigidbody.AddForce(_transform.forward * power, ForceMode.Acceleration);
		}
		Cnt++;
	}

	void HideShadow() {
		isShadow = false;
		if (Physics.Raycast(_transform.position, Vector3.down, out hit, 2)) {
			if (hit.transform.CompareTag(Constants.ROAD_TAG) 
				|| hit.transform.CompareTag(Constants.RAILING_TAG)) {
				Debug.DrawLine (_transform.position, hit.point, Color.yellow);
				isShadow = true;
			}
		}
		if (((360 - _transform.rotation.eulerAngles.x) > 10 && (360 - _transform.rotation.eulerAngles.x) < 355)
			|| ((360 - _transform.rotation.eulerAngles.z) > 30 && (360 - _transform.rotation.eulerAngles.z) < 330)) {
			isShadow = false;
		}
	}
}
