﻿using UnityEngine;
using GameServices;
using GameServices.UserData;
public class GameCamera : MonoBehaviour {

	public static GameCamera Instance;

    #region Variables
	public Transform car;
	public float distance = 5.5f;
	public float height = 1.5f;
	public float rotationDamping = 10.0f;
	public float heightDamping = 10.0f;
	public float zoomratio = 0.2f;

	private float deafaultFOV = 60.0f;
	private float CameraAng = 0.0f;
	private Vector3 rotationVactor;
	private Rigidbody carRigidbody;
	private CarController carController;
	private Camera mCamera;
	private int mTargetFrameRate = 60;
	public GameObject MiniCamera;
	private readonly float[] dist = { 6.5f, 4.5f, -1.0f };
	private float jumpDist = 7.5f;
	private float multiplyRotaionEffet = 0.3f;
	private float cameraZRotationDump = 2.0f;    //how slowly camera z pos return to 0; higher the value >> slower the transition
	private float cameraZRotationLimit = 15.0f;  //max rotaion in z
	private float zRotation;
	private float acc;
	private float CurrentAngleY;
	private float wantedangleY;
	private float wantedHeight;
	private float currentHeight;
	private float currentRotationAngle;
	private Quaternion currentRotation;
	private int rotaionLimit = 70;
	private int accelarationLimit = 70;
	private float WaitTime = 10;
	private float Ang;
	private float Height = 1;
	private float cnt = 0;
    #endregion

    void Awake() {
		if(Instance == null) {
			Instance = this;
		}
	}

	void Start() {
		car = car.GetChild(CarManager.GetSelectedCar().GetCarID()).transform;
		carRigidbody = car.GetComponent<Rigidbody>();
		carController = car.GetComponent<CarController>();
		mCamera = GetComponent<Camera>();
		Ang = Random.Range(-180, 180) + 30;
		distance = dist[UserDataManager.GetCameraZoomLevel()];
		height = 1;
		rotationDamping = 5f;
		heightDamping = 10f;
		zoomratio = 0.3f;
		CameraAng = 5;
	}

	float xRotation;
	void LateUpdate() {
		if (UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
			if (GameManager.isInTutorial && TutorialManager.isTutorialPaused) {
				return; 
			}
			wantedangleY = car.eulerAngles.y;
			wantedHeight = car.position.y + height;
			CurrentAngleY = Mathf.LerpAngle(transform.eulerAngles.y, wantedangleY, rotationDamping * Time.deltaTime);
			currentHeight = Mathf.Lerp(transform.position.y, wantedHeight, heightDamping * Time.deltaTime);
			currentRotation = Quaternion.Euler(0, CurrentAngleY, 0);
			if (CarUserControl.isSky) {
				distance = Mathf.Lerp(distance, jumpDist, Time.deltaTime * 5f);
			} else {
				distance = Mathf.Lerp(distance, dist[UserDataManager.GetCameraZoomLevel()], Time.deltaTime * 5f);
			}
			transform.position = car.position;
			transform.position -= currentRotation * Vector3.forward * distance;
			transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

			zRotation = Mathf.Lerp(zRotation,
					CarUserControl.mSteer.x * carController.CurrentSpeed * multiplyRotaionEffet,
					Time.deltaTime * cameraZRotationDump);
			
			if (Mathf.Abs(zRotation) > rotaionLimit) {
				zRotation = Mathf.Sign(zRotation) * rotaionLimit;
			}
			//Debug.Log("rot >>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + rot);
			acc = carRigidbody.velocity.magnitude;
			if (acc > accelarationLimit) {
				acc = accelarationLimit;
			}
			zRotation = Mathf.Clamp(zRotation, -cameraZRotationLimit, cameraZRotationLimit);
			//Debug.Log("accelarationLimit:       " + accelarationLimit);
			switch (InputManager.GetCurrentController()) {
				case ControllerTypes.Touch:
					zRotation /= 1.5f;
					break;
				case ControllerTypes.Remote:
					zRotation /= 1.75f;
					break;
			}
            switch (InputManager.GetCurrentPlatform()) {
                case BuildPlatforms.AmazonTab:
				case BuildPlatforms.Android:
				case BuildPlatforms.IOS:
					xRotation = Random.Range(acc * 0.007f, acc * 0.015f);
					break;
                case BuildPlatforms.AmazonTv:
					xRotation = 0;
                    break;
            }
			transform.rotation = Quaternion.Euler(xRotation + CameraAng, CurrentAngleY, zRotation);
        }
        if (UIManager.CurrentScreen == GameScreen.GameCompleteScreen 
			|| UIManager.CurrentScreen == GameScreen.GameOverScreen 
			|| UIManager.CurrentScreen == GameScreen.EliminationScreen) {
			wantedHeight = car.position.y + height + Height;
			currentRotationAngle = Mathf.LerpAngle(transform.eulerAngles.y, car.eulerAngles.y + Ang, 2 * Time.deltaTime);
			currentHeight = Mathf.Lerp(transform.position.y, wantedHeight, heightDamping * Time.deltaTime);
			currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
			transform.position = car.position;
			transform.position -= currentRotation * Vector3.forward * dist[0];
			transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
			transform.LookAt(car);
			if (UIManager.CurrentScreen == GameScreen.GameCompleteScreen) {
				ChangeView();
			}
		}
    }
    void Update() {
		if (Navigation.IsInGameScene()) {
			MiniCamera.SetActive(UIManager.CurrentScreen == GameScreen.GamePlayScreen); 
		} else if(Navigation.IsInTutorialScene()) {
			MiniCamera.SetActive(false);
		}
		if (Application.targetFrameRate != mTargetFrameRate) {
			Application.targetFrameRate = mTargetFrameRate;
		}
		float acc = carRigidbody.velocity.magnitude;
		Vector3 localvelocity = car.transform.InverseTransformDirection(carRigidbody.velocity);
		rotationVactor.x = car.eulerAngles.x;
		if (localvelocity.z < -1.5f) {
			rotationVactor.y = car.eulerAngles.y + 180;
		} else {
			rotationVactor.y = car.eulerAngles.y;
		}
		if (CarUserControl.isBoost || CarUserControl.isSky || CarUserControl.isDrift) {
			zoomratio = Mathf.Lerp(zoomratio, 0.3f, Time.deltaTime);
		} else {
			zoomratio = Mathf.Lerp(zoomratio, 0f, Time.deltaTime);
		}
		mCamera.fieldOfView = deafaultFOV + acc * zoomratio;
	}

	void ChangeView() {
		cnt += Time.deltaTime * 3f;
		if (cnt > WaitTime) {
			Ang += Random.Range(-180, 180) + 30;
			Height = Random.Range(-1f, 8f);
			cnt = 0;
		}
	}

	public void ChangeCameraView() {
		int zoomLevel = UserDataManager.GetCameraZoomLevel();
		zoomLevel += 1;
		if (zoomLevel >= dist.Length) {
			zoomLevel = 0;
		}
		UserDataManager.SaveCameraZoomLevel(zoomLevel);
	}
}