﻿using UnityEngine;

public class RotateWind : MonoBehaviour {

	void Update() {
		transform.Rotate(Vector3.left * Time.deltaTime * 20);
	}
}
