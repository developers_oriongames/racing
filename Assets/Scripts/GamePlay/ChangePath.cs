﻿using UnityEngine;

public class ChangePath : MonoBehaviour {

	[SerializeField] int mNode = 0;
	[SerializeField] GameObject mPath = null;

	void OnTriggerEnter(Collider other) {
		if (other.transform.root.name == Constants.PLAYER_NAME || other.transform.root.name == Constants.RIVAL_CAR_NAME) {
			if (other.transform.parent.parent.GetComponent<WaypointFollow>().getCurrentPath().name != transform.name) {
				other.transform.parent.parent.GetComponent<WaypointFollow>().ChangePath(mPath, mNode);
			}
		}
	}
}
