﻿using UnityEngine;
using GameServices;
using System.Collections.Generic;
using MEC;

public class GameController : MonoBehaviour {

	public static GameController Instance;

	public GameObject mPlayer;
	public GameObject Opponent;
	public GameObject startLine;
	public GameObject startBarrier;
	public GameObject finalLine;

	[HideInInspector] public CarUserControl carUserControl;

	float showtime = 0, mCounter = 0;
	float[] Dist;
	public int mTotalPlayer;
	public static int mFinalRank = 0;
	int mEliminationTime = 0;
	bool isOnce = false;

	//Game Over
	public static float[] mTimeArray = new float[5];
	public static int[] mRankArray = new int[5];
	public static int mCnt = 0;
	public static int totalPlayer = 5;
	public static int tmpmoney = 0;
	public static int noStar = 0;

	void Awake() {
		if (Instance == null) {
			Instance = this;
		}
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).gameObject.SetActive(false);
		}
		mPlayer = transform.GetChild(CarManager.GetSelectedCar().GetCarID()).gameObject;
		mPlayer.gameObject.SetActive(true);
		if (GameModeManager.GetCurrentGameMode != GameModes.Versus) {
			mTotalPlayer = 5;
		} else {
			mTotalPlayer = 2;
		}
		Dist = new float[mTotalPlayer];
		mFinalRank = 0;
		mCounter = GameManager.isInTutorial ? 100 : 0;
		mCnt = 0;
		carUserControl = mPlayer.GetComponent<CarUserControl>();
	}

	void Start() {
		showtime = 0;
		isOnce = false;
		mEliminationTime = (int)Time.time; 
		PlayBgSound();
		if (!GameManager.isInTutorial) { AudioManager.PlayGamePlaySound(GamePlaySoundType.Start); }
		if (GameModeManager.GetCurrentGameMode == GameModes.Elimination) {
			finalLine.SetActive(false);
        } else {
			finalLine.SetActive(true);
		}
	}

    float mTime = 1;

    void Update()
    {
        if (AudioManager.musicVolume > 0f && (UIManager.CurrentScreen == GameScreen.GameLoading))
        {
            AudioManager.StopMusic(); //TODO why here?
        }

        if (UIManager.CurrentScreen == GameScreen.GamePlayScreen)
        {
            AudioManager.PlayCarSound();
            PlayBgSound();
            ChangePitch();
            if (GameModeManager.GetCurrentGameMode == GameModes.Elimination)
            {
                if (GameManager.isInTutorial)
                {
                    return;
                }

                if (!isOnce)
                {
                    mEliminationTime = 20 + (int) Time.time;
                    isOnce = true;
                }

                mTime = Mathf.Abs(mEliminationTime - (int) Time.time);
                string timeInSeconds = (mTime % 60).ToString("f1");
                UIManager.Instance.uIGame.gamePlayPanel.SetEliminationText(timeInSeconds);
                if (mTime < 12)
                {
                    AudioManager.PlayGamePlaySound(GamePlaySoundType.EliminateBeep);
                }

                if (transform.GetChild(CarManager.GetSelectedCar().GetCarID()).GetComponent<CarUserControl>()
                    .mPlrRank == 1 && mTotalPlayer == 1)
                {
                    if (UIManager.CurrentScreen != GameScreen.GameCompleteScreen)
                    {
                        AudioManager.StopMusic();
                        AudioManager.StopCarSound();
                        AudioManager.PlayGamePlaySound(GamePlaySoundType.Finish);
                        UIManager.CurrentScreen = GameScreen.GameCompleteScreen;
                        GameEvents.OnGameCompleteScreen();
                        PlayBgSound();
                        CarUserControl.isBoost = CarUserControl.isDrift = false;
                        GameManager.LevelComplete(); //TODO add funcionality to this
                    }
                }

                UIManager.Instance.uIGame.gamePlayPanel.SetPlayerOutText(yougone
                    ? Constants.YOU_ARE_ELIMINITED
                    : (Constants.PLAYER_ + (mTotalPlayer + 1) + Constants._ELIMINITED));
                UIManager.Instance.uIGame.gamePlayPanel.SetPlayerOutTextStatus(showtime >= 1 && showtime < 5);
                if (UIManager.Instance.uIGame.gamePlayPanel.IsPlayerOutTextActive())
                {
                    //print("111111111111111111");
                    //UIManager.Instance.uIGame.gamePlayPanel.SetCrossFadeAlpha(0.01f, 1);
                }

                if (showtime > 0)
                {
                    showtime += Time.deltaTime * 2;
                    if (showtime > 5)
                    {
                        showtime = 0;
                        //print("222222222222222222");
                        //UIManager.Instance.uIGame.gamePlayPanel.SetCrossFadeAlpha(1, 1);
                    }
                }
            }

            UpdateRank();
            //if (GameModeManager.GetCurrentGameMode == GameModes.Versus 
            //	|| GameModeManager.GetCurrentGameMode == GameModes.Classic) {
            //mFinalLine.SetActive(mCounter > 100);  //Auto enables final line
            startLine.SetActive(GameManager.isInTutorial ? mCounter < 200 : mCounter < 50);

            mCounter += Time.deltaTime * 2f;
            //}
        }
        startBarrier.SetActive(GameManager.isInGamePlay);
    }

    public void SetGameOver() {
		tmpmoney = 0;
		UIManager.Instance.uIGame.gameOverPanel.Set();
		switch (GameModeManager.GetCurrentGameMode) {
			case GameModes.Versus:
				noStar = carUserControl.mPlrRank == 1 ? 5 : 1;
				break;
			default:
				noStar = 5 - (carUserControl.mPlrRank - 1);
				break;
		}

		if (LevelManager.LevStar[LevelManager.selectedLevel/*currentSelectedLevelNo*/] < noStar) {
			LevelManager.LevStar[LevelManager.selectedLevel/*currentSelectedLevelNo*/] = noStar;
		}

		switch (GameModeManager.GetCurrentGameMode) {
			case GameModes.Versus:
				totalPlayer = 2;
				break;
			default:
				totalPlayer = 5;
				break;
		}

		for (int i = 0; i < totalPlayer; i++) {
			if (GameModeManager.GetCurrentGameMode == GameModes.Elimination) {
                mTimeArray[i] = 125f - (i * 25.0f + i * 0.01f); 
				if (i + 1 == carUserControl.mPlrRank) {
					mRankArray[i] = carUserControl.mPlrRank;
				} else {
					mRankArray[i] = i + 1;
				}
			} else {
				if (mTimeArray[i] == 0) {
					mTimeArray[i] = carUserControl.mPlayerTime + (2f + i + Random.Range(0.1f, 4f));
					mRankArray[i] = i + 1;
				}
			}
		}

		if (carUserControl.mPlrRank > 0) {
			float rnd = Random.value * 1000 + 2;
			int amount = 8000 / (carUserControl.mPlrRank) + (int)rnd;
			CurrencyManager.GiveMoney(amount);
		}
		AudioManager.StopMusic();
		LevelManager.SaveLevelData();
	}

	bool yougone;
	void UpdateRank() {
		if (UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
			for (int i = 0; i < Dist.Length; i++) {
				if (i < Dist.Length - 1) {
					Dist[i] = Opponent.transform.GetChild(i).GetComponent<WaypointFollow>().mRankDistance;
				} else {
					Dist[i] = mPlayer.transform.GetComponent<WaypointFollow>().mRankDistance;
				}
			}
			for (int i = 0; i < Dist.Length; i++) {
				for (int j = i + 1; j < Dist.Length; j++) {
					if (Dist[i] > Dist[j]) {
						float a = Dist[i];
						Dist[i] = Dist[j];
						Dist[j] = a;
					}
				}
			}
			for (int i = 0; i < Dist.Length; i++) {
				for (int j = 0; j < Dist.Length; j++) {
					if (Dist[i] == Opponent.transform.GetChild(j % Opponent.transform.childCount).GetComponent<WaypointFollow>().mRankDistance) {
						Opponent.transform.GetChild(j % Opponent.transform.childCount).GetComponent<CarAIControl>().mOppRank = i + 1;
						if (GameModeManager.GetCurrentGameMode == GameModes.Elimination) {
							if (mTime < 1 && Opponent.transform.GetChild(j % Opponent.transform.childCount).GetComponent<CarAIControl>().mOppRank == mTotalPlayer) {
								Opponent.transform.GetChild(j % Opponent.transform.childCount).gameObject.SetActive(false);
								if (mTotalPlayer > 0) {
									mTotalPlayer--;
									UIManager.Instance.uIGame.gamePlayPanel.SetCrossFadeAlpha(1, 0.2f);
									showtime = 1;
									yougone = false;
									mEliminationTime = 20 + (int)Time.time;
									AudioManager.PlayGamePlaySound(GamePlaySoundType.Eliminate);
								}
							}
						}
					}
					if (Dist[i] == mPlayer.transform.GetComponent<WaypointFollow>().mRankDistance) {
						transform.GetChild(CarManager.GetSelectedCar().GetCarID()).GetComponent<CarUserControl>().mPlrRank = i + 1;
						if (GameModeManager.GetCurrentGameMode == GameModes.Elimination) {
							if (mTime < 1 && transform.GetChild(CarManager.GetSelectedCar().GetCarID()).GetComponent<CarUserControl>().mPlrRank == mTotalPlayer) {
								AudioManager.PlayGamePlaySound(GamePlaySoundType.Eliminate);
								//UIManager.CurrentScreen = GameScreen.EliminationScreen;
								//GameEvents.OnEliminationScreen();
								CarUserControl.isBoost = CarUserControl.isDrift = false;
								yougone = true;
								UIManager.Instance.uIGame.gamePlayPanel.SetCrossFadeAlpha(1, 0.2f);
								showtime = 1;
								AudioManager.StopCarSound();
								Timing.RunCoroutine(_SwitchToEliminationScreen());
							}
						}
					}
				}
			}
		}
	}

	IEnumerator<float> _SwitchToEliminationScreen() {
		yield return Timing.WaitForSeconds(2);
		UIManager.CurrentScreen = GameScreen.EliminationScreen;
		GameEvents.OnEliminationScreen();
	}

	float pitch;
	void ChangePitch() {
		float fac = transform.GetChild(CarManager.GetSelectedCar().GetCarID()).GetComponent<CarController>().CurrentSpeed
					/ transform.GetChild(CarManager.GetSelectedCar().GetCarID()).GetComponent<CarController>().MaxSpeed;
		pitch = Mathf.Lerp(pitch, fac + 0.2f, Time.deltaTime);	
		AudioManager.ChangeCarSoundPitch(pitch);
	}

	public void PlayBgSound() {
		if (UIManager.CurrentScreen == GameScreen.GameCompleteScreen 
			|| UIManager.CurrentScreen == GameScreen.GameOverScreen) {
			AudioManager.PlayLevelCompleteMusic(); 
		} else { 
			AudioManager.PlayGamePlayMusic(); 
		}
	}

	static int minutes, seconds, milliseconds;

    public static string FormatTime(float time)
    {
        minutes = (int) time / 60;
        seconds = (int) time - 60 * minutes;
        milliseconds = (int) (1000 * (time - minutes * 60 - seconds));
        return string.Format("{0:00} :{1:00} :{2:000}", minutes, seconds, milliseconds);
    }

    public void ResetCar()
    {
		mPlayer.transform.GetComponent<CarUserControl>().ResetCar();
		//UIManager.Instance.uIGame.gamePlayPanel.SetResetButtonInteractive(false);
    }
}