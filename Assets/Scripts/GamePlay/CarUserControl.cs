using System;
using UnityEngine;
using System.Collections.Generic;
using GameServices;
using MEC;

public class CarUserControl : MonoBehaviour {

	int currentLap, mWrongDirGo;
	public int mSlowMotion, mPlrRank;
	static public int mTotalLap;
	public static float Forward = 1, StartTime;
	public float boostVal;
	float boostInc;
	public float playerSpeed;
    public static bool isBoost, jump, isSky, isDrift;

	public float mPlayerTime;
	GameObject BoostEffectObj, mFireWork;
	CarController carController;
	WaypointFollow waypointFollow;
    private Rigidbody m_Rigidbody;
	bool isShadow = true;
	static public float Power;
	float mBoostSpd, mNormalSpd;
	float mPerfectTime;

	public static Vector3 mSteer = Vector3.zero;
	public static float steerDirection;

	float inputMultiply;
	float FlipCnt;

	Vector3 ForceVector = Vector3.zero;
	byte colval;

	int counter, counter1;
	float mResetCnt;
	Vector3 velocity;

	float dir;
	float mDriftime;
	int mDriftCnt;

	bool rewardAdForBoostActive;

    void OnEnable() {
		GameEvents.OnMouseDown += OnMousePressed;
		GameEvents.OnMouseUp += OnMouseReleased;

		GameEvents.OnInputAcceleration += OnAcceleration;
		GameEvents.OnInputHorizontalAxis += OnHorizontalAxis;
		
		GameEvents.OnKeyUpA += OnResetDirection;
		GameEvents.OnKeyUpD += OnResetDirection;
		GameEvents.OnKeyA += OnSetDirectionLeft;
		GameEvents.OnKeyD += OnSetDirectionRight;

		GameEvents.OnInputMenu += OnMenuPressed;

        GameEvents.OnInputEndUp += OnJoystick0Pressed;

		GameEvents.OnInputLeft += OnLeftPressed;
		GameEvents.OnInputRight += OnRightPressed;
		GameEvents.OnInputEndLeft += OnEndLeftOrRightPressed;
		GameEvents.OnInputEndRight += OnEndLeftOrRightPressed;

		GameEvents.OnInputDown += OnDownPressed;
		GameEvents.OnInputEndDown += OnEndDownPressed;
	}

    void OnDisable() {
		GameEvents.OnMouseDown -= OnMousePressed;
		GameEvents.OnMouseUp -= OnMouseReleased;

		GameEvents.OnInputAcceleration -= OnAcceleration;
		GameEvents.OnInputHorizontalAxis -= OnHorizontalAxis;

		GameEvents.OnKeyUpA -= OnResetDirection;
		GameEvents.OnKeyUpD -= OnResetDirection;
		GameEvents.OnKeyA -= OnSetDirectionLeft;
		GameEvents.OnKeyD -= OnSetDirectionRight;

		GameEvents.OnInputMenu -= OnMenuPressed;
	
		GameEvents.OnInputEndUp -= OnJoystick0Pressed;

		GameEvents.OnInputLeft -= OnLeftPressed;
		GameEvents.OnInputRight -= OnRightPressed;
		GameEvents.OnInputEndLeft -= OnEndLeftOrRightPressed;
		GameEvents.OnInputEndRight -= OnEndLeftOrRightPressed;

		GameEvents.OnInputDown -= OnDownPressed;
		GameEvents.OnInputEndDown -= OnEndDownPressed;
	}

	void Start() {
		InputManager.CheckForDefaultController();
		carController = GetComponent<CarController>();
		waypointFollow = GetComponent<WaypointFollow>();
        m_Rigidbody = GetComponent<Rigidbody>();
		currentLap = 0;
		StartTime = Time.time;
		mPlrRank = 0;
		boostInc = -1;
		Forward = 1;
		boostVal = 0.2f;
		mSlowMotion = 0;
		mFireWork = gameObject.transform.GetChild(7).gameObject;
		mFireWork.SetActive(false);
		BoostEffectObj = gameObject.transform.GetChild(5).gameObject;
		BoostEffect(false);
		mTotalLap = 1;
		FlipCnt = 0;
		SetProperty();
		isShadow = true;
		isDrift = false;
		isBoost = false;
		isSky = false;
		transform.GetChild(transform.childCount - 1).localPosition = new Vector3(0, 50, 0);
		mPerfectTime = 0;
		mDriftCnt = 0;

		rewardAdForBoostActive = true;
	}

	void InputCalculation() {
		//if the value of 'inputMultiply' too large, car do auto turning on touch input
		inputMultiply = Mathf.Clamp(1.1f + (CarManager.GetSelectedCar().GetCarID() + 1) * 0.045f, 1.145f, 1.6f);
		switch (InputManager.GetCurrentPlatform()) {
			case BuildPlatforms.None:
				break;
			case BuildPlatforms.AmazonTab:
			case BuildPlatforms.Android:
			case BuildPlatforms.IOS:
				switch (InputManager.GetCurrentController()) {
					case ControllerTypes.Sensor:
#if UNITY_EDITOR
						mSteer.x = horizontalInput * inputMultiply;
#elif UNITY_ANDROID
						mSteer  = accelerationInput * inputMultiply;
#endif
						break;
					case ControllerTypes.Touch:
#if UNITY_EDITOR
						mSteer.x = (Mathf.Lerp(mSteer.x, steerDirection, 0.2f)) * inputMultiply;
#elif UNITY_ANDROID
						mSteer.x = (Mathf.Lerp(mSteer.x, steerDirection, 0.2f)) * inputMultiply;
#endif
						break;
					default:
						if (GameManager.isInGamePlay) {
							Debug.LogError("No Controller Found!");
						}
						break;
				}
				break;
			case BuildPlatforms.AmazonTv:
				switch (InputManager.GetCurrentController()) {
					case ControllerTypes.Remote:
						mSteer.x = (Mathf.Lerp(mSteer.x, steerDirection, 0.2f)) * inputMultiply;
						break;
					default:
						if (GameManager.isInGamePlay) {
							Debug.LogError("No Controller Found!");
						}
						break;
				}
				break;
			default:
				break;
		}
	}

	void FixedUpdate() {
		if (UIManager.CurrentScreen == GameScreen.GamePlayScreen) {			
			InputCalculation();		
			if (mSteer.x > 1) {
				mSteer.Normalize();
			}
			float dx = 0.7f;
			mSteer.x = Mathf.Clamp(mSteer.x, -dx, dx);
			mSteer.x *= dx;
			if (isDrift) {
				carController.Move(mSteer.x, Math.Abs(Forward), 0, 0);
				DriftCar(mSteer.x);
			} else {
				carController.Move(mSteer.x, Forward, Forward, 0);  //FWD-REV
				if (isShadow) {
					AddPower(Forward);
				}
				//transform.GetComponent<CarController> ().UpdateSpring ();
				Timing.RunCoroutine(_Boost(), Segment.FixedUpdate);
				Timing.RunCoroutine(_ApplyJumpForce(), Segment.FixedUpdate);
				Timing.RunCoroutine(_ApplyCollisionForce(), Segment.FixedUpdate);
			}
		}
		if (UIManager.CurrentScreen == GameScreen.GameCompleteScreen 
			|| UIManager.CurrentScreen == GameScreen.GameOverScreen) {			
				AddPower(0.1f);
				AutoMove();			
		}
		if (UIManager.CurrentScreen == GameScreen.EliminationScreen) {
			carController.Move(0, 0, 0, 1);
			if (FlipCnt < 2.5) {
				m_Rigidbody.AddTorque(transform.up * 50, ForceMode.Acceleration);
				FlipCnt += Time.deltaTime * 2f;
			}
		}
	}
	void EngageBrake() {
        if (GameManager.isInPause) {
			return;
        }
		if (InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv) {
			UIManager.Instance.uIGame.gamePlayPanel.EnableBreakImage(); 
		}
		Brake(-1);
	}
	void ReleaseBrake() {
		Brake(1);
		if (InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv) { 
			UIManager.Instance.uIGame.gamePlayPanel.DisableBreakAndBoostImages();
		}
	}
	void EngageBoost() {
		if (InputManager.GetCurrentPlatform() != BuildPlatforms.AmazonTv) { 
			UIManager.Instance.uIGame.gamePlayPanel.EnableBoostImage();
		}
		if (boostVal > 0) {
			isDrift = false;
			isBoost = !isBoost;
			boostInc = -1;
			BoostEffect(true);
			if (isBoost) {
				AudioManager.PlayGamePlaySound(GamePlaySoundType.Boost);
			}
		}
	}

	#region Input Listeners

	#region Sensor Controller events
	float horizontalInput;
	/// <summary>
	/// ----->>> For Testing Sensor Input <<<------ 
	/// Listens to Input Horizontal Axis
	/// </summary>
	/// <param name="axisValue"></param>
	private void OnHorizontalAxis(float axisValue) {
		horizontalInput = axisValue;
	}
	Vector3 accelerationInput;
	/// <summary>
	/// ----->>> For acctual Sensor Input <<<------ 
	/// Listens to Input Acceleration
	/// </summary>
	/// <param name="acceleration"></param>
	private void OnAcceleration(Vector3 acceleration) {
		accelerationInput = acceleration;
	}
    #endregion

    #region Touch Controller events
    /// <summary>
    /// ----->>> For Testing Touch Input <<<------
    /// Listens to A and D key up events
    /// </summary>
    void OnResetDirection() {
		steerDirection = 0;
	}
	/// <summary>
	/// ----->>> For Testing Touch Input <<<------ 
	/// Listens to D key press events
	/// </summary>
	void OnSetDirectionRight() {
		steerDirection = 1;
	}
	/// <summary>
	/// ----->>> For Testing Touch Input <<<------ 
	/// Listens to A key press events
	/// </summary>
	void OnSetDirectionLeft() {
		steerDirection = -1;
	}
    #endregion

    #region Mouse Events
    /// <summary>
    /// Listens to OnMouseDown event from input manager,
    /// actives brake or boost accorodingly
    /// </summary>
    void OnMousePressed() {
		if (IsToActiveBreak()) {
			EngageBrake();
		}
		if (IsToActiveBoost()) {
			EngageBoost();
		}
	}
	/// <summary>
	/// Listens to Input manager's OnMouseUp event,
	/// disables brake accrodingly
	/// </summary>
	void OnMouseReleased() {
		ReleaseBrake();
	}
	#endregion

	#region AmazonTv Events
	/// <summary>
	/// Listens to InputManager's Amazon TV remote's 'Menu' button press 
	/// </summary>
	void OnMenuPressed() {
		//change camera
		//GameCamera.Instance.ChangeCameraView();
		//reset car
		ResetCar();
	}
	/// <summary>
	/// Listens to InputManager's Amazon TV remote's 'Joystick 0' button press 
	/// </summary>
	void OnJoystick0Pressed() {
		if (IsToActiveBoost()) {
			EngageBoost();
		}
	}
	/// <summary>
	/// Listens to InputManager's Amazon TV remote's 'Left' button press 
	/// </summary>
	void OnLeftPressed() {
		//go left
		steerDirection = -1;
	}
	/// <summary>
	/// Listens to InputManager's Amazon TV remote's 'Right' button press 
	/// </summary>
	void OnRightPressed() {
		//go right
		steerDirection = +1;
	}
	/// <summary>
	/// Listens to InputManager's Amazon TV remote's 'Left' or Right button press end
	/// </summary>
	void OnEndLeftOrRightPressed() {
		//stop go left or right
		steerDirection = 0;
	}
	/// <summary>
	/// Listens to InputManager's Amazon TV remote's 'Down' button press 
	/// </summary>
	void OnDownPressed() {
		if (IsToActiveBreak()) {
			EngageBrake();
		}
	}
	/// <summary>
	/// Listens to InputManager's Amazon TV remote's 'Down' button press end
	/// </summary>
	void OnEndDownPressed() {
		ReleaseBrake();
	}
	#endregion

	#endregion

	bool isValidInputForBreak;
	bool IsToActiveBreak() {
        switch (InputManager.GetCurrentPlatform()) {
            case BuildPlatforms.AmazonTab:
            case BuildPlatforms.Android:
            case BuildPlatforms.IOS:
				isValidInputForBreak = InputManager.IsInLeftHalfOfTheScreen();
				break;
			case BuildPlatforms.AmazonTv:
				isValidInputForBreak = true;
				break;
		}	
		if (GameManager.isInTutorial) {
			return isValidInputForBreak && !TutorialManager.isTutorialPaused && TutorialManager.isBreakFeatureShowed;
		}
		return isValidInputForBreak;
	}

	bool isValidInputForBoost;
	bool IsToActiveBoost() {
		switch (InputManager.GetCurrentPlatform()) {
			case BuildPlatforms.AmazonTab:
			case BuildPlatforms.Android:
			case BuildPlatforms.IOS:
				isValidInputForBoost = InputManager.IsInRightHalfOfTheScreen();
				break;
			case BuildPlatforms.AmazonTv:
				isValidInputForBoost = true;
				break;
		}
		if (GameManager.isInTutorial) {
			return isValidInputForBoost && !TutorialManager.isTutorialPaused && TutorialManager.isBoostFeatureShowed;
		}
		return isValidInputForBoost;
	}

	float _time;
	string posString;
	string speedString;
	string lapString;
	string timeString;
	void Update() {
		HideShadow();
		transform.GetChild(8).gameObject.SetActive(isShadow);

		switch (UIManager.CurrentScreen)
        {
            case GameScreen.GamePlayScreen:
            {
                if (isBoost && boostVal > 0) {
                    transform.GetComponent<CarController>().MaxSpeed
                        = Mathf.Lerp(transform.GetComponent<CarController>().MaxSpeed,
                            mBoostSpd, Time.deltaTime * 4f);
                } else {
                    transform.GetComponent<CarController>().MaxSpeed
                        = Mathf.Lerp(transform.GetComponent<CarController>().MaxSpeed,
                            mNormalSpd, Time.deltaTime * 2f);
                }
                if (isBoost && boostVal > 0) {
                    boostVal += boostInc * (0.04f * Time.deltaTime);
                }
                if (boostVal > 1) { boostVal = 1; }
                if (boostVal < 0) { boostVal = 0; }
                if (GameManager.isInTutorial) {
                    UIManager.Instance.uITutorial.UpdateBoostMeter(boostVal);
                    UIManager.Instance.uIGame.gamePlayPanel.UpdateBoostMeter(boostVal);
                } else {
                    UIManager.Instance.uIGame.gamePlayPanel.UpdateBoostMeter(boostVal);
                }

                if (boostVal <= 0.5f && !GameManager.isInTutorial && rewardAdForBoostActive) {
                    if (!InputManager.IsInAmazonTv() && RI_Extension.AnalyticsClient.booster)
                    {
                        UIManager.Instance.uIGame.gamePlayPanel.RewardAdBoostEnable(true);
                    }
                } 
                else {
                    UIManager.Instance.uIGame.gamePlayPanel.RewardAdBoostEnable(false);
                }

                CheckAndResetCar();
                WrongDir();
			
                mPlayerTime = Time.time - StartTime;
                _time = Time.time - StartTime;
                playerSpeed = carController.CurrentSpeed;

                posString = Constants.POS_ + mPlrRank + Constants.SLASH + GameController.Instance.mTotalPlayer; //Pos
                speedString = playerSpeed.ToString("f1") + Constants._KM; //Distance
                lapString = Constants.LAP + ((currentLap > mTotalLap) ? mTotalLap : currentLap + 1) + Constants.SLASH + mTotalLap; //Lap
                timeString = GameController.FormatTime(_time); //Time

                UIManager.Instance.uIGame.gamePlayPanel.UpdateGamePlayTexts(posString, speedString, lapString, timeString);
                UIManager.Instance.uIGame.gamePlayPanel.EnableDisableEliminationImages();

                Timing.RunCoroutine(_SlowEffect());
                mPerfectTime += Time.deltaTime;
                if (mPerfectTime > 28) {
                    UIManager.Instance.uIGame.gamePlayPanel.EnablePerfectText(true);
                    if (mPerfectTime > 30) {
                        UIManager.Instance.uIGame.gamePlayPanel.EnablePerfectText(false);
                        boostVal += 0.2f;
                        AudioManager.PlayGamePlaySound(GamePlaySoundType.BoostCollect);
                        mPerfectTime = 0;
                    }
                }

                break;
            }
            case GameScreen.EliminationScreen:
            {
                //mOverCnt++;
                if (FlipCnt >= 2.5f) {
                    UIManager.Instance.uIGame.GameOverElimination();
                    AudioManager.StopMusic();
                    UIManager.CurrentScreen = GameScreen.GameOverScreen;
                    GameEvents.OnGameOverScreen();
                    BoostEffect(false);
                }

                break;
            }
            case GameScreen.GameCompleteScreen:
            case GameScreen.GameOverScreen:
            {
                GameController.Instance.PlayBgSound();
                CheckAndResetCar();
                if (mPlrRank == 1) {
                    mFireWork.SetActive(true);
                }

                break;
            }
        }
    }

	void OnCollisionEnter(Collision col) {

        if (UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
			isDrift = false;
			UIManager.Instance.uIGame.gamePlayPanel.EnablePerfectText(false);
			
			if (col.gameObject.CompareTag(Constants.OPPONENT_TAG) 
				&& UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
				mPerfectTime = 0;
				if (playerSpeed > GetComponent<CarController>().MaxSpeed * 0.85f) {
					AudioManager.PlayGamePlaySound(GamePlaySoundType.SmallCrash);
					Vector3 pos = col.contacts[0].point.normalized;
					col.gameObject.GetComponent<Rigidbody>().AddRelativeForce(Vector3.up * playerSpeed, ForceMode.Acceleration);
					col.gameObject.GetComponent<Rigidbody>().AddRelativeTorque(pos * playerSpeed * 8, ForceMode.Acceleration);
					col.gameObject.GetComponent<Rigidbody>().angularDrag = .05f;
					mSlowMotion = 1;
				}
			}

			if (col.gameObject.CompareTag(Constants.RAMP_TAG) 
				|| col.gameObject.CompareTag(Constants.SMALL_RAMP_TAG) 
				|| col.gameObject.CompareTag(Constants.TURN_RAMP_L_TAG) 
				|| col.gameObject.CompareTag(Constants.TURN_RAMP_R_TAG)) {
				mSlowMotion = 1;
				if (col.gameObject.CompareTag(Constants.RAMP_TAG)) {
                    mActionType = 0;
                }
                if (col.gameObject.CompareTag(Constants.SMALL_RAMP_TAG)) {
                    mActionType = 1;
                }
                if (col.gameObject.CompareTag(Constants.TURN_RAMP_L_TAG)) {
                    mActionType = 2;
                }
                if (col.gameObject.CompareTag(Constants.TURN_RAMP_R_TAG)) {
                    mActionType = 3;
                }
                if (!jump) {
					Debug.Log("innnnnnnnn jumppp Caruser");
					boostVal += .2f;
					jump = true;
					isSky = true;
					AudioManager.PlayGamePlaySound(GamePlaySoundType.Jump);
				}
			}
			if (col.gameObject.CompareTag(Constants.ROAD_TAG)) {
				if (isSky) {
					jump = false;
					isSky = false;
					isDrift = false;
					mActionType = 0;
				}
			}
			if (transform.root.name == Constants.RIVAL_CAR_NAME) {
				var otherAI = col.rigidbody.GetComponent<CarAIControl>();
				if (otherAI != null) {
					otherAI.m_AvoidOtherCarTime = Time.time + 1;
					if (Vector3.Angle(transform.forward, otherAI.transform.position - transform.position) < 90) {
						otherAI.m_AvoidOtherCarSlowdown = 0.5f;
                    }
                    var otherCarLocalDelta = transform.InverseTransformPoint(otherAI.transform.position);
					float otherCarAngle = Mathf.Atan2(otherCarLocalDelta.x, otherCarLocalDelta.z);
					otherAI.m_AvoidPathOffset = (6) - Mathf.Sign(otherCarAngle);
					otherAI.mPerfectTime = 0;
				}
			}
		}
	}

    void OnTriggerEnter(Collider col) {
		if (col.CompareTag(Constants.LAP_TAG) 
			&& UIManager.CurrentScreen == GameScreen.GamePlayScreen 
			&& GameModeManager.GetCurrentGameMode != GameModes.Elimination) {
			if (mWrongDirGo == 0 
				&& transform.InverseTransformDirection(m_Rigidbody.velocity).z > 0) {
				currentLap++;
				if (currentLap >= mTotalLap) {
					AudioManager.StopCarSound();
					AudioManager.StopMusic();
					UIManager.CurrentScreen = GameScreen.GameCompleteScreen;
					GameEvents.OnGameCompleteScreen();
					mPlrRank = ++GameController.mFinalRank;
					GameController.mRankArray[GameController.mCnt] = mPlrRank;
					GameController.mTimeArray[GameController.mCnt] = mPlayerTime;
					GameController.mCnt++;
					AudioManager.PlayGamePlaySound(GamePlaySoundType.Finish);
					isBoost = isDrift = false;
					BoostEffect(false);
					GameManager.LevelComplete(); //TODO add funcionality to this
				}
			}
			//if (col.CompareTag(Constants.ROAD_OBJECT_TAG) && UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
			//	//Debug.Log ("      innnnnnnnn   ");
			//}
		}

		if (col.CompareTag(Constants.AD_ENDING)) {
			//print("AD_ENDING");
			rewardAdForBoostActive = false;
			UIManager.Instance.uIGame.gamePlayPanel.SetResetButtonInteractive(false);
		}
    }

	void SetProperty() {
		mBoostSpd = transform.GetComponent<CarController>().MaxSpeed + 20f;
		mNormalSpd = transform.GetComponent<CarController>().MaxSpeed;
		Power = CarManager.GetSelectedCar().GetAcceleration();
	}
	void Brake(int val) {
		if (transform.GetComponent<CarController>().CurrentSpeed > 120 && val == -1) {
			isDrift = !isDrift;
			if (isDrift) {
				AudioManager.PlayGamePlaySound(GamePlaySoundType.BrakeBig);
			}
		} else if (val == -1) {
			AudioManager.PlayGamePlaySound(GamePlaySoundType.BrakeSmall);
		}
		if (isBoost && isDrift) {
			isBoost = false;
		}
		Forward = val;
	}
	IEnumerator<float> _SlowEffect() {
		if (mSlowMotion == 1) {
			if (Time.timeScale >= 0.5f) {
				Time.timeScale *= 0.85f;
			}
			yield return Timing.WaitForSeconds(1f);
			Time.timeScale = 1f;
			mSlowMotion = 0;
		}
	}

	void WrongDir() {

		if (UIManager.CurrentScreen == GameScreen.GamePlayScreen) {

            Vector3 progressDelta = waypointFollow.target.position - transform.position;
			float dotProduct = Vector3.Dot(progressDelta, transform.forward);
            
            if (dotProduct < -200f){
				mWrongDirGo = 1;
				UIManager.Instance.uIGame.gamePlayPanel.UpdateWrongDirectionText(Constants.WRONG_DIRECTION, counter1 % 5 == 0);
				counter++;
				if (counter % 5 == 0) {
					counter1++;
					AudioManager.PlayGamePlaySound(GamePlaySoundType.WrongDirection);
				}
				if (counter > 100) {
					if (!GameManager.isToAllowWrongDirection) {			//Testing purpose: always false in builds 
                        ResetWork(waypointFollow.target.position, waypointFollow.target.rotation, 2.0f);
                        counter = 0;
					}
				}
			} else {
				UIManager.Instance.uIGame.gamePlayPanel.UpdateWrongDirectionText("", false);
				mWrongDirGo = 0;
			}
		}
	}

	public void ResetCar()
	{
		if (UIManager.Instance.uIGame.gamePlayPanel.IsResetButtonInteractive())
		{
			UIManager.Instance.uIGame.gamePlayPanel.SetResetButtonInteractive(false);
			ResetWork(waypointFollow.circuit.GetRoutePosition(waypointFollow.progressDistance),
				Quaternion.LookRotation(waypointFollow.circuit.GetRoutePoint(waypointFollow.progressDistance).direction),
				2.0f);
			mResetCnt = 0;
		}
	}

    void ResetWork(Vector3 position, Quaternion rotation, float yPos)
    {
        position.y += yPos;
        transform.SetPositionAndRotation(position, rotation);
        m_Rigidbody.velocity = Vector3.zero;
        mResetCnt = 0;
	}

    void CheckAndResetCar()
    {
        if (UIManager.CurrentScreen == GameScreen.GamePlayScreen)
        {
            velocity = transform.InverseTransformDirection(transform.GetComponent<Rigidbody>().velocity);
            if (velocity.y < -1 && transform.position.y < -50f || velocity.y > 1 && transform.position.y > 50f)
            {
                mResetCnt += Time.deltaTime * 2f;
                if (mResetCnt > 1)
                {
                    Quaternion rot = transform.rotation;
                    if (rot.z != 0)
                    {
                        rot = Quaternion.LookRotation(waypointFollow.circuit
                            .GetRoutePoint(waypointFollow.progressDistance).direction);
                        rot.x = 0;
                        rot.z = 0;
                    }

                    ResetWork(waypointFollow.target.position, rot, 2);
                }
            }
            else if (velocity.magnitude < 2)
            {
                if (360 - transform.rotation.eulerAngles.x > 30
                    && 360 - transform.rotation.eulerAngles.x < 330
                    || 360 - transform.rotation.eulerAngles.z > 30
                    && 360 - transform.rotation.eulerAngles.z < 330)
                {
                    mResetCnt += Time.deltaTime;
                }

                if (mResetCnt > 2)
                {
                    ResetWork(waypointFollow.target.position,
                        Quaternion.LookRotation(waypointFollow.circuit.GetRoutePoint(waypointFollow.progressDistance)
                            .direction),
                        1.5f);
                }
            }
            else if ((transform.position.y < -100f) || (transform.position.y > 100f))
            {
                ResetWork(waypointFollow.target.position,
                    Quaternion.LookRotation(waypointFollow.circuit.GetRoutePoint(waypointFollow.progressDistance)
                        .direction),
                    1.5f);
            }
        }
    }

    void HideShadow() {
		isShadow = false;
		if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 2)) {
			if (hit.transform.CompareTag(Constants.ROAD_TAG) 
				|| hit.transform.CompareTag(Constants.RAILING_TAG)) {
				//Debug.DrawLine (transform.position, hit.point, Color.yellow);
				isShadow = true;
            }
		}
		if (((360 - transform.rotation.eulerAngles.x) > 10 && (360 - transform.rotation.eulerAngles.x) < 355) 
			|| ((360 - transform.rotation.eulerAngles.z) > 30 && (360 - transform.rotation.eulerAngles.z) < 330)) {
			isShadow = false;
			//Debug.Log ("	xxxx	"+(360-transform.rotation.eulerAngles.x) + "	zzzz	"+(360-transform.rotation.eulerAngles.z));
		}

	}

    private void AutoMove() {
		const float desiredSpeed = 100;
		carController.MaxSpeed = desiredSpeed;
		Vector3 offsetTargetPos = waypointFollow.target.position;
		offsetTargetPos += waypointFollow.target.right * (Mathf.PerlinNoise(Time.time * .1f, 1) * 2 - 1) * 2;
		float accelBrakeSensitivity = (desiredSpeed < carController.CurrentSpeed) ? -1 : 1;
		float accel = Mathf.Clamp((desiredSpeed) * accelBrakeSensitivity, -1, 1);
		accel *= (1 - .1f) + (Mathf.PerlinNoise(Time.time * .1f, 100) * .1f);
		Vector3 localTarget = transform.InverseTransformPoint(offsetTargetPos);
		float targetAngle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
		float steer = Mathf.Clamp(targetAngle * .01f, -1, 1) * Mathf.Sign(desiredSpeed);
		carController.Move(steer, 0, 0, 0);
		playerSpeed = carController.CurrentSpeed;
		CheckAndResetCar();
	}

	void DriftCar(float steer) {
		if (isDrift) {
			//Debug.Log ("   "+GameShop.TOPSPD[GameShop.mCarSel]+"           "+350);
			isBoost = false;
			BoostEffect(false);
			float force = m_Rigidbody.angularDrag * 200 * Time.deltaTime;
			if (steer < -.05f) {
				dir = -1;
			} else if (steer > .05f) {
				dir = 1f;
			} else {
				dir = 0;
				mDriftime += Time.deltaTime * 2f;
				if (mDriftime > 2) {
					isDrift = false;
					mDriftime = 0;
				}
			}
			
			Vector3 tor = new Vector3(0, (dir + steer) * .25f, 0);
			m_Rigidbody.AddRelativeTorque((tor * force), ForceMode.Acceleration);
			Vector3 Pos = new Vector3(-(dir + steer) * 1.2f, 0, Mathf.Abs(dir + steer) * 2f);
			m_Rigidbody.AddRelativeForce((Pos * force), ForceMode.Acceleration);
			if (mDriftCnt % 4 == 0) {
				boostVal += .003f;
			}
			mDriftCnt++;
		}
	}

    private IEnumerator<float> _ApplyCollisionForce() {
		if (colval == 1) {
			mSlowMotion = 1;
			m_Rigidbody.AddRelativeForce(Vector3.up * playerSpeed, ForceMode.Acceleration);
			m_Rigidbody.AddRelativeTorque(ForceVector * playerSpeed * 5f, ForceMode.Acceleration);
			yield return Timing.WaitForSeconds(0.2f);
			colval = 0;
		}
	}

    private void AddPower(float val) {
		if (!jump) {
			m_Rigidbody.AddForce(transform.forward * (val < 0 ? Power * val : Power), ForceMode.Acceleration);
		}
	}

	public int mActionType = 0;

    private IEnumerator<float> _ApplyJumpForce() {
		if (jump) {
			float power = ((360f / playerSpeed) * 1600f) * Time.deltaTime;
			if (playerSpeed < 70) {
				power = 0;
			}
			switch (mActionType) {
				case 0:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer.x * 2, power * .7f, power * .9f), ForceMode.Acceleration);
					break;
				case 1:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer.x * 2, power * .3f, power * .5f), ForceMode.Acceleration);
					break;
				case 2:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer.x * 2, power * 2f, power * 1.5f), ForceMode.Acceleration);
					m_Rigidbody.AddRelativeTorque(new Vector3(0, 0, 500 * Time.deltaTime * 100), ForceMode.Acceleration);
					break;
				case 3:
					m_Rigidbody.AddRelativeForce(new Vector3(power * mSteer.x, power * 2f, power * 1.5f), ForceMode.Acceleration);
					m_Rigidbody.AddRelativeTorque(new Vector3(0, 0, -500 * Time.deltaTime * 100), ForceMode.Acceleration);
					break;
			}
			float time = 2;
			if (mActionType == 2 || mActionType == 3) {
				time = 1.2f;
			}
			yield return Timing.WaitForSeconds(time);
			jump = false;
		}
	}

	IEnumerator<float> _Boost() {
        if (boostVal > 0 || boostInc >= 0) {
        } else {
            isBoost = false;
        }
        BoostEffect(isBoost);
		if (isBoost && boostVal > 0 && UIManager.CurrentScreen == GameScreen.GamePlayScreen) {
			float power = 20;
			m_Rigidbody.AddForce(transform.forward * power, ForceMode.Acceleration);
			yield return Timing.WaitForOneFrame;
		}
	}

	void BoostEffect(bool isShow) {
		for (int i = 0; i < BoostEffectObj.transform.childCount; i++) {
			ParticleSystem.EmissionModule em = BoostEffectObj.transform.GetChild(i).gameObject.GetComponent<ParticleSystem>().emission;
			em.enabled = isShow;
		}
	}

	float GetAngle(float angle) {
		angle %= 360;
		return angle > 180 ? angle - 360 : angle;
	}
}