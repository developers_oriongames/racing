﻿using UnityEngine;

public class LightOnOff : MonoBehaviour {

	int Count = 0, Count2 = 0;
	[SerializeField] Texture mTex_On = null, mTex_Off = null;
	Renderer _renderer;

	void Awake() {
		_renderer = transform.GetComponent<Renderer>();
	}

	void Start() {
		Count = Count2 = 0;
	}

	void Update() {
		_renderer.materials[1].mainTexture = Count2 % 8 == 0 ? mTex_Off : mTex_On;
		Count++;
		if (Count % 8 == 0) {
			Count2++;
		}
	}
}
