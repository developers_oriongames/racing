using System.Collections.Generic;
using UnityEngine;
using MEC;

namespace UnityStandardAssets.Vehicles.Car {
    public class SkidTrail : MonoBehaviour {

        [SerializeField] private float m_PersistTime = 0;
        IEnumerator<float> Start() {
            while (true) {
                yield return Timing.WaitForOneFrame;
                //print("////////////////////// check this out ////////////////////////");
                if (transform.parent.parent == null) {
                    Destroy(gameObject, m_PersistTime);
                }
            }
        }
    }
}