﻿using DG.Tweening;
using UnityEngine;

public class MoveOffSet : MonoBehaviour {

	[SerializeField] float scrollSpeed = 1f;
	float offset = 0;
	Renderer _renderer;

	void Start() {
		_renderer = GetComponent<Renderer>();
		Move();
	}

	void Move() {
		_renderer.material.DOOffset(new Vector2(1, 0), 1.0f).SetLoops(-1, LoopType.Restart).SetUpdate(UpdateType.Normal).SetEase(Ease.Linear);
	}

	//void Update() {
	//	offset += (Time.deltaTime * scrollSpeed);
	//	_renderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
	//	offset %= 5;
	//}
}