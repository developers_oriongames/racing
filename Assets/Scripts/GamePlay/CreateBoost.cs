﻿using System.Collections.Generic;
using UnityEngine;
using MEC;

public class CreateBoost : MonoBehaviour {

	[SerializeField] GameObject mBoostObj = null;
	GameObject[] mBoostObjClone, mBoostObjLeft, mBoostObjRight;
	[SerializeField] Transform mPoints = null;

	int Cnt = 0;
	void Start() {
		Cnt = 0;
		if(mPoints == null) {
			Debug.Log("mPoints is null!");
			return;
		}
		int no = mPoints.childCount / 8;
		for (int i = 0; i < mPoints.childCount - 10; i += no) {
			Cnt++;
		}
		mBoostObjClone = new GameObject[Cnt];
		mBoostObjLeft = new GameObject[Cnt];
		mBoostObjRight = new GameObject[Cnt];
		Cnt = 0;
		//Debug.Log ("      "+mBoostObjClone.Length);
		int ck = Navigation.GetCurrentScene() == Scenes.Track5 ? 6 : 3;
		for (int i = ck; i < mPoints.childCount - 10; i += no) {
			Vector3 pos = new Vector3(mPoints.GetChild(i).position.x + 2, mPoints.GetChild(i).position.y + 1, mPoints.GetChild(i).position.z);

			mBoostObjClone[Cnt] = Instantiate(mBoostObj, pos, Quaternion.identity);
			mBoostObjClone[Cnt].transform.localEulerAngles = mPoints.GetChild(i).transform.localEulerAngles;
			mBoostObjClone[Cnt].transform.SetParent(transform);
			mBoostObjClone[Cnt].transform.name = "Boost" + Cnt;

			mBoostObjLeft[Cnt] = Instantiate(mBoostObj, transform.InverseTransformPoint(pos - mBoostObjClone[Cnt].transform.right * 10f), Quaternion.identity);
			mBoostObjLeft[Cnt].transform.localEulerAngles = mPoints.GetChild(i).transform.localEulerAngles;
			mBoostObjLeft[Cnt].transform.SetParent(transform);
			mBoostObjLeft[Cnt].transform.name = "BoostLeft" + Cnt;

			mBoostObjRight[Cnt] = Instantiate(mBoostObj, transform.InverseTransformPoint(pos + mBoostObjClone[Cnt].transform.right * 10f), Quaternion.identity);
			mBoostObjRight[Cnt].transform.localEulerAngles = mPoints.GetChild(i).transform.localEulerAngles;
			mBoostObjRight[Cnt].transform.SetParent(transform);
			mBoostObjRight[Cnt].transform.name = "BoostRight" + Cnt;

			Cnt++;
			//Debug.Log ("            "+i+"           "+Cnt);
		}
	}

	public void WakeUp(int i) {
		//StartCoroutine(AwakeChild(i));
		Timing.RunCoroutine(_AwakeChild(i).CancelWith(gameObject));
	}

	IEnumerator<float> _AwakeChild(int i) {
		transform.GetChild(i).GetComponent<BoostCollision>().gameObject.SetActive(false);
		yield return Timing.WaitForSeconds(2f);
		transform.GetChild(i).GetComponent<BoostCollision>().isCollide = false;
		transform.GetChild(i).GetComponent<BoostCollision>().gameObject.SetActive(true);
	}
}