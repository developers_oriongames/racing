using UnityEngine;
using GameServices;

internal enum CarDriveType {
	FrontWheelDrive,
	RearWheelDrive,
	FourWheelDrive
}

internal enum SpeedType {
	MPH,
	KPH
}

public class CarController : MonoBehaviour {
	[SerializeField] private CarDriveType m_CarDriveType = CarDriveType.FourWheelDrive;
	public WheelCollider[] m_WheelColliders = new WheelCollider[4]; 
	[SerializeField] GameObject[] m_WheelMeshes = new GameObject[4]; 
	[SerializeField] WheelEffects[] m_WheelEffects = new WheelEffects[4]; 
	[SerializeField] public Vector3 m_CentreOfMassOffset;
	[SerializeField] public float m_MaximumSteerAngle;
	[Range(0, 1)] [SerializeField] public float m_SteerHelper; // 0 is raw physics , 1 the car will grip in the direction it is facing
	[Range(0, 1)] [SerializeField] public float m_TractionControl; // 0 is no traction control, 1 is full interference
	[SerializeField] public float m_FullTorqueOverAllWheels;
	[SerializeField] public float m_ReverseTorque;
	[SerializeField] public float m_MaxHandbrakeTorque;
	[SerializeField] public float m_Downforce = 100f;
	[SerializeField] private SpeedType m_SpeedType = SpeedType.KPH;
	[SerializeField] private float m_TopSpeed = 200;
	[SerializeField] public static int NoOfGears = 5;
	[SerializeField] public float m_RevRangeBoundary = 1f;
	float m_SideSlipLimit, m_FwdSlipLimit;
	[SerializeField] public float m_BrakeTorque;

	private Quaternion[] m_WheelMeshLocalRotations;
	private Vector3 m_Prevpos, m_Pos;
	private float m_SteerAngle;
	private int m_GearNum;
	private float m_GearFactor;
	private float m_OldRotation;
	private float m_CurrentTorque;
	Rigidbody m_Rigidbody;
	private const float k_ReversingThreshold = 0.01f;

	public bool Skidding { get; private set; }
	public float BrakeInput { get; private set; }
	public float CurrentSteerAngle { get { return m_SteerAngle; } }
	public float CurrentSpeed { get { return m_Rigidbody.velocity.magnitude * SpdFact; } }
	public float MaxSpeed { get { return m_TopSpeed; } set { m_TopSpeed = value; } }
	public float Revs { get; private set; }
	public float AccelInput { get; private set; }

	private GameObject COG = null;

	[SerializeField] private GameObject carMarker;

	int mPickUp = 10;
	float SpdFact = 0;
	private float mph = 2.23693629f;
	private float kph = 2.7f; 

	[SerializeField] private AnimationCurve mwheelRestrictCurve;
	
	void Awake() {
		m_Rigidbody = transform.GetComponent<Rigidbody>();
		for (int i = 0; i < transform.GetChild(1).childCount; i++) {
			m_WheelMeshes[i] = transform.GetChild(1).GetChild(i).gameObject;
			m_WheelColliders[i] = transform.GetChild(2).GetChild(i).GetComponent<WheelCollider>();
			m_WheelEffects[i] = transform.GetChild(2).GetChild(i).GetComponent<WheelEffects>();
		}
		m_WheelMeshLocalRotations = new Quaternion[4];
		for (int i = 0; i < 4; i++) {
			m_WheelMeshLocalRotations[i] = m_WheelMeshes[i].transform.localRotation;
		}
		COG = transform.GetChild(4).gameObject;
		m_CentreOfMassOffset = new Vector3(0, COG.transform.localPosition.y, 0);
		m_WheelColliders[0].attachedRigidbody.centerOfMass = m_CentreOfMassOffset;
		m_MaxHandbrakeTorque = float.MaxValue;
		m_CurrentTorque = m_FullTorqueOverAllWheels - (m_TractionControl * m_FullTorqueOverAllWheels);
		SpdFact = m_SpeedType == SpeedType.KPH ? kph : mph;

		Vector3 mIntertia = transform.GetComponent<Rigidbody>().inertiaTensor;
		m_Rigidbody.centerOfMass = m_CentreOfMassOffset;
		transform.GetComponent<Rigidbody>().inertiaTensor = mIntertia;
		SetCarProperty();
		carMarker = transform.Find("Marker").gameObject;
	}

	void Update() {
		//	Debug.Log("m_TopSpeed : ::::::::::::::::::::::::::::::::: :" + (int)m_TopSpeed);
		carMarker.transform.localRotation = Quaternion.Euler(new Vector3(90, transform.rotation.y, 90));
		//carMarker.transform.rotation = Quaternion.Euler(new Vector3(90, transform.rotation.y, 90));
	}

	private void GearChanging() {
		float f = Mathf.Abs(CurrentSpeed / MaxSpeed);
		float upgearlimit = (1 / (float)NoOfGears) * (m_GearNum + 1);
		float downgearlimit = (1 / (float)NoOfGears) * m_GearNum;
		if (m_GearNum > 0 && f < downgearlimit) {
			m_GearNum--;
		}
		if (f > upgearlimit && (m_GearNum < (NoOfGears - 1))) {
			m_GearNum++;
		}
	}

	private static float CurveFactor(float factor) {
		return 1 - (1 - factor) * (1 - factor);
	}

	private static float ULerp(float from, float to, float value) {
		return (1.0f - value) * from + value * to;
	}

	private void CalculateGearFactor() {
		float f = (1 / (float)NoOfGears);
		var targetGearFactor = Mathf.InverseLerp(f * m_GearNum, f * (m_GearNum + 1), Mathf.Abs(CurrentSpeed / MaxSpeed));
		m_GearFactor = Mathf.Lerp(m_GearFactor, targetGearFactor, Time.deltaTime * 5f);
	}

	private void CalculateRevs() {
		// calculate engine revs (for display / sound)
		// (this is done in retrospect - revs are not used in force/power calculations)
		CalculateGearFactor();
		var gearNumFactor = m_GearNum / (float)NoOfGears;
		var revsRangeMin = ULerp(0f, m_RevRangeBoundary, CurveFactor(gearNumFactor));
		var revsRangeMax = ULerp(m_RevRangeBoundary, 1f, gearNumFactor);
		Revs = ULerp(revsRangeMin, revsRangeMax, m_GearFactor);
	}

	public void Move(float steering, float accel, float footbrake, float handbrake) {
		for (int i = 0; i < 4; i++) {
			m_WheelColliders[i].GetWorldPose(out Vector3 position, out Quaternion quat);
			m_WheelMeshes[i].transform.position = position;
			m_WheelMeshes[i].transform.rotation = quat;
		}
		//clamp input values
		steering = Mathf.Clamp(steering, -1, 1);
		AccelInput = accel = Mathf.Clamp(accel, 0, 1);
		BrakeInput = footbrake = -1 * Mathf.Clamp(footbrake, -1, 0);
		handbrake = Mathf.Clamp(handbrake, 0, 1);
		m_MaximumSteerAngle = mwheelRestrictCurve.Evaluate(CurrentSpeed);
		m_SteerAngle = steering * m_MaximumSteerAngle;

		if (CarUserControl.isDrift && transform.root.name == Constants.PLAYER_NAME) {
			m_WheelColliders[0].steerAngle = -m_SteerAngle * 2;
			m_WheelColliders[1].steerAngle = -m_SteerAngle * 2;
		} else {
			m_WheelColliders[0].steerAngle = m_SteerAngle;
			m_WheelColliders[1].steerAngle = m_SteerAngle;
		}

		SteerHelper();
		ApplyDrive(accel, footbrake);
		CapSpeed();
		if (handbrake > 0f) {
			var hbTorque = handbrake * m_MaxHandbrakeTorque;
			for (int i = 0; i < 4; i++) {
				m_WheelColliders[i].brakeTorque = hbTorque;
				//Debug.Log("Brake");
			}
		}
		//					else 
		//					 {
		//						for (int i = 0; i < 4; i++) {
		//							m_WheelColliders [i].brakeTorque = 0;
		//						}
		//					 }
		CalculateRevs();
		GearChanging();
		AddDownForce();
		CheckForWheelSpin();
		TractionControl();
		ApplyStiffness();
		JumpFromRamp();
	}

	float dv = 0.01f;
	JointSpring S;
	void ApplyStiffness() {
		for (int i = 0; i < 4; i++) {
			if (m_WheelColliders[i].GetGroundHit(out WheelHit hit)) {
				WheelFrictionCurve fFriction = m_WheelColliders[i].forwardFriction;
				WheelFrictionCurve sFriction = m_WheelColliders[i].sidewaysFriction;
				float stiffness = hit.collider.material.staticFriction;
				//float val = m_Rigidbody.velocity.magnitude;
				if (transform.root.name == Constants.PLAYER_NAME) {
					fFriction.stiffness = 1;
					stiffness += m_TopSpeed * 0.01f;
					if (CarManager.GetSelectedCar().GetCarID() == 4 
					 || CarManager.GetSelectedCar().GetCarID() == 1)
						sFriction.stiffness = stiffness * 0.2f;
					//sFriction.extremumSlip  = CarUserControl.isDrift?(10):(SideExtremumSlip);
					sFriction.stiffness = CarUserControl.isDrift ? (0.5f) : stiffness;
					m_FwdSlipLimit = CarUserControl.Forward < 0 ? (0.2f) : (1f);
					m_SideSlipLimit = CarUserControl.isDrift ? (0.1f) : (0.2f);
					//						if(CarUserControl.isSky)
					//							m_Rigidbody.angularDrag = 1f; 
					//						else
					m_Rigidbody.angularDrag = (m_TopSpeed * 0.025f + m_Rigidbody.velocity.magnitude * 0.025f) - (CarManager.GetSelectedCar().GetCarID() * 0.3f);
					//						Debug.Log ("          "+m_Rigidbody.velocity.magnitude+"      "+stiffness+"           "+m_Rigidbody.angularDrag);
				} else {
					stiffness += m_TopSpeed * 0.01f;
					fFriction.stiffness = 1;
					sFriction.stiffness = stiffness;
					m_Rigidbody.angularDrag = m_TopSpeed * 0.025f + m_Rigidbody.velocity.magnitude * 0.025f;
				}
				m_WheelColliders[i].forwardFriction = fFriction;
				m_WheelColliders[i].sidewaysFriction = sFriction;
			}
		}
	}

	void JumpFromRamp() {
		for (int i = 0; i < 4; i++) {
			if (m_WheelColliders[i].GetGroundHit(out WheelHit hit)) {
				if (transform.root.name == Constants.PLAYER_NAME) {
					CarUserControl carUserControl = GetComponent<CarUserControl>();
					if (hit.collider.CompareTag(Constants.RAMP_TAG)
						|| hit.collider.CompareTag(Constants.SMALL_RAMP_TAG)
						|| hit.collider.CompareTag(Constants.TURN_RAMP_L_TAG)
						|| hit.collider.CompareTag(Constants.TURN_RAMP_R_TAG)) {
						carUserControl.mSlowMotion = 1;
						if (hit.collider.CompareTag(Constants.RAMP_TAG)) {
                            carUserControl.mActionType = 0;
                        }
                        if (hit.collider.CompareTag(Constants.SMALL_RAMP_TAG)) {
                            carUserControl.mActionType = 1;
                        }
                        if (hit.collider.CompareTag(Constants.TURN_RAMP_L_TAG)) {
                            carUserControl.mActionType = 2;
                        }
                        if (hit.collider.CompareTag(Constants.TURN_RAMP_R_TAG)) {
                            carUserControl.mActionType = 3;
                        }
                        if (!CarUserControl.jump) {
							Debug.Log("innnnnnnnn jumppp");
							carUserControl.boostVal += 0.2f;
							CarUserControl.jump = true;
							CarUserControl.isSky = true;
							//GetComponentInParent<GameController>().PlayClip("Jump");
							AudioManager.PlayGamePlaySound(GamePlaySoundType.Jump);
							//Debug.Log ("              "+hit.collider.tag+"          "+CarUserControl.jump);
						}
					}
					if (hit.collider.CompareTag(Constants.ROAD_TAG)) {
						if (CarUserControl.isSky) {
							CarUserControl.isSky = false;
							CarUserControl.isDrift = false;
						}
					}
				} else {
					CarAIControl carAIControl = GetComponent<CarAIControl>();
					if (hit.collider.CompareTag(Constants.RAMP_TAG)
						|| hit.collider.CompareTag(Constants.SMALL_RAMP_TAG)
						|| hit.collider.CompareTag(Constants.TURN_RAMP_L_TAG)
						|| hit.collider.CompareTag(Constants.TURN_RAMP_R_TAG)) {
						if (hit.collider.CompareTag(Constants.RAMP_TAG)) {
							carAIControl.mActionType = 0;
						}
						if (hit.collider.CompareTag(Constants.SMALL_RAMP_TAG)) {
							carAIControl.mActionType = 1;
						}
						if (hit.collider.CompareTag(Constants.TURN_RAMP_L_TAG)) {
							carAIControl.mActionType = 1;
						}
						if (hit.collider.CompareTag(Constants.TURN_RAMP_R_TAG)) {
							carAIControl.mActionType = 1;
						}
						if (!carAIControl.jump) {
							//Debug.Log ("innnnnnnnn jumppp");
							//transform.GetComponent<CarUserControl> ().boostVal += .2f;
							carAIControl.mBoostVal = 1;
							carAIControl.mBoostPower = 20;
							carAIControl.mBoostTime += 12;
							carAIControl.jump = true;
						}
					}
					//						if(hit.collider.tag == "Ramp") 
					//						{
					//							if(!transform.GetComponent<CarAIControl> ().jump) 
					//							{
					//							  transform.GetComponent<CarAIControl> ().jump = true;
					//							}
					//						}
				}
			}
		}
	}
	private void CapSpeed() {
		float speed = m_Rigidbody.velocity.magnitude;
		m_TopSpeed = GameManager.isInTutorial && !TutorialManager.isTutorialDemonstrationOver
				? TutorialManager.GetTutorialCarTopSpeed() : m_TopSpeed;
		switch (m_SpeedType) {
			case SpeedType.MPH:
				speed *= SpdFact;
				if (speed > m_TopSpeed) {
					m_Rigidbody.velocity = (m_TopSpeed / SpdFact) * m_Rigidbody.velocity.normalized;
				}
				break;
			case SpeedType.KPH:
				speed *= SpdFact;
				if (speed > m_TopSpeed) {
					m_Rigidbody.velocity = (m_TopSpeed / SpdFact) * m_Rigidbody.velocity.normalized;
				}
				break;
		}
	}

	private void ApplyDrive(float accel, float footbrake) {
		float thrustTorque;
		switch (m_CarDriveType) {
			case CarDriveType.FourWheelDrive:
				thrustTorque = accel * (m_CurrentTorque / 4f);
				for (int i = 0; i < 4; i++) {
					m_WheelColliders[i].motorTorque = thrustTorque;
				}
				break;
			case CarDriveType.FrontWheelDrive:
				thrustTorque = accel * (m_CurrentTorque / 2f);
				m_WheelColliders[0].motorTorque = m_WheelColliders[1].motorTorque = thrustTorque;
				break;
			case CarDriveType.RearWheelDrive:
				thrustTorque = accel * (m_CurrentTorque / 2f);
				m_WheelColliders[2].motorTorque = m_WheelColliders[3].motorTorque = thrustTorque;
				break;
		}
		for (int i = 0; i < 4; i++) {
			if (CurrentSpeed > 5 && Vector3.Angle(transform.forward, m_Rigidbody.velocity) < 50f) {
				m_WheelColliders[i].brakeTorque = m_BrakeTorque * footbrake;
			} else if (footbrake > 0) {
				m_WheelColliders[i].brakeTorque = 0f;
				m_WheelColliders[i].motorTorque = -m_ReverseTorque * footbrake;
			}
		}
	}

	private void SteerHelper() {
		for (int i = 0; i < 4; i++) {
			m_WheelColliders[i].GetGroundHit(out WheelHit wheelhit);
			if (wheelhit.normal == Vector3.zero) {
				return;
			}
		}
		if (Mathf.Abs(m_OldRotation - transform.eulerAngles.y) < 10f) {
			var turnadjust = (transform.eulerAngles.y - m_OldRotation) * m_SteerHelper;
			Quaternion velRotation = Quaternion.AngleAxis(turnadjust, Vector3.up);
			m_Rigidbody.velocity = velRotation * m_Rigidbody.velocity;
		}
		m_OldRotation = transform.eulerAngles.y;
	}

	private void AddDownForce() {
		m_WheelColliders[0].attachedRigidbody.AddForce(-Vector3.up * m_Downforce * m_WheelColliders[0].attachedRigidbody.velocity.magnitude);
	}

	private void CheckForWheelSpin() {
		//loop through all wheels
		for (int i = 0; i < 4; i++) {
			WheelHit wheelHit;
			if (m_WheelColliders[i].GetGroundHit(out wheelHit)) {
				if (Mathf.Abs(wheelHit.forwardSlip) >= m_FwdSlipLimit || Mathf.Abs(wheelHit.sidewaysSlip) >= m_SideSlipLimit) {
					//if(gameObject.tag == "PlayerCar")
					{
						m_WheelEffects[i].EmitTyreSmoke();
						if (!AnySkidSoundPlaying()) {
							//m_WheelEffects[i].PlayAudio();
						}
					}
					continue;
				}
				m_WheelEffects[i].EndSkidTrail();
			} else {
				//if (m_WheelEffects[i].PlayingAudio)
				//{
				//m_WheelEffects[i].StopAudio();
				//}
				m_WheelEffects[i].EndSkidTrail();
			}

		}
	}

	// crude traction control that reduces the power to wheel if the car is wheel spinning too much
	private void TractionControl() {
		WheelHit wheelHit;
		switch (m_CarDriveType) {
			case CarDriveType.FourWheelDrive:
				// loop through all wheels
				for (int i = 0; i < 4; i++) {
					m_WheelColliders[i].GetGroundHit(out wheelHit);
					AdjustTorque(wheelHit.forwardSlip);
				}
				break;

			case CarDriveType.RearWheelDrive:
				m_WheelColliders[2].GetGroundHit(out wheelHit);
				AdjustTorque(wheelHit.forwardSlip);

				m_WheelColliders[3].GetGroundHit(out wheelHit);
				AdjustTorque(wheelHit.forwardSlip);
				break;

			case CarDriveType.FrontWheelDrive:
				m_WheelColliders[0].GetGroundHit(out wheelHit);
				AdjustTorque(wheelHit.forwardSlip);

				m_WheelColliders[1].GetGroundHit(out wheelHit);
				AdjustTorque(wheelHit.forwardSlip);
				break;
		}
	}

	private void AdjustTorque(float forwardSlip) {
		if (forwardSlip >= m_FwdSlipLimit && m_CurrentTorque > 0) {
			m_CurrentTorque -= mPickUp * m_TractionControl;
		} else {
			m_CurrentTorque += mPickUp * m_TractionControl;
			if (m_CurrentTorque > m_FullTorqueOverAllWheels) {
				m_CurrentTorque = m_FullTorqueOverAllWheels;
			}
		}
	}

	private bool AnySkidSoundPlaying() {
		for (int i = 0; i < 4; i++) {
			//if (m_WheelEffects[i].PlayingAudio)
			//{
			//    return true;
			//}
		}
		return false;
	}

	float SideExtremumSlip = 0;
    private float aiPowerModifier = 0.75f;

    void SetCarProperty() {
		m_Rigidbody.angularDrag = 1f;
		m_Rigidbody.drag = .01f;
		m_SideSlipLimit = .17f;
		m_FwdSlipLimit = 1;
		m_Downforce = 1000;
		m_Rigidbody.mass = 1000;
		mPickUp = 10;
		if (transform.parent.name == Constants.PLAYER_NAME) {
			m_MaximumSteerAngle = 35;
			m_TopSpeed = CarManager.GetSelectedCar().GetTopSpeed();
			m_SteerHelper = CarManager.GetSelectedCar().GetHandling(); 
			m_FullTorqueOverAllWheels = m_TopSpeed * 15f;
			mwheelRestrictCurve = new AnimationCurve(new Keyframe(0f, 30f), new Keyframe(m_TopSpeed, 10f));//first number in Keyframe is speed, second is max wheelbar degree
		} else {
			m_MaximumSteerAngle = 35;
			m_TopSpeed = GetSpeed();
			m_SteerHelper = 0.65f;
			m_FullTorqueOverAllWheels = m_TopSpeed * 17f;
		}
		for (int i = 0; i < 4; i++) {
			WheelFrictionCurve fFriction = m_WheelColliders[i].forwardFriction;
			WheelFrictionCurve sFriction = m_WheelColliders[i].sidewaysFriction;
			m_WheelColliders[i].forceAppPointDistance = -m_WheelColliders[i].radius / 2f;
			m_WheelColliders[i].mass = 25f;

			fFriction.extremumSlip = .4f;
			fFriction.extremumValue = 1;
			fFriction.asymptoteSlip = .8f;
			fFriction.asymptoteValue = .5f;
			m_WheelColliders[i].forwardFriction = fFriction;

			SideExtremumSlip = sFriction.extremumSlip = .2f;
			sFriction.extremumValue = 1;
			sFriction.asymptoteSlip = .5f;
			sFriction.asymptoteValue = .75f;
			m_WheelColliders[i].sidewaysFriction = sFriction;

			//				m_WheelColliders [i].forceAppPointDistance = -.1f;	
			if (transform.parent.name == Constants.PLAYER_NAME) {
				m_WheelColliders[i].suspensionDistance = .1f;
				S = m_WheelColliders[i].suspensionSpring;
				S.spring = 100000;
				S.damper = 5000f;
				S.targetPosition = .5f;
				m_WheelColliders[i].suspensionSpring = S;
			} else {
				//					m_WheelColliders [i].wheelDampingRate = .25f;
				S = m_WheelColliders[i].suspensionSpring;
				S.spring = 50000;
				S.damper = 2000;
				S.targetPosition = .5f;
				m_WheelColliders[i].suspensionSpring = S;
			}
		}
		float topSpeed = CarManager.GetSelectedCar().GetTopSpeed();
		m_Rigidbody.mass = topSpeed * 8f;

		if (topSpeed > 300) {
			kph = 4.5f;
		} else if (topSpeed > 270 && topSpeed <= 300) {
			kph = 4f;
		} else if (topSpeed > 250 && topSpeed <= 270) {
			kph = 3.5f;
		} else if (topSpeed > 220 && topSpeed <= 250) {
			kph = 3.3f;
		} else {
			kph = 3.1f;
		}

		SpdFact = m_SpeedType == SpeedType.KPH ? kph : mph;
	}

	public void UpdateSpring() {
		for (int i = 0; i < 4; i++) {
			S = m_WheelColliders[i].suspensionSpring;
			S.targetPosition += dv;
			if (S.targetPosition > .7f) {
				dv = -.01f;
			}
			if (S.targetPosition < .4f) {
				dv = .01f;
			}
			S.targetPosition = Mathf.Clamp(S.targetPosition, .4f, .7f);
			m_WheelColliders[i].suspensionSpring = S;
		}
	}

	float GetSpeed() {
        Car gameCar = new Car();
        int level = LevelManager.selectedLevel;
        bool isToReduceAIPower = false;
		switch (level)
        {
            case 0:
                gameCar = CarManager.Instance.cars[0]; isToReduceAIPower = true;
                break;
            case 1:
                gameCar = CarManager.Instance.cars[0]; isToReduceAIPower = true;
				break;
            case 2:
                gameCar = CarManager.Instance.cars[1]; isToReduceAIPower = true;
				break;
            case 3:
                gameCar = CarManager.Instance.cars[1];
                break;
            case 4:
                gameCar = CarManager.Instance.cars[2]; isToReduceAIPower = true;
				break;
            case 5:
                gameCar = CarManager.Instance.cars[2]; isToReduceAIPower = true;
				break;
            case 6:
                gameCar = CarManager.Instance.cars[3]; isToReduceAIPower = true;
				break;
            case 7:
                gameCar = CarManager.Instance.cars[3];
                break;
            case 8:
                gameCar = CarManager.Instance.cars[4]; isToReduceAIPower = true;
				break;
            case 9:
                gameCar = CarManager.Instance.cars[4]; isToReduceAIPower = true;
				break;
            case 10:
                gameCar = CarManager.Instance.cars[5];
                break;
            case 11:
                gameCar = CarManager.Instance.cars[5];
                break;
            case 12:
                gameCar = CarManager.Instance.cars[6];
                break;
            case 13:
                gameCar = CarManager.Instance.cars[6]; isToReduceAIPower = true;
				break;
            case 14:
                gameCar = CarManager.Instance.cars[7];
                break;
            case 15:
                gameCar = CarManager.Instance.cars[7];
                break;
            case 16:
                gameCar = CarManager.Instance.cars[8]; isToReduceAIPower = true;
				break;
            case 17:
                gameCar = CarManager.Instance.cars[8]; isToReduceAIPower = true;
				break;
            case 18:
                gameCar = CarManager.Instance.cars[9]; isToReduceAIPower = true;
				break;
            case 19:
                gameCar = CarManager.Instance.cars[9]; isToReduceAIPower = true;
				break;
            case 20:
                gameCar = CarManager.Instance.cars[10];
                break;
            case 21:
                gameCar = CarManager.Instance.cars[10];
                break;
            case 22:
                gameCar = CarManager.Instance.cars[11]; isToReduceAIPower = true;
				break;
            case 23:
                gameCar = CarManager.Instance.cars[11];
                break;
            case 24:
                gameCar = CarManager.Instance.cars[12];
                break;
            case 25:
                gameCar = CarManager.Instance.cars[13];
                break;
            case 26:
                gameCar = CarManager.Instance.cars[14]; isToReduceAIPower = true;
				break;
        }
        switch (InputManager.GetCurrentPlatform())
        {
            case BuildPlatforms.AmazonTab:
                aiPowerModifier = 0.85f; //0.9f
                break;
            case BuildPlatforms.AmazonTv:
                aiPowerModifier = 0.80f; //0.85f
                break;
        }
		if (!isToReduceAIPower)
        {
            aiPowerModifier = 1;
        }
        float spd = gameCar.GetTopSpeed() * aiPowerModifier;
        float acc = gameCar.GetAcceleration() * aiPowerModifier;

        transform.GetComponent<CarAIControl>().mCarPower = acc;
		if (Navigation.GetCurrentScene() == Scenes.Track4 
			|| Navigation.GetCurrentScene() == Scenes.Track6
			|| Navigation.GetCurrentScene() == Scenes.Track7) {
			if (GameModeManager.GetCurrentGameMode == GameModes.Versus 
				|| GameModeManager.GetCurrentGameMode == GameModes.Elimination) {
				spd += 5f;	
			} else {
				spd += 3f;
			}
		} else {
			if (GameModeManager.GetCurrentGameMode == GameModes.Versus 
				|| GameModeManager.GetCurrentGameMode == GameModes.Elimination) {
				spd += 2f;	
			} else {
				spd += 1f;
			}
		}
		mwheelRestrictCurve = new AnimationCurve(new Keyframe(0f, 30f), new Keyframe(spd, 20f));	//first number in Keyframe is speed, second is max wheelbar degree
		return spd;
	}
}