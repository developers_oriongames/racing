﻿using UnityEngine;
using GameServices;

public class BoostCollision : MonoBehaviour {

	[SerializeField] GameObject mBoostParticle = null;
	public bool isCollide = false;
	GameObject mBoostParticleClone;
	int mDir = 0;

	void Start() {
		mDir = -1;
		isCollide = false;
	}

	void OnTriggerEnter(Collider col) {
		if (col.transform.parent.parent.CompareTag(Constants.PLAYER_TAG)) {
			if (!isCollide) {
				//col.transform.root.GetComponent<GameController>().PlayClip("boostcollect");
				AudioManager.PlayGamePlaySound(GamePlaySoundType.BoostCollect);
				col.transform.parent.parent.GetComponent<CarUserControl>().boostVal += 0.2f + CarManager.GetSelectedCar().GetNitro();
				mBoostParticleClone = Instantiate(mBoostParticle, 
										new Vector3(col.transform.position.x, col.transform.position.y + 2f, col.transform.position.z),
										Quaternion.Euler(0, 90, 0));
				mBoostParticleClone.transform.SetParent(col.transform.parent.parent.transform);
				Destroy(mBoostParticleClone, 3f);
				isCollide = true;
				transform.GetComponentInParent<CreateBoost>().WakeUp(transform.GetSiblingIndex());
			}
		}
		if (col.transform.parent.parent.CompareTag(Constants.OPPONENT_TAG)) {
			isCollide = true;
			col.transform.parent.parent.GetComponent<CarAIControl>().mBoostVal = 1;
			col.transform.parent.parent.GetComponent<CarAIControl>().mBoostPower = 20;
			col.transform.parent.parent.GetComponent<CarAIControl>().mBoostTime += 12;
			transform.GetComponentInParent<CreateBoost>().WakeUp(transform.GetSiblingIndex());
			//Debug.Log ("innnnnnnnn      "+col.transform.parent.name);
		}
	}
	void Update() {
		transform.Rotate(Vector3.forward * Time.deltaTime * mDir * 50);
		if (GetAngle(transform.localEulerAngles.z) < -60 && mDir == -1) { mDir = 1; }
		if (GetAngle(transform.localEulerAngles.z) > 60 && mDir == 1) { mDir = -1; }
	}
	float GetAngle(float angle) {
		angle %= 360;
		return angle > 180 ? angle - 360 : angle;
	}
}