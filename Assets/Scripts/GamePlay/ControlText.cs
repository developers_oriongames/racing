﻿using UnityEngine;
using GameServices;

public class ControlText : MonoBehaviour {

	private float time = 0;
	[SerializeField] private byte mType = 0;
	[SerializeField] private MeshRenderer meshRenderer;

	void Start() {
		time = 0;
	}

	void Update() {
		if (mType == 0) {
			meshRenderer.enabled = time < 30;
			if (time < 30) {
				time += Time.deltaTime;
			}
		} else {
			meshRenderer.enabled = time > 100 && GameModeManager.GetCurrentGameMode != GameModes.Elimination;
			time += Time.deltaTime * 2;
		}
	}
}
