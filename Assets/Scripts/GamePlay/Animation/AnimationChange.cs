﻿using MEC;
using TMPro;
using UnityEngine;
using System.Collections.Generic;

public class AnimationChange : MonoBehaviour
{
    public TMP_Text countdownText;

    private float environmentShowWaitTime = 3.5f;   //3.5f
    private float firstTextShowWaitTime = 2.5f;     //1.5f
    private float secondextShowWaitTime = 2.5f;    //2.75f
    private float thirdTextShowWaitTime = 2.5f;     //3.0f

    private Color tempColor;

    private const string EMPTY = "";
    private const string THREE = "3";
    private const string TWO = "2";
    private const string ONE = "1";

    public  void StartCountDown()
    {
        print("Play StartCountDown!");
        Timing.RunCoroutine(_TextAnimation());
    }

    void ChangeAnim(int no)
    {
        Debug.LogError("In game called: ChangeAnim(int no): " + no);
        for (int i = 0; i < transform.parent.childCount; i++)
        {
            transform.parent.GetChild(i).gameObject.SetActive(i == no);
        }
    }

    /// <summary>
    /// Call from animation clip
    /// </summary>
    void StartGame()
    {
        GameAnimationManager.StartGame();
    }

    private IEnumerator<float> _TextAnimation()
    {
        countdownText.text = EMPTY;
        yield return Timing.WaitForSeconds(environmentShowWaitTime);
        countdownText.text = THREE;
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(_ShowText(firstTextShowWaitTime)));
        countdownText.text = TWO;
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(_ShowText(secondextShowWaitTime)));
        countdownText.text = ONE;
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(_ShowText(thirdTextShowWaitTime)));
        countdownText.text = EMPTY;
    }

    private IEnumerator<float> _ShowText(float totalTime)
    {
        float lerpTime = 0.0f;
        while (lerpTime < totalTime)
        {
            lerpTime += Time.deltaTime;
            tempColor = countdownText.color;
            tempColor.a = Mathf.Lerp(1, 0, lerpTime / totalTime);
            countdownText.color = tempColor;
            yield return Timing.WaitForOneFrame;
        }
    }
}