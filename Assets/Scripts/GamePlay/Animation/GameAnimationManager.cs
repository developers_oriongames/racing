﻿using UnityEngine;
using GameServices;

public class GameAnimationManager : MonoBehaviour {

    private static GameAnimationManager instance;

    [Header("Asset Ref: Game Animation Clips")]
    [SerializeField] private AnimationClip[] gameStartAnimationClips;

    [Header("Game Ref")] [SerializeField] private AnimationChange animationChange;
    [SerializeField] private GameObject gameStartAnimationObj;
    [SerializeField] private GameObject gameStartAnimationPanel;
    [SerializeField] private GameObject skipButtonObj;
    private Animation gameStartAnimation;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    void Start() {
        SetAnimationClip();
        if (!GameManager.isInTutorial) {
            PlayAnimation();
            if (GameManager.isToHideGameStartAnimation) {
                Invoke(nameof(HideAnimationTest), 0.1f);
            }
        }
    }

    private void HideAnimationTest() {
        SkipAnimation();
    }

    private void SetAnimationClip() {
        gameStartAnimation = gameStartAnimationObj.AddComponent<Animation>();
        gameStartAnimation.clip = null;
        AnimationClip clip = null;
        if (Navigation.IsInGameScene()) {
            clip = gameStartAnimationClips[(int)Navigation.GetCurrentScene() - (int)Scenes.Track0];   //(int)Scenes.Track0 == 2 is the 1st game scene
        } else if (Navigation.IsInTutorialScene()) {
            clip = gameStartAnimationClips[gameStartAnimationClips.Length - 1];
        }
        gameStartAnimation.clip = clip;
        gameStartAnimation.AddClip(clip, clip.name);
        //skipButtonObj.SetActive(!GameManager.isInTutorial);
        gameStartAnimationPanel.SetActive(!GameManager.isInTutorial);
    }

    public static void PlayAnimation() {
        if (instance.gameStartAnimation.clip != null) {
           instance.gameStartAnimation.Play();
           instance.animationChange.StartCountDown();
           print("Play Animation!");
        } else {
            Debug.LogError("No Animation Clip Were Found");
        }
        //instance.skipButtonObj.SetActive(!GameManager.isInTutorial);
        instance.gameStartAnimationPanel.SetActive(!GameManager.isInTutorial);
    }

    public static void StartGame() {
        instance.gameStartAnimationObj.SetActive(false);
        Destroy(instance.gameStartAnimationObj);
        if (!GameManager.isInTutorial) {
            Debug.Log("Game Starts Now!");      
            UIManager.CurrentScreen = GameScreen.GamePlayScreen;
            GameEvents.OnGamePlayScreen();
        } else if(GameManager.isInTutorial) {
            Debug.Log("Tutorial Starts Now!");
            TutorialManager.GameStartAnimationEnded();
        }
    }

    /// <summary>
    /// Called From Skip button
    /// </summary>
    public void SkipAnimation() {
        instance.gameStartAnimation.Stop();
        AudioManager.StopGamePlaySound();
        StartGame();
    }
}