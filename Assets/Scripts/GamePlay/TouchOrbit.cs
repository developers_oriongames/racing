﻿using GameServices;
using UnityEngine;

public class TouchOrbit : MonoBehaviour {

	[SerializeField] private Transform target;

	private float distance = 5.0f;

	private float xSpeed = 0.5f;
	private float ySpeed = 0.5f;

	private float yMinLimit = 5f;
	private float yMaxLimit = 60f;

	private float distanceMin = 5f;
	private float distanceMax = 5.1f;

	private float rotationXAxis = 0.0f;
	private float rotationYAxis = 0.0f;

	private float velocityX = 0.0f;
	private float velocityY = 0.0f;

    private float smoothTime = 2f;

	private Touch touch;

	void Start() {
		Vector3 angles = transform.eulerAngles;
		rotationYAxis = angles.y;
		rotationXAxis = angles.x;
	}

	void LateUpdate() {
		if (target && UIManager.CurrentScreen == GameScreen.PreviewScreen) {
			if (InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) { return; }
#if UNITY_EDITOR
			if (Input.GetMouseButton(0)) {
				velocityX += xSpeed * Input.GetAxis("Mouse X") * 0.02f;
				velocityY += ySpeed * Input.GetAxis("Mouse Y") * 0.02f;
			}
#endif
#if UNITY_ANDROID
			if (Input.touchCount > 0 
				&& (Input.GetTouch(0).phase == TouchPhase.Began 
					|| Input.GetTouch(0).phase == TouchPhase.Moved)) {
				//One finger touch does orbit
				touch = Input.GetTouch(0);
				velocityX += xSpeed * touch.deltaPosition.x * 0.02f;
				velocityY += ySpeed * touch.deltaPosition.y * 0.02f;
			}
#endif
			rotationYAxis += velocityX;
			rotationXAxis -= velocityY;
			rotationXAxis = ClampAngle(rotationXAxis, yMinLimit, yMaxLimit);
			//Quaternion fromRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
			Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
			Quaternion rotation = toRotation;
			distance = Mathf.Clamp(distance, distanceMin, distanceMax);
			Vector3 negDistance = new Vector3(0.0f, 0, -distance);
			Vector3 position = rotation * negDistance + target.position;
			transform.rotation = rotation;
			transform.position = position;
			velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * smoothTime);
			velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * smoothTime);
		}
	}

	void Update() {
		transform.LookAt(target);
		if (UIManager.CurrentScreen == GameScreen.PreviewScreen) {
			if (isToRotate && InputManager.GetCurrentPlatform() == BuildPlatforms.AmazonTv) {
				transform.RotateAround(target.position, Vector3.down, 10 * Time.deltaTime);
			}
		}
	}

	public static float ClampAngle(float angle, float min, float max) {
        if (angle < -360F) {
			angle += 360F;
		}
        if (angle > 360F) {
			angle -= 360F;
		}
		return Mathf.Clamp(angle, min, max);
	}

	bool isToRotate;
	public void OnTouchCamera() {
		velocityX += xSpeed * 5f;
		velocityY += ySpeed * 5f;
		isToRotate = true;
	}

	public void SetCamera() {
		//transform.position = new Vector3(-6.22f, 2, 0);
		//transform.rotation = Quaternion.Euler(0, 0, 0);
		isToRotate = false;
		transform.SetPositionAndRotation(new Vector3(-4.484f, 2.05f, 4.311f), Quaternion.Euler(9.492f, 133.876f, 0));
	}
}

